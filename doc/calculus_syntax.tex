The program in EFJ is a list of class definitions. They are very similar to FJ
class definitions: the supertype is always included (unlike in the Java
language, where it can be omitted if it is \texttt{Object}), constructor is
mandatory and the receiver for field access and method invocation is always
required (in Java it can be omitted if it is \texttt{this}). What changes is
that the constructor parameters are no longer tied to class fields, not all
fields need to be assigned (they have default values: \texttt{0} for
\texttt{int}, \texttt{false} for \texttt{boolean} and \texttt{null} for
\texttt{Object} and user defined types), constructor and method bodies are now
sequences of operations and there are many new kinds of expressions for
assignments, constants and arithmetic and logical operations.

Let metavariable $C$ range over class names, $f$ range over field names, $v$
range over variable names (including special name \texttt{this} representing
current object in method and constructor bodies), $K$ range over constructor
declarations, $M$ range over method declarations, $S$ range over sequences, $n$
range over integers and $t$ range over types (including raw types --
\texttt{int} and \texttt{boolean}).

The notation $\overline{X}$ is a shorthand for (possibly empty) sequence $X_1$
$X_2$ \ldots $X_n$. If a character in brackets appears after sequence, it is
used as separator in that sequence. The syntax of class declarations is as
follows:

\begin{grammar}
<L> ::=
    \texttt{class $C$ extends $C$ \{ $\overline{t\ f;}$ $K$ $\overline{M}$ \}}
\end{grammar}

This in fact looks exactly the same as in FJ. The differences are buried deeper.
To define syntax for constructor and method bodies, it is necessary to define
the sequences of operations first:

\begin{grammar}
<S> ::= $\epsilon$ | \texttt{$t$ $v$; $S$} | \texttt{$e$; $S$}
\alt \texttt{if ($e$) \{ $S$ \} else \{ $S$ \} $S$}
\alt \texttt{while ($e$) \{ $S$ \} $S$}
\alt \texttt{return $e$;}
\end{grammar}

where $\epsilon$ represents empty sequence, the second case is a field
declaration, the third case is a single expression, the fourth case is a
conditional, the fifth case is a while loop and the sixth case is a return
statement. All cases, except the first and the last ones, represent the first
statement of the sequence and are followed by the rest of it. With sequences
defined, the syntax of constructor and method declarations is as follows:

\begin{grammar}
<K> ::=
    \texttt{$C$($\overline{t\ x}^{(,)}$) \{ super($\overline{e}^{(,)}$); $S$ \}}

<M> ::= \texttt{$t$ $m$($\overline{t\ x}^{(,)}$) \{ $S$ \}}
\end{grammar}

These two cases, due to the use of sequences, no longer resemble the
corresponding cases in FJ. The \texttt{super} call (invocation of superclass
constructor) also allows expressions as arguments instead of just variables.
The last part of syntax that still needs to be defined is an expression:

\begin{grammar}
<e> ::= \texttt{$v$} | \texttt{$e$.$f$ = $e$} | \texttt{$v$ = $e$}
\alt \texttt{$e$.$m$($\overline{e}^{(,)}$)}
   | \texttt{new $C$($\overline{e}^{(,)}$)} | \texttt{$e$.$f$}
\alt $n$ | \texttt{$e$ + $e$} | \texttt{$e$ - $e$} | \texttt{$e$ * $e$}
   | \texttt{$e$ / $e$} | \texttt{$e$ \% $e$} | \texttt{-$e$}
\alt \texttt{$e$ \textless\ $e$} | \texttt{$e$ \textless= $e$}
   | \texttt{$e$ \textgreater\ $e$} | \texttt{$e$ \textgreater= $e$}
   | \texttt{$e$ == $e$} | \texttt{$e$ != $e$}
\alt \texttt{true} | \texttt{false} | \texttt{$e$ \&\& $e$}
   | \texttt{$e$ || $e$} | \texttt{!$e$}
\alt \texttt{($t$)$e$} | \texttt{null} | \texttt{($e$)}
\end{grammar}

In the first line there is a variable and two additions: an assignment to a
field and an assignment to a variable. The second line shows the cases which are
present in FJ: method call, object creation and field access. The next line
presents numeric constants and arithmetic operations. The fourth line presents
comparison operations. The fifth line presents boolean constants and logical
operations. The last line contains typecast (present in FJ), \texttt{null}
constant and an expression in brackets.

Field lookup as well as method type and body lookup functions were changed to
relations (which should still be deterministic for well-typed programs). New
functions were added for constructor type and body lookup. List syntax is used,
where \texttt{[]} is an empty list and \texttt{$l_1$ ++ $l_2$} means list
concatenation.

\vspace{1em}

\begin{center}

\noindent
\AxiomC{$fields($\texttt{Object}, \texttt{[]}$)$}
\DisplayProof

\vspace{1em}

\noindent
\AxiomC{
    \texttt{class $C$ extends $D$ \{ $\overline{t\ f;}$ $K$ $\overline{M}$ \}}}
\AxiomC{$fields(D, g)$}
\BinaryInfC{\texttt{$fields(C, \overline{t\ f;}$ ++ $g)$}}
\DisplayProof

\vspace{1em}

\noindent
\AxiomC{
    \texttt{class $C$ extends $D$ \{ $\overline{t\ f;}$ $K$ $\overline{M}$ \}}}
\AxiomC{\texttt{$t$ $m$($\overline{t\ x}^{(,)}$) \{ $S$ \} $\in \overline{M}$}}
\BinaryInfC{$mtype(m, C, \overline{t} \rightarrow t)$}
\DisplayProof

\vspace{1em}

\noindent
\AxiomC{
    \texttt{class $C$ extends $D$ \{ $\overline{t\ f;}$ $K$ $\overline{M}$ \}}}
\AxiomC{$m \notin \overline{M}$}
\AxiomC{$mtype(m, D, t)$}
\TrinaryInfC{$mtype(m, C, t)$}
\DisplayProof

\vspace{1em}

\noindent
\AxiomC{
    \texttt{class $C$ extends $D$ \{ $\overline{t\ f;}$ $K$ $\overline{M}$ \}}}
\AxiomC{\texttt{$t$ $m$($\overline{t\ x}^{(,)}$) \{ $S$ \} $\in \overline{M}$}}
\BinaryInfC{$mbody(m, C, (\overline{t\ x}, S))$}
\DisplayProof

\vspace{1em}

\noindent
\AxiomC{
    \texttt{class $C$ extends $D$ \{ $\overline{t\ f;}$ $K$ $\overline{M}$ \}}}
\AxiomC{$m \notin \overline{M}$}
\AxiomC{$mbody(m, D, b)$}
\TrinaryInfC{$mbody(m, C, b)$}
\DisplayProof

\vspace{1em}

\noindent
\AxiomC{
    \texttt{class $C$ extends $D$ \{ $\overline{t\ f;}$ $K$ $\overline{M}$ \}}}
\noLine
\UnaryInfC{
    \texttt{$K = C$($\overline{t\ x}^{(,)}$) \{ super($\overline{e}^{(,)}$); $S$
        \}}}
\UnaryInfC{$ctype(C) = \overline{t} \rightarrow \emptyset$}
\DisplayProof

\vspace{1em}

\noindent
\AxiomC{
    \texttt{class $C$ extends $D$ \{ $\overline{t\ f;}$ $K$ $\overline{M}$ \}}}
\noLine
\UnaryInfC{
    \texttt{$K = C$($\overline{t\ x}^{(,)}$) \{ super($\overline{e}^{(,)}$); $S$
        \}}}
\UnaryInfC{\texttt{$cbody(C) = (\overline{t\ x},$ super($\overline{e}^{(,)}$);
    $S)$}}
\DisplayProof

\end{center}

\vspace{1em}
