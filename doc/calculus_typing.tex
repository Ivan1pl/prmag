The subtyping relation presented in FJ had to be modified to allow raw types.
Another change was made to make reasoning easier -- rules for transitivity and
direct subtype were merged into one.

Let $t$, $u$ and $v$ range over types (including raw types), $C$, $D$ and $E$
range over class names, $e$ range over expressions, $s$ and $b$ range over
sequences, $x$ range over variable names, $n$ range over integers and $\Gamma$
be a typing environment (a mapping from variable names to types). The modified
subtyping relation is defined as follows:

\vspace{1em}

\noindent
\AxiomC{$t <: t$}
\DisplayProof
\hspace{\stretch{1}}
\AxiomC{\texttt{class $C$ extends $D$ \{\ldots\}}}
\AxiomC{$D <: E$}
\BinaryInfC{$C <: E$}
\DisplayProof

\vspace{1em}

As \texttt{int} and \texttt{boolean} may not be used as class names, these rules
show that they cannot be a subtype of anything but themselves. It can be easily
shown that the merged rules hold with this new subtyping relation: direct
subtyping is a simple case of the second rule with $D$ and $E$ being the same
(which is obviously true from first rule). Transitivity can be proven by
induction on the first premise, and it has been done in lemma \texttt{
subtype_trans} (for Coq proof reference, see Reference index in chapter 4).

Most of the typing rules present in FJ remain unchanged. Apart from many new
rules for the new kinds of expressions, the only differences can be seen in the
rules for cast expressions and in the field and method lookups where relations
are used instead of functions.

The notation $\Gamma\ \vdash\ e\ :\ t$ means that the expression $e$ has type
$t$ in typing environment $\Gamma$. The complete list of typing rules:
\vspace{1em}

\noindent
\hspace{\stretch{1}}
\AxiomC{$\Gamma\ \vdash\ x\ :\ \Gamma(x)$}
\DisplayProof
\hspace{\stretch{1}}
\textsc{(T-Var)}
\vspace{1em}

If $x$ is mapped to $t$ in typing environment $\Gamma$, then variable $x$ has
type $t$.
\vspace{1em}

\noindent
\hspace{\stretch{1}}
\AxiomC{$\Gamma\ \vdash\ e_1\ :\ C$}
\AxiomC{$fields(C, \overline{f})$}
\noLine
\UnaryInfC{$t_1\ f\ \in\ \overline{f}$}
\AxiomC{$\Gamma\ \vdash\ e_2\ :\ t_2$}
\noLine
\UnaryInfC{$t_2 <: t_1$}
\TrinaryInfC{\texttt{$\Gamma\ \vdash\ e_1$.$f$ = $e_2\ :\ t_2$}}
\DisplayProof
\hspace{\stretch{1}}
\textsc{(T-FieldAsgn)}
\vspace{1em}

If $e_1$ has type $C$, class $C$ has field $f$ of type $t_1$, $e_2$ has type
$t_2$ and $t_2$ is a subtype of $t_1$, then field assignment of expression $e_2$
to field $f$ of expression $e_1$ has type $t_2$.
\vspace{1em}

\noindent
\hspace{\stretch{1}}
\AxiomC{$\Gamma(x) = t_1$}
\AxiomC{$\Gamma\ \vdash\ e\ :\ t_2$}
\AxiomC{$t_2 <: t_1$}
\TrinaryInfC{\texttt{$\Gamma\ \vdash\ x$ = $e\ :\ t_2$}}
\DisplayProof
\hspace{\stretch{1}}
\textsc{(T-Asgn)}
\vspace{1em}

If $x$ is mapped to $t_1$ in typing environment $\Gamma$, expression $e$ has
type $t_2$ and $t_2$ is a subtype of $t_1$, then assignment of expression $e$ to
variable $x$ has type $t_2$.
\vspace{1em}

\noindent
\hspace{\stretch{1}}
\AxiomC{$\Gamma\ \vdash\ e\ :\ C$}
\AxiomC{$\Gamma\ \vdash\ \overline{e}\ :\ \overline{t}$}
\AxiomC{$mtype(m, C, \overline{u}\to v)$}
\noLine
\UnaryInfC{$\overline{t}\ <:\ \overline{u}$}
\TrinaryInfC{\texttt{$\Gamma\ \vdash\ e$.$m$($\overline{e}^{(,)}$)$\ :\ v$}}
\DisplayProof
\hspace{\stretch{1}}
\textsc{(T-Invk)}
\vspace{1em}

If the expression $e$ has type $C$, the type of method $m$ in class $C$ is
$\overline{u} \rightarrow v$ and all expressions on list $\overline{e}$ have
types that are subtypes of corresponding types on list $\overline{u}$, then
method call of method $m$ on expression $e$ with parameters $\overline{e}$ has
type $v$.
\vspace{1em}

\noindent
\hspace{\stretch{1}}
\AxiomC{$ctype(C) = \overline{u} \rightarrow \emptyset$}
\AxiomC{$\Gamma\ \vdash\ \overline{e}\ :\ \overline{t}$}
\AxiomC{$\overline{t}\ <:\ \overline{u}$}
\TrinaryInfC{\texttt{$\Gamma\ \vdash$ new $C$($\overline{e}^{(,)}$)$\ :\ C$}}
\DisplayProof
\hspace{\stretch{1}}
\textsc{(T-New)}
\vspace{1em}

If the type of the constructor of class $C$ is
$\overline{u} \rightarrow \emptyset$ and all expressions on list $\overline{e}$
have types that are subtypes of corresponding types on list $\overline{u}$, then
object creation expression of object of class $C$ with parameters $\overline{e}$
has type $C$.
\vspace{1em}

\noindent
\hspace{\stretch{1}}
\AxiomC{\texttt{$\Gamma($this$) = C$}}
\noLine
\UnaryInfC{$ctype(D) = \overline{u} \rightarrow \emptyset$}
\AxiomC{$\Gamma\ \vdash\ \overline{e}\ :\ \overline{t}$}
\noLine
\UnaryInfC{$\overline{t}\ <:\ \overline{u}$}
\noLine
\UnaryInfC{\texttt{class $C$ extends $D$ \{ \ldots \}}}
\BinaryInfC{\texttt{$\Gamma\ \vdash$ super($\overline{e}^{(,)}$)$\ :\ C$}}
\DisplayProof
\hspace{\stretch{1}}
\textsc{(T-Super)}
\vspace{1em}

To make reasoning easier, the special \texttt{super} statement (calling the
constructor of direct superclass) is considered an expression here. The parser
should ensure that it appears only as the first statement inside the constructor
of class $C$, and that the result of this operation is never used (using it
elsewhere does not cause any problems in the calculus but is incompatible with
Java).

If class $D$ is a direct subtype of class $C$, the type of the constructor of
class $D$ is $\overline{u} \rightarrow \emptyset$ and all expressions on list
$\overline{e}$ have types that are subtypes of corresponding types on list
$\overline{u}$, then superclass constructor call in class $C$ with parameters
$\overline{e}$ has type $C$.
\vspace{1em}

\noindent
\hspace{\stretch{1}}
\AxiomC{$\Gamma\ \vdash\ e\ :\ C$}
\AxiomC{$fields(C, \overline{f})$}
\AxiomC{$t\ f\ \in\ \overline{f}$}
\TrinaryInfC{\texttt{$\Gamma\ \vdash\ e$.$f\ :\ t$}}
\DisplayProof
\hspace{\stretch{1}}
\textsc{(T-Field)}
\vspace{1em}

If the expression $e$ has type $C$ and class $C$ has field $f$ of type $t$, then
field access of field $f$ on expression $e$ has type $t$.
\vspace{1em}

\noindent
\hspace{\stretch{1}}
\AxiomC{\texttt{$\Gamma\ \vdash\ n\ :$ int}}
\DisplayProof
\hspace{\stretch{1}}
\textsc{(T-Num)}
\vspace{1em}

Numeric constants have type \texttt{int}.
\vspace{1em}

\noindent
\hspace{\stretch{1}}
\AxiomC{\texttt{$\Gamma\ \vdash\ e_1\ :$ int}}
\AxiomC{\texttt{$\Gamma\ \vdash\ e_2\ :$ int}}
\AxiomC{$\odot \in
        \{$\texttt{+}, \texttt{-}, \texttt{*}, \texttt{/}, \texttt{\%}$\}$}
\TrinaryInfC{\texttt{$\Gamma\ \vdash\ e_1 \odot e_2\ :$ int}}
\DisplayProof
\hspace{\stretch{1}}
\textsc{(T-Aop)}
\vspace{1em}

If expressions $e_1$ and $e_2$ have type \texttt{int}, then arithmetic operation
(addition, subtraction, multiplication, division and modulo) on expressions
$e_1$ and $e_2$ has type \texttt{int}.
\vspace{1em}

\noindent
\hspace{\stretch{1}}
\AxiomC{\texttt{$\Gamma\ \vdash\ e\ :$ int}}
\UnaryInfC{\texttt{$\Gamma\ \vdash\ $-$e\ :$ int}}
\DisplayProof
\hspace{\stretch{1}}
\textsc{(T-UMinus)}
\vspace{1em}

If the expression $e$ has type \texttt{int}, then unary minus on expression $e$
also has type \texttt{int}.
\vspace{1em}

\noindent
\hspace{\stretch{1}}
\AxiomC{\texttt{$\Gamma\ \vdash\ e_1\ :$ int}}
\AxiomC{\texttt{$\Gamma\ \vdash\ e_2\ :$ int}}
\AxiomC{$\odot \in \{$\texttt{<}, \texttt{<=}, \texttt{>}, \texttt{>=}$\}$}
\TrinaryInfC{\texttt{$\Gamma\ \vdash\ e_1 \odot e_2\ :$ boolean}}
\DisplayProof
\hspace{\stretch{1}}
\textsc{(T-Cop)}
\vspace{1em}

If expressions $e_1$ and $e_2$ have type \texttt{int}, then comparison of
expressions $e_1$ and $e_2$ has type \texttt{boolean}.
\vspace{1em}

\noindent
\hspace{\stretch{1}}
\AxiomC{$\Gamma\ \vdash\ e_1\ :\ t_1$}
\AxiomC{$\Gamma\ \vdash\ e_2\ :\ t_2$}
\AxiomC{$\odot \in \{$\texttt{==}, \texttt{!=}$\}$}
\TrinaryInfC{\texttt{$\Gamma\ \vdash\ e_1 \odot e_2\ :$ boolean}}
\DisplayProof
\hspace{\stretch{1}}
\textsc{(T-EqOp)}
\vspace{1em}

If expressions $e_1$ and $e_2$ have types, then equality check of expressions
$e_1$ and $e_2$ has type \texttt{boolean}.
\vspace{1em}

\noindent
\hspace{\stretch{1}}
\AxiomC{$e \in \{$\texttt{true}, \texttt{false}$\}$}
\UnaryInfC{\texttt{$\Gamma\ \vdash\ e\ :$ boolean}}
\DisplayProof
\hspace{\stretch{1}}
\textsc{(T-BConst)}
\vspace{1em}

Boolean constants have type \texttt{boolean}.
\vspace{1em}

\noindent
\hspace{\stretch{1}}
\AxiomC{\texttt{$\Gamma\ \vdash\ e_1\ :$ boolean}}
\AxiomC{\texttt{$\Gamma\ \vdash\ e_2\ :$ boolean}}
\AxiomC{$\odot \in \{$\texttt{\&\&}, \texttt{||}$\}$}
\TrinaryInfC{\texttt{$\Gamma\ \vdash\ e_1 \odot e_2\ :$ boolean}}
\DisplayProof
\hspace{\stretch{1}}
\textsc{(T-Bop)}
\vspace{1em}

If expressions $e_1$ and $e_2$ have type \texttt{boolean}, then logical
operation (and, or) on expressions $e_1$ and $e_2$ has type \texttt{boolean}.
\vspace{1em}

\noindent
\hspace{\stretch{1}}
\AxiomC{\texttt{$\Gamma\ \vdash\ e\ :$ boolean}}
\UnaryInfC{\texttt{$\Gamma\ \vdash$ !$e\ :$ boolean}}
\DisplayProof
\hspace{\stretch{1}}
\textsc{(T-Not)}
\vspace{1em}

If the expression $e$ has type \texttt{boolean}, then \texttt{not} operation on
expression $e$ also has type \texttt{boolean}.
\vspace{1em}

\noindent
\hspace{\stretch{1}}
\AxiomC{\texttt{$\Gamma\ \vdash$ null $:\ C$}}
\DisplayProof
\hspace{\stretch{1}}
\textsc{(T-Null)}
\vspace{1em}

The \texttt{null} constant has type $C$ for every class $C$.
\vspace{1em}

\noindent
\hspace{\stretch{1}}
\AxiomC{$\Gamma\ \vdash\ e\ :\ t_1$}
\AxiomC{$t_1 <: t_2 \lor t_2 <: t_1$}
\BinaryInfC{\texttt{$\Gamma\ \vdash$ ($t_2$)$e\ :\ t_2$}}
\DisplayProof
\hspace{\stretch{1}}
\textsc{(T-Cast-Sub)}
\vspace{1em}

If the expression $e$ has type $t_1$ and types $t_1$ and $t_2$ are related (one
is a subtype of the other), then cast of expression $e$ to type $t_2$ has type
$t_2$. This rule represents rules \textsc{T-UCast} and \textsc{T-DCast} from FJ
merged into one.
\vspace{1em}

\noindent
\hspace{\stretch{1}}
\AxiomC{\texttt{$\Gamma\ \vdash\ e\ :$ int}}
\UnaryInfC{\texttt{$\Gamma\ \vdash\ $(boolean)$e\ :$ boolean}}
\DisplayProof
\hspace{\stretch{1}}
\textsc{(T-Cast-Int)}
\vspace{1em}

If the expression $e$ has type \texttt{int}, then cast of expression $e$ to type
\texttt{boolean} has type \texttt{boolean}.
\vspace{1em}

\noindent
\hspace{\stretch{1}}
\AxiomC{\texttt{$\Gamma\ \vdash\ e\ :$ boolean}}
\UnaryInfC{\texttt{$\Gamma\ \vdash\ $(int)$e\ :$ int}}
\DisplayProof
\hspace{\stretch{1}}
\textsc{(T-Cast-Bool)}
\vspace{1em}

If the expression $e$ has type \texttt{boolean}, then cast of expression $e$ to
type \texttt{int} has type \texttt{int}.

Note that the only casts allowed between unrelated types are between \texttt{
int} and \texttt{boolean}. The rule \textsc{T-SCast} from FJ allowed such casts
between any types with \textit{stupid warning} because of optimization issues
(well-typed program could no longer be well-typed after casts were optimized).
As optimizations are not taken into consideration in this work, that rule can be
skipped. However, casts between \texttt{int} and \texttt{boolean} are a feature
of Java which can be kept with small amount of work, so they are allowed in the
EFJ.

As sequences were added to the language and method typing depends on them,
it is also necessary to define typing rules for them. The notation
$\Gamma\ \vdash\ s\ :\rightarrow\ t$ means that the sequence $s$ has type $t$ in
typing environment $\Gamma$. The notation $\Gamma,\ x\ :\ t$ means the
environment $\Gamma$ extended by mapping variable name $x$ to type $t$ and
$\Gamma,\ \overline{x\ :\ t}$ is a shorthand for $\Gamma,\ x_1\ :\ t_1,\ldots,\ 
x_n\ :\ t_n$. If $\Gamma$ is skipped it means an empty typing environment.
Sequence typing is defined by the following rules:
\vspace{1em}

\noindent
\hspace{\stretch{1}}
\AxiomC{$\Gamma\ \vdash\ \epsilon\ :\rightarrow\ \emptyset$}
\DisplayProof
\hspace{\stretch{1}}
\textsc{(T-EndSeq)}
\vspace{1em}

The empty sequence has the empty type. Note that this is different from one
having no type, which would mean it was incorrect.
\vspace{1em}

\noindent
\hspace{\stretch{1}}
\AxiomC{$x \notin \Gamma$}
\AxiomC{$\Gamma,\ x\ :\ u\ \vdash\ s\ :\rightarrow\ t$}
\BinaryInfC{\texttt{$\Gamma\ \vdash\ u\ x$; $s\ :\rightarrow\ t$}}
\DisplayProof
\hspace{\stretch{1}}
\textsc{(T-Declaration)}
\vspace{1em}

If $x$ is undeclared (it is indicated by $x$ not appearing in the typing
environment) and the sequence $s$ has type $t$ in the typing environment
extended by mapping variable $x$ to type $u$, then the sequence
\texttt{$u\ x$; $s$} has type $t$.
\vspace{1em}

\noindent
\hspace{\stretch{1}}
\AxiomC{$\Gamma\ \vdash\ e\ :\ u$}
\AxiomC{$\Gamma\ \vdash\ s\ :\rightarrow\ t$}
\BinaryInfC{\texttt{$\Gamma\ \vdash\ e$; $s\ :\rightarrow\ t$}}
\DisplayProof
\hspace{\stretch{1}}
\textsc{(T-Expression)}
\vspace{1em}

If the expression $e$ has type (it does not matter what type it is as it is
ignored) and the sequence $s$ has type $t$, then the sequence \texttt{$e$; $s$}
has type $t$.
\vspace{1em}

\noindent
\hspace{\stretch{1}}
\AxiomC{\texttt{$\Gamma\ \vdash\ e\ :$ boolean}}
\noLine
\UnaryInfC{$\Gamma\ \vdash\ b_1\ :\rightarrow\ t_1$}
\AxiomC{$t_1\ <:\ t_2$}
\noLine
\UnaryInfC{$\Gamma\ \vdash\ b_2\ :\rightarrow\ t_2$}
\BinaryInfC{\texttt{$\Gamma\ \vdash\ $if ($e$) \{ $b_1$ \} else \{ $b_2$ \} 
$\epsilon\ :\rightarrow\ t_2$}}
\DisplayProof
\hspace{\stretch{1}}
\textsc{(T-If-End$_1$)}
\vspace{1em}

\noindent
\hspace{\stretch{1}}
\AxiomC{\texttt{$\Gamma\ \vdash\ e\ :$ boolean}}
\noLine
\UnaryInfC{$\Gamma\ \vdash\ b_1\ :\rightarrow\ t_1$}
\AxiomC{$t_2\ <:\ t_1$}
\noLine
\UnaryInfC{$\Gamma\ \vdash\ b_2\ :\rightarrow\ t_2$}
\BinaryInfC{\texttt{$\Gamma\ \vdash\ $if ($e$) \{ $b_1$ \} else \{ $b_2$ \} 
$\epsilon\ :\rightarrow\ t_1$}}
\DisplayProof
\hspace{\stretch{1}}
\textsc{(T-If-End$_2$)}
\vspace{1em}

\noindent
\hspace{\stretch{1}}
\AxiomC{\texttt{$\Gamma\ \vdash\ e\ :$ boolean}}
\noLine
\UnaryInfC{$\Gamma\ \vdash\ b_1\ :\rightarrow\ \emptyset$}
\AxiomC{$\Gamma\ \vdash\ b_2\ :\rightarrow\ \emptyset$}
\noLine
\UnaryInfC{$\Gamma\ \vdash\ s\ :\rightarrow\ t$}
\BinaryInfC{\texttt{$\Gamma\ \vdash\ $if ($e$) \{ $b_1$ \} else \{ $b_2$ \} 
$s\ :\rightarrow\ t$}}
\DisplayProof
\hspace{\stretch{1}}
\textsc{(T-If-Cont)}
\vspace{1em}

There are three cases for \texttt{if} statement. The first two explain that if
a sequence in any of its branches has a type, then sequences in both branches
must have types and those types have to be related in terms of subtyping.
Moreover, that \texttt{if} statement must be the last statement in the sequence.
This restriction was introduced because otherwise, the sequence typing relation
would need to be annotated with the expected type, or the greatest common type
(in terms of subtyping) of all subsequences in all branches would need to be
passed forward (to get the greatest common type of the whole sequence). This
approach prevents that from happening as branches must either both have no type
(the third rule -- then the type of the sequence following the \texttt{if}
statement is the type of the whole sequence) or both have a type and the types
must be related (first two rules -- then the type of the whole sequence is the
one that is more general of those two types). The type of the expression $e$
appearing as the \texttt{if} statement condition must be \texttt{boolean}.
\vspace{1em}

\noindent
\hspace{\stretch{1}}
\AxiomC{\texttt{$\Gamma\ \vdash\ e\ :$ boolean}}
\noLine
\UnaryInfC{$\Gamma\ \vdash\ b\ :\rightarrow\ \emptyset$}
\AxiomC{$\Gamma\ \vdash\ s\ :\rightarrow\ t$}
\BinaryInfC{\texttt{$\Gamma\ \vdash\ $while ($e$) \{ $b$ \} $s\ :\rightarrow\ t$
}}
\DisplayProof
\hspace{\stretch{1}}
\textsc{(T-While)}
\vspace{1em}

If the expression $e$ has type \texttt{boolean}, the sequence $b$ (loop body)
has empty type (for the same reason as in the \texttt{if} statement branches)
and the sequence $s$ has type $t$, then the sequence \texttt{while($e$) \{ $b$
\} $s$} has type $t$.
\vspace{1em}

\noindent
\hspace{\stretch{1}}
\AxiomC{$\Gamma\ \vdash\ e\ :\ t$}
\UnaryInfC{\texttt{$\Gamma\ \vdash\ $return $e$; $:\rightarrow\ t$}}
\DisplayProof
\hspace{\stretch{1}}
\textsc{(T-Return)}
\vspace{1em}

If the expression $e$ has type $t$, then the return statement of expression $e$
has type $t$.

It is now possible to define typing rules for method and constructor bodies.
They show when methods and constructors are correct in a given class. In FJ,
there was no typing rule for constructors (correctness was ensured in the typing
rule for classes as constructor arguments had to match with fields and there
were only assignments in constructor bodies). The typing rule for methods was
slightly different because of the use of sequences (in FJ, methods contained
only a single expression). Additional modification was made to make the
definition clearer, to make a better use of $mtype$ and $mbody$ relations and to
avoid using conditionals in premises and direct subtyping.
\vspace{1em}

\noindent
\hspace{\stretch{1}}
\AxiomC{$mtype(m, C, \overline{t} \rightarrow t)$}
\noLine
\UnaryInfC{$mbody(m, C, (\overline{t\ x}, s))$}
\noLine
\UnaryInfC{\texttt{this $:\ C,\ \overline{x\ :\ t}\ \vdash\ s\ :\rightarrow\ t$}
}
\UnaryInfC{\texttt{$m$ OK IN $C$}}
\DisplayProof
\hspace{\stretch{1}}
\textsc{(T-Method)}
\vspace{1em}

If the type of method $m$ is $\overline{t} \rightarrow t$, it takes parameters
$\overline{x}$ of types $\overline{t}$ and its body has type $t$ with those
parameters mapped to their types and \texttt{this} mapped to $C$ in the typing
environment, then method $m$ is well-typed.
\vspace{1em}

\noindent
\hspace{\stretch{1}}
\AxiomC{$ctype(m, C) = \overline{t} \rightarrow \emptyset$}
\noLine
\UnaryInfC{$cbody(m, C) = (\overline{t\ x}, s)$}
\noLine
\UnaryInfC{\texttt{
this $:\ C,\ \overline{x\ :\ t}\ \vdash\ s\ :\rightarrow\ \emptyset$}}
\UnaryInfC{\texttt{<init> OK IN $C$}}
\DisplayProof
\hspace{\stretch{1}}
\textsc{(T-Constructor)}
\vspace{1em}

If the type of the constructor is $\overline{t} \rightarrow \emptyset$, it takes
parameters $\overline{x}$ of types $\overline{t}$ and its body has empty type
with those parameters mapped to their types and \texttt{this} mapped to $C$ in
the typing environment, then the constructor of class $C$ is well-typed.

The next thing that needs to be defined is class typing. Just like in FJ, there
is only one rule and it looks similar to the one shown in FJ.
\vspace{1em}

\noindent
\hspace{\stretch{1}}
\AxiomC{\texttt{$\overline{m}$ OK IN $C$}}
\AxiomC{\texttt{<init> OK IN $C$}}
\noLine
\BinaryInfC{
$E <: C \rightarrow mtype(m,D,t_1) \rightarrow mtype(m,C,t_2) \rightarrow
t_1 = t_2$}
\UnaryInfC{\texttt{$C$ OK}}
\DisplayProof
\hspace{\stretch{1}}
\textsc{(T-Class)}
\vspace{1em}

Unlike in FJ, fields are not mentioned here but there is an additional premise
saying that the overriden methods should have exactly the same type as the
original method. This was introduced to make the reasoning in Coq easier.

There is more to class correctness than typing. The following condition must
also be true for the class to be correct: field names and method names need to
be unique.

For the whole set of classes to be correct, the following conditions must be
true:
\begin{itemize}
\item{no class may be named \texttt{Object},}
\item{class names have to be unique,}
\item{all classes have to be correct,}
\item{no inheritance loops may be present (if $C <: D$ and $D <: C$ then
$C = D$ and for all classes $C$, $C <:$ \texttt{Object}).}
\end{itemize}
