\documentclass[english,mgr,shortabstract]{iithesis}
\usepackage[utf8]{inputenc}
\usepackage{syntax}
\usepackage{bussproofs}
\usepackage[makeroom]{cancel}
\usepackage[htt]{hyphenat}
\usepackage{amsthm}
\let\lll\undefined
\usepackage{amssymb}

\renewcommand{\syntleft}{}
\renewcommand{\syntright}{}

\setlength{\grammarparsep}{0pt}
\setlength{\grammarindent}{3.5em}

\englishtitle{A Coq formalization of a type system\fmlinebreak
for an object-oriented programming language}
\polishtitle{Formalizacja w systemie Coq systemu typów\fmlinebreak
orientowanego obiektowo języka programowania}
\englishabstract{This work proposes a Coq formalization of a type system for
an extended version of Featherweight Java. The extension has been created
specifically for this thesis and includes many features of the full Java
programming language, including raw types and complex method bodies with
sequences of operations and conditionals. The work proposes a type system and
operational semantics that simulates the behaviour of Java Virtual Machine. It
then provides a proof of a type system safety, making the first step towards
creating a certified Java compiler.}
\polishabstract {Ta praca przedstawia formalizację w systemie Coq systemu
typów języka, który jest rozszerzoną wersją Featherweight Java. Rozszerzenie to
powstało konkretnie na potrzeby tej pracy i zawiera wiele elementów
pełnego języka programowania Java, w tym typy proste i złożone ciała metod z
sekwencjami operacji i operacjami warunkowymi. Praca przedstawia system typów i
semantykę operacyjną, która symuluje zachowanie maszyny wirtualnej Java.
Następnie przedstawia dowód bezpieczeństwa systemu typów, czyniąc pierwszy krok
do stworzenia certyfikowanego kompilatora języka Java.}
\author{Mikołaj Nowak\\Uniwersytet Wrocławski}
\advisor{dr hab. Dariusz Biernacki}
\date{\today}

\newtheorem*{preservation}{Theorem}
\newtheorem*{preservation-aux}{Lemma}
\newtheorem*{induction-hyp-t}{Hypothesis}
\newtheorem*{induction-hyp-e}{Hypothesis}
\newtheorem*{induction-hyp-l}{Hypothesis}
\newtheorem*{preservation-aux-l}{Definition}
\newtheorem*{set-field-heap-split}{Lemma}
\newtheorem*{set-field-get-heap}{Lemma}
\newtheorem*{set-field-aux-def-length}{Lemma}
\newtheorem*{set-field-aux-obj-length}{Lemma}
\newtheorem*{set-field-aux-def-field-changed}{Lemma}
\newtheorem*{set-field-aux-obj-field-changed}{Lemma}
\newtheorem*{set-field-get-refs}{Lemma}
\newtheorem*{set-field-aux-def-name-preserved}{Lemma}
\newtheorem*{set-field-state-heap-correct}{Lemma}
\newtheorem*{mtype-mbody-eq-types}{Lemma}
\newtheorem*{mtype-mbody-eq-types-sub}{Lemma}
\newtheorem*{map-params-heap-not-changed}{Lemma}
\newtheorem*{map-params-aux-heap-not-changed}{Lemma}
\newtheorem*{eval-seq-heap-entry-remains}{Lemma}
\newtheorem*{eval-heap-entry-remains}{Lemma}
\newtheorem*{eval-args-heap-entry-remains}{Lemma}
\newtheorem*{get-class-subclass}{Lemma}
\newtheorem*{apply-state-aux-set-value-aux}{Lemma}
\newtheorem*{apply-state-aux-set-value-aux-neq}{Lemma}
\newtheorem*{apply-state-aux-map-params-this}{Lemma}
\newtheorem*{all-subtype-sublist}{Lemma}
\newtheorem*{state-correct-map-params-aux}{Lemma}
\newtheorem*{state-correct-map-params}{Lemma}
\newtheorem*{preservation-aux-evaluates-seq}{Lemma}
\newtheorem*{progress}{Theorem}
\newtheorem*{induction-hyp-t2}{Hypothesis}
\newtheorem*{induction-hyp-e2}{Hypothesis}
\newtheorem*{induction-hyp-l2}{Hypothesis}
\newtheorem*{set-field-aux-def-some}{Lemma}
\newtheorem*{set-field-aux-some}{Lemma}
\newtheorem*{set-field-some-aux}{Lemma}
\newtheorem*{set-field-some}{Lemma}
\newtheorem*{subtype-trans}{Lemma}
\newtheorem*{has-type-subtype}{Lemma}
\newtheorem*{preservation-l}{Lemma}
\newtheorem*{nth-error-get-refs-set-value}{Lemma}
\newtheorem*{progress-seq-aux}{Lemma}
\newtheorem*{progress-seq-aux-Some}{Lemma}

\begin{document}

\chapter{Introduction}

%INTRODUCTION

\chapter{Calculus}

%CALCULUS

\chapter{Type system safety}

%TYPE_SAFETY

\chapter{Formalization}

The entire work has been formalized in Coq since the very beginning. All the
definitions, theorems and proofs presented here can be found in the Coq code.
However, due to its complexity and some technical details specific to Coq,
there are several differences between the definitions in the previous sections
and the actual code. This section explains those differences.

\section{Syntax}

For the sake of simplicity, the type \texttt{int} has been defined as
\texttt{Z_as_Int.t}. Types \texttt{jType} (representing the type of variable,
later also used for typing, defined as either an \texttt{Int}, a
\texttt{Boolean} or a \texttt{Named} type (\texttt{string})), \texttt{jVar}
(representing variable declaration), \texttt{jAOp} (enumeration of arithmetic
operations), \texttt{jCOp} (enumeration of numeric comparisons), \texttt{jEqOp}
(enumeration of object comparisons), \texttt{jBOp} (enumeration of logical
operations), \texttt{jExpr} (representing all kinds of expressions),
\texttt{jSeq} (representing sequences), \texttt{jMethod} (representing methods),
\texttt{jConstructor} (representing constructors) and \texttt{jClass}
(representing classes) were defined as inductive types.

A custom induction hypothesis \texttt{jExpr_ind2} was provided for the type
\texttt{jExpr} due to the fact that the default one does not handle with the
lists of expressions (parameters in method and constructor calls).

For the sake of simplicity, the constructor \texttt{Super} (representing
invocation of a constructor of a supertype) was included in the \texttt{jExpr}
type. Such expressions can only appear as the first expressions in constructor
bodies but this work treats them as if they could appear anywhere. Such uses can
easily be filtered out during parsing phase.

Not mentioned before, the \texttt{world} is defined as a list of \texttt{jClass}
values. As classes cannot appear out of nowhere, most reasoning is done only on
the classes appearing in the world, which represents the whole program. Since
classes can be defined incorrectly, all the important lemmas and theorems state
propositions only about things that can be found in the world.

Often used and not mentioned before is a class lookup. In the previous chapters,
classes were treated as types, but here they are different things. The class
type (\texttt{Named} constructor in the \texttt{jType} type) contains only the
name of the class (a \texttt{string}). The function \texttt{get_class} provides
a way to obtain a class with a given name from the world.

Relations \texttt{fields}, \texttt{mtype} and \texttt{mbody} are defined
inductively as stated before, with one difference only, namely, they all use
the world and class lookup (to obtain superclasses).

Functions \texttt{ctype} and \texttt{cbody} are defined exactly as stated
before.

\section{Typing}

The subtyping relation is defined inductively as described before, but again,
the class $C$ extending $D$ in the second rule must be in the world. The
decidability of this relation is axiomatized.

The typing environment is defined as a function of type \texttt{string
$\rightarrow$ option jType}, representing a partial function of type
\texttt{string $\rightarrow$ jType}.

The expression and sequence typing relations are defined inductively following
the relation described before. In the expression typing relation, the world
appears quite often in the rules that concern fields, methods or constructors
(class lookup, \texttt{fields}, \texttt{mtype} and \texttt{mbody} relations,
\texttt{ctype} and \texttt{cbody} functions all use the world and they are used
here).

Method and class typing are not directly defined but their various properties
appear later as assumptions for lemmas and theorems (including both
\texttt{preservation} and \texttt{progress}.

\section{State}

Values are defined as an inductive type with constructors for an \texttt{int},
a \texttt{boolean}, the \texttt{null} value and a reference. The reference is
represented as a \texttt{string} (class name) and a natural number (index of the
object on the heap).

Objects are represented as an inductive type with just one constructor, taking
a \texttt{string} (class name) and an association list which represents a
mapping from field names to values.

The state is represented as a record with an association list as the stack (a
mapping from variable names to values) and a list of objects as the heap.

Value typing is defined as a relation where only an \texttt{int} value may have
the type \texttt{int}, only a \texttt{boolean} value may have the type
\texttt{boolean}, the \texttt{null} value is of any class type and a reference
with class name $c$ has the type $t$ when $c\ <:\ t$.

The function $get\_refs$ mentioned hereinbefore is defined as a list
concatenation of the stack and fields of all objects on the heap.

The state correctness is defined, as described before, with the use of relations
\texttt{fields_unique}, \texttt{heap_correct}, \texttt{state_heap_correct} and
\texttt{state_correct}, each extending the previous one with an additional
condition, together making it a conjunction of four conditions: stack
correctness, reference consistency, heap correctness and field uniqueness.

\section{Evaluation}

The inductive type \texttt{exc} represents an enumeration of all possible
exceptions. The inductive type \texttt{result} represents an evaluation result
(either a value or an exception).

The evaluation relations for expressions, expression lists and sequences are
defined as four mutually recursive inductive propositions
\texttt{evaluates_expr}, \texttt{evaluates_args} (evaluation of a list of
expressions to a value or a $\bullet$), \texttt{evaluates_args_exc} (evaluation
of a list of expressions to an exception) and \texttt{evaluates_seq}.

The section containing proofs assumes many things which are true only when all
classes in the world are well-typed. The assumptions can be explained as
follows:
\begin{itemize}
\item \texttt{Hclass_name_not_object} -- No class definition has a name
\texttt{Object}.
\item \texttt{Hclass_name_unique} -- There are not any different classes with
the same name in the world.
\item \texttt{Hfield_decl_unique} -- No two fields with the same name appear in
any of the class definitions in the world.
\item \texttt{Hmethod_name_unique} -- No two methods with different return types
or parameters list share a name in any of the classes in the world.
\item \texttt{Hmethod_override_equal_types} -- The overriden methods have the
same types as the base methods in all classes in the world.
\item \texttt{Hmethod_has_type_in_subclass} -- All methods have some type in
subclasses of the class in which they were defined.
\item \texttt{Hmethod_body_correct_type} -- Bodies of all methods in all classes
in the world have the same type as their declared return type.
\item \texttt{Hfields_determ} -- Fields relation is deterministic (true if no
class appears more than once in the world).
\item \texttt{Hconstructor_body_correct_type} -- Bodies of all constructors in
all classes in the world have the type $\emptyset$.
\item \texttt{Hfields_f_exist} -- For all classes in the world, it is possible
to  determine what fields they have (true if there are no inheritance loops).
\end{itemize}

A tactic \texttt{preservation_auto} is defined to automatize some proofs (it
deals quite well with simple cases that do not directly modify the state). A
tactic \texttt{heap_entry_remains_auto} is defined specifically to automatize
the proof of the lemma \texttt{eval_heap_entry_remains}.

The next class lookup function \texttt{get_class_t} is defined to look up
classes not by name but by the type (\texttt{jType}). This is necessary for the
proof of the lemma \texttt{get_class_subclass} where the induction is used on
the evidence that one type is a subtype of the other and the types need to be as
general as possible for the induction to work (\texttt{Named} constructor of the
\texttt{jType} type cannot be used directly).

\section{Statistics}

The whole project consists of five Coq files: \texttt{Syntx.v},
\texttt{Typing.v}, \texttt{State.v}, \texttt{StrongInduction.v} and
\texttt{Semantics.v}. Here is a table that shows how many lines of code each of
these files contains (with specifications and proofs counted separately):

\begin{table}[!htbp]
\centering
\begin{tabular}{|l|r|r|}
\hline
\textbf{File name}         & \textbf{Specifications} & \textbf{Proofs} \\ \hline
\texttt{Syntx.v}           & $182$                   & $68$            \\ \hline
\texttt{Typing.v}          & $157$                   & $0$             \\ \hline
\texttt{State.v}           & $236$                   & $392$           \\ \hline
\texttt{StrongInduction.v} & $31$                    & $60$            \\ \hline
\texttt{Semantics.v}       & $644$                   & $2789$          \\ \hline
\textbf{Total}             & $1250$                  & $3309$          \\ \hline
\end{tabular}
\end{table}

\section{Coq definition index}

Entries are divided into two groups, namely, definitions and theorems with
lemmas, and presented in an alphabetical order.

\subsection{Definitions}

\texttt{EmptyState}, in \texttt{State.v}, line 36\\
\texttt{addObject}, in \texttt{State.v}, line 47\\
\texttt{all_subtype}, in \texttt{Typing.v}, line 21\\
\texttt{aop}, in \texttt{Semantics.v}, line 27\\
\texttt{apply_state}, in \texttt{State.v}, line 33\\
\texttt{apply_state_aux}, in \texttt{State.v}, line 27\\
\texttt{cbody}, in \texttt{Syntx.v}, line 217\\
\texttt{cop}, in \texttt{Semantics.v}, line 36\\
\texttt{ctype}, in \texttt{Syntx.v}, line 197\\
\texttt{empty_typing_env}, in \texttt{Typing.v}, line 27\\
\texttt{eqOp}, in \texttt{Semantics.v}, line 44\\
\texttt{evaluates_args}, in \texttt{Semantics.v}, line 270\\
\texttt{evaluates_args_exc}, in \texttt{Semantics.v}, line 276\\
\texttt{evaluates_expr}, in \texttt{Semantics.v}, line 74\\
\texttt{evaluates_seq}, in \texttt{Semantics.v}, line 281\\
\texttt{exc}, in \texttt{Semantics.v}, line 13\\
\texttt{expr_typing}, in \texttt{Typing.v}, line 47\\
\texttt{extend_typing_env}, in \texttt{Typing.v}, line 30\\
\texttt{extend_typing_env_multi}, in \texttt{Typing.v}, line 34\\
\texttt{fields}, in \texttt{Syntx.v}, line 142\\
\texttt{fields_f}, in \texttt{Syntx.v}, line 150\\
\texttt{fields_unique}, in \texttt{State.v}, line 144\\
\texttt{get_class}, in \texttt{Syntx.v}, line 133\\
\texttt{get_class_t}, in \texttt{Semantics.v}, line 756\\
\texttt{get_fields_init_vals}, in \texttt{State.v}, line 50\\
\texttt{get_refs}, in \texttt{State.v}, line 141\\
\texttt{has_type}, in \texttt{State.v}, line 124\\
\texttt{heap_correct}, in \texttt{State.v}, line 147\\
\texttt{heap_entry_remains_auto}, in \texttt{Semantics.v}, line 604\\
\texttt{int}, in \texttt{Syntx.v}, line 7\\
\texttt{is_object}, in \texttt{Semantics.v}, line 439\\
\texttt{jAOp}, in \texttt{Syntx.v}, line 19\\
\texttt{jBOp}, in \texttt{Syntx.v}, line 39\\
\texttt{jCOp}, in \texttt{Syntx.v}, line 27\\
\texttt{jClass}, in \texttt{Syntx.v}, line 126\\
\texttt{jConstructor}, in \texttt{Syntx.v}, line 122\\
\texttt{jEqOp}, in \texttt{Syntx.v}, line 34\\
\texttt{jExpr}, in \texttt{Syntx.v}, line 44\\
\texttt{jExpr_ind2}, in \texttt{Syntx.v}, line 87\\
\texttt{jMethod}, in \texttt{Syntx.v}, line 118\\
\texttt{jSeq}, in \texttt{Syntx.v}, line 109\\
\texttt{jType}, in \texttt{Syntx.v}, line 9\\
\texttt{jVar}, in \texttt{Syntx.v}, line 15\\
\texttt{map_params}, in \texttt{State.v}, line 109\\
\texttt{map_params_aux}, in \texttt{State.v}, line 100\\
\texttt{mbody}, in \texttt{Syntx.v}, line 205\\
\texttt{mkObject}, in \texttt{State.v}, line 62\\
\texttt{mtype}, in \texttt{Syntx.v}, line 185\\
\texttt{object}, in \texttt{State.v}, line 21\\
\texttt{preservation_auto}, in \texttt{Semantics.v}, line 447\\
\texttt{preservation_aux_l}, in \texttt{Semantics.v}, line 848\\
\texttt{result}, in \texttt{Semantics.v}, line 20\\
\texttt{seq_typing}, in \texttt{Typing.v}, line 136\\
\texttt{setValue}, in \texttt{State.v}, line 44\\
\texttt{setValue_aux}, in \texttt{State.v}, line 38\\
\texttt{set_field}, in \texttt{State.v}, line 85\\
\texttt{set_field_aux}, in \texttt{State.v}, line 77\\
\texttt{set_field_aux_def}, in \texttt{State.v}, line 65\\
\texttt{state}, in \texttt{State.v}, line 25\\
\texttt{state_correct}, in \texttt{State.v}, line 159\\
\texttt{state_heap_correct}, in \texttt{State.v}, line 154\\
\texttt{subtype}, in \texttt{Typing.v}, line 12\\
\texttt{val}, in \texttt{State.v}, line 10\\
\texttt{world}, in \texttt{Syntx.v}, line 131

\subsection{Theorems, lemmas, facts and axioms}

\texttt{all_subtype_eq_lengths}, in \texttt{Semantics.v}, line 1029\\
\texttt{all_subtype_sublist}, in \texttt{Semantics.v}, line 890\\
\texttt{aop_is_int}, in \texttt{Semantics.v}, line 1685\\
\texttt{apply_state_aux_map_params_this}, in \texttt{Semantics.v}, line 880\\
\texttt{apply_state_aux_nth_error}, in \texttt{State.v}, line 527\\
\texttt{apply_state_aux_set_value_aux}, in \texttt{Semantics.v}, line 858\\
\texttt{apply_state_aux_set_value_aux_neq}, in \texttt{Semantics.v}, line 868\\
\texttt{apply_state_nth_error}, in \texttt{State.v}, line 541\\
\texttt{cbody_exists}, in \texttt{Semantics.v}, line 3074\\
\texttt{class_has_fields}, in \texttt{Semantics.v}, line 378\\
\texttt{class_in_get_class_some}, in \texttt{Semantics.v}, line 762\\
\texttt{class_in_get_class_some_2}, in \texttt{Semantics.v}, line 841\\
\texttt{class_in_get_class_t_some}, in \texttt{Semantics.v}, line 790\\
\texttt{ctype_cbody_eq_types_sub}, in \texttt{Semantics.v}, line 428\\
\texttt{eval_args_heap_entry_remains}, in \texttt{Semantics.v}, line 742\\
\texttt{eval_heap_entry_remains}, in \texttt{Semantics.v}, line 617\\
\texttt{eval_seq_heap_entry_remains}, in \texttt{Semantics.v}, line 511\\
\texttt{evaluates_args_list_lengths}, in \texttt{Semantics.v}, line 1018\\
\texttt{field_in_heap}, in \texttt{State.v}, line 616\\
\texttt{fields_determ}, in \texttt{State.v}, line 341\\
\texttt{fields_f_fields}, in \texttt{Syntx.v}, line 168\\
\texttt{fields_present_in_new_object}, in \texttt{Semantics.v}, line 1570\\
\texttt{fields_unique_new_object}, in \texttt{Semantics.v}, line 1620\\
\texttt{flat_map_app}, in \texttt{Semantics.v}, line 1544\\
\texttt{get_class_In_W}, in \texttt{Syntx.v}, line 262\\
\texttt{get_class_eq_name}, in \texttt{Syntx.v}, line 247\\
\texttt{get_class_subclass}, in \texttt{Semantics.v}, line 826\\
\texttt{get_class_subclass_t}, in \texttt{Semantics.v}, line 798\\
\texttt{get_field_init_vals_fields_unique}, in \texttt{Semantics.v}, line 1598\\
\texttt{get_fields_init_vals_nth}, in \texttt{Semantics.v}, line 1587\\
\texttt{get_refs_new_object}, in \texttt{Semantics.v}, line 1552\\
\texttt{has_type_dec}, in \texttt{State.v}, line 675\\
\texttt{has_type_subtype}, in \texttt{State.v}, line 431\\
\texttt{map_params_aux_heap_not_changed}, in \texttt{Semantics.v}, line 487\\
\texttt{map_params_heap_not_changed}, in \texttt{Semantics.v}, line 478\\
\texttt{mbody_determ}, in \texttt{Semantics.v}, line 388\\
\texttt{mbody_exists}, in \texttt{Semantics.v}, line 2650\\
\texttt{mtype_mbody_eq_types}, in \texttt{Semantics.v}, line 404\\
\texttt{mtype_mbody_eq_types_aux}, in \texttt{Syntx.v}, line 225\\
\texttt{mtype_mbody_eq_types_sub}, in \texttt{Semantics.v}, line 413\\
\texttt{nat_ind_strong}, in \texttt{Semantics.v}, line 496\\
\texttt{no_refs_in_new_object}, in \texttt{Semantics.v}, line 1561\\
\texttt{nth_error_get_refs_set_value}, in \texttt{State.v}, line 572\\
\texttt{preservation}, in \texttt{Semantics.v}, line 2619\\
\texttt{preservation_aux}, in \texttt{Semantics.v}, line 1692\\
\texttt{preservation_aux_evaluates_seq}, in \texttt{Semantics.v}, line 1041\\
\texttt{preservation_aux_l_add_new_object}, in \texttt{Semantics.v}, line 1667\\
\texttt{preservation_l}, in \texttt{Semantics.v}, line 2661\\
\texttt{progress}, in \texttt{Semantics.v}, line 3082\\
\texttt{progress_seq_aux}, in \texttt{Semantics.v}, line 2691\\
\texttt{progress_seq_aux_Some}, in \texttt{Semantics.v}, line 3061\\
\texttt{set_field_aux_def_field_changed}, in \texttt{State.v}, line 185\\
\texttt{set_field_aux_def_length}, in \texttt{State.v}, line 163\\
\texttt{set_field_aux_def_name_preserved}, in \texttt{State.v}, line 320\\
\texttt{set_field_aux_def_some}, in \texttt{State.v}, line 630\\
\texttt{set_field_aux_obj_field_changed}, in \texttt{State.v}, line 200\\
\texttt{set_field_aux_obj_length}, in \texttt{State.v}, line 176\\
\texttt{set_field_aux_some}, in \texttt{State.v}, line 640\\
\texttt{set_field_fields_unique}, in \texttt{State.v}, line 392\\
\texttt{set_field_get_heap}, in \texttt{State.v}, line 299\\
\texttt{set_field_get_refs}, in \texttt{State.v}, line 209\\
\texttt{set_field_heap_split}, in \texttt{State.v}, line 269\\
\texttt{set_field_some}, in \texttt{Semantics.v}, line 1629\\
\texttt{set_field_some_aux}, in \texttt{State.v}, line 653\\
\texttt{set_field_state_heap_correct}, in \texttt{State.v}, line 441\\
\texttt{set_fields_aux_def_entry_existed}, in \texttt{State.v}, line 354\\
\texttt{set_fields_aux_def_fields_unique}, in \texttt{State.v}, line 371\\
\texttt{set_fields_aux_fields_unique}, in \texttt{State.v}, line 382\\
\texttt{set_value_apply_state_aux}, in \texttt{State.v}, line 547\\
\texttt{state_correct_add_new_object}, in \texttt{Semantics.v}, line 1631\\
\texttt{state_correct_map_params}, in \texttt{Semantics.v}, line 977\\
\texttt{state_correct_map_params_aux}, in \texttt{Semantics.v}, line 906\\
\texttt{state_correct_weaken}, in \texttt{State.v}, line 602\\
\texttt{strong_ind_0}, in \texttt{StrongInduction.v}, line 9\\
\texttt{strong_induction}, in \texttt{StrongInduction.v}, line 26\\
\texttt{strong_induction_le}, in \texttt{StrongInduction.v}, line 19\\
\texttt{subtype_dec}, in \texttt{Typing.v}, line 19\\
\texttt{subtype_trans}, in \texttt{State.v}, line 349\\
\texttt{val_dec}, in \texttt{State.v}, line 17

\chapter{Conclusion}

This thesis presented an object-oriented programming language similar to, and
containing many of the features of the Java programming language. It proposed
a type system and introduced a big-step semantics that closely follows the
behaviour of Java Virtual Machine. It then provided a proof that the defined
type system is safe in the proposed semantics.

The language was, at first, based on Featherweight Java but then it diverted
very far from it, introducing many new features and bringing it much closer to
the actual Java programming language (and not just in terms of syntax but also
typing and, most of all, the semantics -- with the introduction of the state and
values that actually resemble the ones used in Java Virtual Machine).

The presented work is a step towards creating a certified compiler for Java (or
a subset of Java). However, there is still much to
be done to achieve that. It should not be hard to define a typing function and
prove that it produces a type that satisfies the typing relation presented here.
The next steps would include formalizing the target language (Java bytecode),
perhaps also some intermediate languages, defining translating functions from
one language into another, defining semantics for each of those languages and
proving that the translated code evaluates to the same values as the original
code, defining evaluation function for the target language (it can then be
extracted to create a certified virtual machine, there is no point compiling the
code with a certified compiler if it is then executed on the virtual machine
that may not maintain the semantics).

The presented formalization is far from complete, with possible extensions not
only in the syntax (interfaces, access modifiers, enums, exceptions,
annotations) but also in the typing (generic types, arrays, more raw types) and
the semantics (garbage collector, multithreading).

\begin{thebibliography}{9}

\bibitem{fj}
  \textsc{Atsushi Igarashi, Benjamin C. Pierce, Philip Wadler},
  2001,
  \textit{Featherweight Java: A Minimal Core Calculus for Java and GJ},
  ACM Transactions on Programming Languages and Systems,
  Vol. 23, No. 3, 396–450.
\bibitem{eopl}
  \textsc{Daniel P. Friedman, Mitchell Wand},
  2008,
  \textit{Essentials of Programming Languages},
  third edition,
  The MIT Press,
  Cambridge, Massachusetts;
  London, England.
\bibitem{java}
  \textsc{James Gosling, Bill Joy, Guy Steele, Gilad Bracha, Alex Buckley},
  March 2015,
  \textit{The Java Language Specification, Java SE 8 Edition},
  Version 8,
  Available through \texttt{https://docs.oracle.com/javase/specs/index.html}.
\bibitem{toplangs}
  \textsc{Thomas Elliott},
  Nov 15, 2018,
  \textit{The State of the Octoverse: top programming languages of 2018},
  Available through \texttt{
  https://blog.github.com/2018-11-15-state-of-the-octoverse-top-programming-languages/}
  (accessed on Jan 15, 2019).
\bibitem{absint}
  \textsc{AbsInt},
  \textit{Formally verified compilation},
  Available through \texttt{https://www.absint.com/compcert/index.htm} (accessed
  on Jan 28, 2019).
\bibitem{compcert}
  \textsc{Xavier Leroy},
  September 17, 2018,
  \textit{The CompCert C verified compiler. Documentation and user's manual},
  Version 3.4,
  Available through \texttt{http://compcert.inria.fr/man/manual.pdf} (accessed
  on Jan 28, 2019).
\bibitem{jscert}
  \textsc{Martin Bodin, Arthur Charguéraud, Daniele Filaretti, Philippa Gardner,
  Sergio Maffeis, et al..},
  Jan 2014,
  \textit{A Trusted Mechanised JavaScript Specification. POPL 2014 - 41st ACM
  SIGPLAN-SIGACT Symposium on Principles of Programming Languages},
  San Diego, United States.\\
  \textless hal-00910135\textgreater
\bibitem{cakeml}
  \textsc{Ramana Kumar, Magnus O. Myreen, Michael Norrish, Scott Owens},
  January 22-24, 2014,
  \textit{CakeML: A Verified Implementation of ML. POPL 2014 - 41st ACM
  SIGPLAN-SIGACT Symposium on Principles of Programming Languages},
  San Diego, United States.
\bibitem{certicoq}
  \textsc{Abhishek Anand, Andrew W. Appel, Greg Morrisett, Zoe Paraskevopoulou,
  Randy Pollack, Olivier Savary Belanger, Matthieu Sozeau, Matthew Weaver},
  Jan 21, 2017,
  \textit{CertiCoq: A verified compiler for Coq},
  CoqPL workshop,
  Paris.
\bibitem{jinja}
  \textsc{Gerwin Klein, Tobias Nipkow},
  July 2006,
  \textit{A Machine-Checked Model for a {Java}-Like Language, Virtual Machine
  and Compiler},
  ACM Transactions on Programming Languages and Systems,
  Volume 28 Issue 4,
  Pages 619-695,
  New York, United States.
\bibitem{mijava}
  \textsc{Martin Strecker},
  2002,
  \textit{Formal Verification of a Java Compiler in Isabelle},
  Automated Deduction--CADE-18. CADE 2002. Lecture Notes in Computer Science,
  vol 2392.
  Springer, Berlin, Heidelberg.
\bibitem{code}
  \textsc{Mikołaj Nowak},
  September 2019,
  \textit{A Coq formalization of a type system for an object-oriented
  programming language},
  Available through \texttt{https://bitbucket.org/Ivan1pl/prmag/src/master/}.

\end{thebibliography}

\end{document}
