Java is currently one of the most popular programming languages\cite{toplangs}
and the most popular object-oriented programming language. Its huge advantage
over other popular programming languages is that it can be run on any platform,
as long as it has Java virtual machine, without the need for recompilation. That
makes it a perfect language for the still growing market of mobile phones. It is
used in most applications for Android, which is currently the most popular
mobile operating system on smartphones, tablet computers, smartTVs, card
dashboards, smartwatches, personal computers and more. That makes Java one of
the most important programming languages in use, and it will probably remain so
for a long time, given the number of the new applications as well as the old
ones that need to be maintained.

Several reduced versions of Java has been created, their purpose being to make
it possible to formally show some properties of the language. One of the best
known is Featherweight Java\cite{fj}, further
referred to as FJ, which is a minimal core calculus for Java. FJ omits most
features from the original language, leaving a small calculus that is easy to
reason about. It provides classes, methods, fields and inheritance. It mimics
Java's syntax in such way that every FJ class is also a valid Java class.
However, the semantics defined for this language make it very different from the
original Java. All fields are considered to be \texttt{final} (once assigned,
it is not possible to change their value), constructors are used only to assign
values to fields and there is no concept of memory as it uses reductions rather
than evaluates expressions to Java values. That makes the language lose its
imperative characteristics and, in fact, makes it a functional language. The key
property that has been proven about FJ is a type system safety -- compact proofs
are provided for both subject reduction and progress, which proves type
soundness, and there is a separate proof for cast safety.

It is a common practice to use interactive theorem provers like Coq or Isabelle 
to rule out any mistakes in the proofs. The proofs constructed with the
help of such tools are mechanically checked. They also often feature proof
automation which may considerably reduce the size and increase the readability
of such proof.

Proving type system safety is often but a first step to a larger goal -- making
a certified compiler. In recent years the development of certified compilers
is becoming more and more popular and they are starting to be used for compiling
commercial applications\cite{absint}. The reason for that is that with normal,
non-certified compiler, a valid and correct program may sometimes behave
incorrectly not due to program developer's error but because of a bug in the
compiler. Certified compilers prevent that from happening, as it has been proven
that they retain the semantics of the source language after the transformation
to the target language. They are therefore perfect for compiling life-critical
programs, where developers want to be sure that their programs will behave
exactly as specified.

The first and best known among those is a CompCert C compiler\cite{compcert} --
a certified compiler for a huge subset of C language (ISO C99). It supports
almost all of ISO C99 with very few exceptions. Many stable versions have been
released and CompCert C is already used in commercial software\cite{absint}.
Compilers for other languages exist or are under development, including
compilers for JavaScript\cite{jscert}, ML\cite{cakeml} and Coq\cite{certicoq}.

There are many formalizations of the subsets of Java but most of them focus just
on a few features and omit the rest of the language. There exists a certified
compiler for a large subset of Java, called Jinja\cite{jinja}, verified in
Isabelle/HOL. Jinja supports exception handling but omits constructors, treats
assignments as statements instead of expressions (they have no value and cannot
be the part of other expressions, for example
\texttt{e1 = (e2 = e3 = v) + (e4 = e5 = w);} is not a valid Jinja expression
while it is valid in Java), downcasts are not allowed (it is not possible to
cast an expression from supertype to its subtype), only one local variable is
allowed in a block (but blocks can be nested to surpass this limitation), there
are no logical operations except for comparison and the only arithmetic
operation is addition. There are several more formalizations of large subsets of
Java, including $\mu$Java\cite{mijava}, but none of them is as extensive as
Jinja and none of them is formalized in Coq.

The purpose of this work is to formalize the type system and semantics for a
significantly larger subset of Java and to prove type system safety for such
language in Coq. This is a step towards making a certified Java compiler.

The language presented in this work extends FJ with two of Java's raw types
(\texttt{int} and \texttt{boolean}) together with arithmetic and logical
operations defined on them, instruction sequences, assignments, local variables,
conditionals and loops. It also adds the concept of memory mimicing the one used
in Java -- method arguments and local variables are put on the stack, objects
are put on the heap. Due to that, many computation states cannot be represented
as expressions, making it impossible to use reductions. The semantics defined
for the language therefore allows to evaluate expressions to Java values
(integers, booleans, null values and references to objects on the heap). This
extended language will further be called Extended Featherweight Java, or EFJ.

The subset of Java selected for EFJ is similar to that of Jinja. It does not
support exception handling but includes complex constructors (they can contain
instruction sequences), allows for chain assignments mentioned before,
downcasts are allowed, the number of local variables is not limited and there
are many more logical and arithmetic operations. But the main difference is
visible in the approach: Jinja defines both big-step and small-step semantics
and reasons on both of them. There are also some differences in the abstract
language. In Jinja, statements are treated like expressions which have no value
while \texttt{if} statement may have a value (if the chosen branch has one).

This work aims to be the first to formalize such a large subset of Java in Coq.
It defines abstract syntax, typing rules and big-step semantics of EFJ.
Computation state is also formalized and follows that used in Java Virtual
Machine. The formalization includes a proof for type preservation and shows that
well-typed programs do not go wrong. It also shows that the correctness of
computation state is preserved. It makes extensive use of Coq's Inductive types
and avoids using coinduction by annotating semantics with recursion depth.
Custom induction principles are often used, as those generated by Coq are too
weak. Most of the proofs presented in this thesis have a simple inductive
structure but some of them are very large (because of the amount of cases needed
by the induction principle, as well as the complexity of some of the cases).

The rest of this work is structured as follows. Chapter 2 describes the
language used in this work: its syntax, typing rules and semantics. Chapter
3 shows the structure of the type safety proof, as if it was done on paper.
Chapter 4 explains the way it was done in Coq and Chapter 5 gives a brief
conclusion of the whole thesis. 
