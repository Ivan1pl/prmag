Require Import String.
Require Import Int.
Require Import List.
Require Import Utf8.
Require Import Omega.

Require Import Syntx.
Require Import Typing.

Inductive val : Set :=
| IntVal  : int → val
| BoolVal : bool → val
| NullVal : val
| RefVal  : string → nat → val
.

Fact val_dec : ∀ v1 v2 : val, {v1 = v2} + {v1 ≠ v2}.
  intros. repeat (decide equality).
Qed.

Inductive object : Set :=
| ObjectDef : string → list (string * val) → object
.

Record state : Set := mkState { st : list (string * val); heap : list object }.

Fixpoint apply_state_aux (l : list (string * val)) (i : string) : option val :=
  match l with
  | nil          => None
  | (s, v) :: ls => if string_dec s i then Some v else apply_state_aux ls i
  end.

Definition apply_state (s : state) (i : string) : option val :=
  apply_state_aux (st s) i.

Definition EmptyState : state := mkState nil nil.

Fixpoint setValue_aux (l : list (string * val)) (x : string) (v : val) : list (string * val) :=
  match l with
  | nil           => (x, v) :: nil
  | (s, v') :: ls => if string_dec s x then (s, v) :: ls else (s, v') :: (setValue_aux ls x v)
  end.

Definition setValue (s : state) (x : string) (v : val) : state :=
  mkState (setValue_aux (st s) x v) (heap s).

Definition addObject (s : state) (o : object) : state :=
  mkState (st s) ((heap s) ++ (o :: nil)).

Definition get_fields_init_vals (f : list jVar) : list (string * val) :=
  map
    (λ v : jVar, match v with
                 | Var t n =>
                     (n,
                     match t with
                     | Named _ => NullVal
                     | Int => IntVal Z_as_Int._0
                     | Boolean => BoolVal false
                     end)
                 end) f.

Definition mkObject (W : world) (c : string) (f : list jVar) : object :=
  ObjectDef c (get_fields_init_vals f).

Fixpoint set_field_aux_def (l : list (string * val)) (s : string) (v : val)
  : option (list (string * val)) :=
  match l with
  | nil          => None
  | (i, x) :: fs => if string_dec i s
                    then Some ((i, v) :: fs)
                    else match set_field_aux_def fs s v with
                         | Some fs => Some ((i, x) :: fs)
                         | None    => None
                         end
  end.

Definition set_field_aux (o : object) (s : string) (v : val) : option object :=
  match o with
  | ObjectDef i l => match set_field_aux_def l s v with
                     | Some l => Some (ObjectDef i l)
                     | None   => None
                     end
  end.

Fixpoint set_field (h : list object) (i : nat) (s : string) (v : val) : option (list object) :=
  match h with
  | nil     => None
  | o :: os => match i with
               | 0%nat => match set_field_aux o s v with
                          | Some o => Some (o :: os)
                          | None   => None
                          end
               | S i   => match set_field os i s v with
                          | Some os => Some (o :: os)
                          | None    => None
                          end
               end
  end.

Fixpoint map_params_aux (s : state) (params : list jVar) (args : list val) : state :=
  match (params, args) with
  | (nil, _)           => mkState nil (heap s)
  | (_, nil)           => mkState nil (heap s)
  | (p :: ps, v :: vs) => match p with
                          | Var _ n => setValue (map_params_aux s ps vs) n v
                          end
  end.

Definition map_params (s : state) (thisVal : val) (params : list jVar) (args : list val) : state :=
  setValue (map_params_aux s params args) "this" thisVal.

Section state.
  Variable W : world.

  Hypothesis Hfield_decl_unique : ∀ C f n n' v t t',
    In C W → fields W C f → nth_error f n = Some (Var t v) → nth_error f n' = Some (Var t' v) → n = n'.

  Notation "'S[' C '<::' D ']'" := (subtype W C D).

  Notation "∅" := (empty_typing_env).
  Notation "Γ ',+' x ':→' τ" := (extend_typing_env Γ x τ) (at level 45, left associativity).
  Notation "Γ ',+*' xs" := (extend_typing_env_multi Γ xs) (at level 40, left associativity).

  Definition has_type (v : val) (t : jType) : Prop :=
    match v with
    | IntVal _   => match t with
                    | Int => True
                    | _   => False
                    end
    | BoolVal _  => match t with
                    | Boolean => True
                    | _       => False
                    end
    | NullVal    => match t with
                    | Named _ => True
                    | _       => False
                    end
    | RefVal c i => S[ Named c <:: t ]
    end.

  Definition get_refs (s : state) : list (string * val) :=
    (st s) ++ (flat_map (fun o => match o with ObjectDef _ l => l end) (heap s)).

  Definition fields_unique (l : list object) : Prop :=
    ∀ s ls n n' f v v', In (ObjectDef s ls) l → nth_error ls n = Some (f, v) → nth_error ls n' = Some (f, v') → n = n'.

  Definition heap_correct (l : list object) : Prop :=
    (∀ s ls C fs, In (ObjectDef s ls) l →
                 get_class W s = Some C →
                 fields W C fs →
                 (∀ t f, In (Var t f) fs → ∃ n v, nth_error ls n = Some (f, v) ∧ has_type v t)) ∧
    fields_unique l.

  Definition state_heap_correct (s : state) : Prop :=
    (∀ k x i n, nth_error (get_refs s) k = Some (x, RefVal i n) →
              ∃ l, nth_error (heap s) n = Some (ObjectDef i l)) ∧
    heap_correct (heap s).

  Definition state_correct (Γ : string → option jType) (s : state) : Prop :=
    (∀ i τ, Γ i = Some τ → ∃ v, apply_state s i = Some v ∧ has_type v τ) ∧
    state_heap_correct s.

  Lemma set_field_aux_def_length : ∀ l l' i v,
    set_field_aux_def l i v = Some l' →
    length l = length l'.
  Proof.
    induction l; intros.
    inversion H.
    destruct a. simpl in H.
    destruct (string_dec s i).
    - inversion H. reflexivity.
    - destruct (set_field_aux_def l i v) eqn:Heq; inversion H.
      simpl. erewrite IHl; eauto.
  Qed.

  Lemma set_field_aux_obj_length : ∀ s s' l l' i v,
    set_field_aux (ObjectDef s l) i v = Some (ObjectDef s' l') →
    length l = length l'.
  Proof.
    intros. simpl in H.
    destruct (set_field_aux_def l i v) eqn:Heq; inversion H; subst.
    eapply set_field_aux_def_length; eauto.
  Qed.

  Lemma set_field_aux_def_field_changed : ∀ l l' i v n,
    set_field_aux_def l i v = Some l' →
    nth_error l' n = nth_error l n ∨ nth_error l' n = Some (i, v).
  Proof.
    induction l; intros.
    inversion H.
    destruct a. simpl in H.
    destruct (string_dec s i).
    - inversion H. subst.
      destruct n; [right|left]; reflexivity.
    - destruct (set_field_aux_def l i v) eqn:Heq; inversion H.
      destruct n; [left; reflexivity|simpl].
      eapply IHl; eauto.
  Qed.

  Lemma set_field_aux_obj_field_changed : ∀ s s' l l' i v n,
    set_field_aux (ObjectDef s l) i v = Some (ObjectDef s' l') →
    nth_error l' n = nth_error l n ∨ nth_error l' n = Some (i, v).
  Proof.
    intros. simpl in H.
    destruct (set_field_aux_def l i v) eqn:Heq; inversion H; subst.
    apply set_field_aux_def_field_changed. assumption.
  Qed.

  Lemma set_field_get_refs : ∀ s n i v hp k,
    set_field (heap s) n i v = Some hp →
    nth_error (get_refs (mkState (st s) hp)) k = nth_error (get_refs s) k ∨
      nth_error (get_refs (mkState (st s) hp)) k = Some (i, v).
  Proof.
    destruct s. simpl. induction heap0; intros.
    - inversion H.
    - destruct n.
      + simpl in H.
        unfold get_refs. simpl.
        destruct (Compare_dec.le_lt_dec (length st0) k).
        * repeat (rewrite nth_error_app2; [|assumption]).
          destruct a.
          destruct (Compare_dec.le_lt_dec (length l0) (k - length st0)).
          { left. rewrite nth_error_app2; [|assumption].
            destruct (set_field_aux (ObjectDef s l0) i v) eqn:Heq; inversion H.
            simpl. destruct o.
            apply set_field_aux_obj_length in Heq. rewrite Heq in l1.
            rewrite nth_error_app2; [|assumption]. rewrite Heq.
            reflexivity. }
          { rewrite nth_error_app1; [|assumption].
            destruct (set_field_aux (ObjectDef s l0) i v) eqn:Heq; inversion H.
            simpl. destruct o.
            assert (length l0 = length l2).
            { apply set_field_aux_obj_length in Heq. assumption. }
            rewrite H0 in l1. repeat (rewrite nth_error_app1; [|assumption]).
            eapply set_field_aux_obj_field_changed; eauto. }
        * left.
          repeat (rewrite nth_error_app1; [|assumption]). reflexivity.
      + simpl in H.
        destruct (set_field heap0 n i v) eqn:Heq; inversion H.
        unfold get_refs; destruct a; simpl.
        destruct (Compare_dec.le_lt_dec (length st0) k).
        * repeat (rewrite (nth_error_app2 st); [|assumption]).
          destruct (Compare_dec.le_lt_dec (length l0) (k - length st0)).
          { repeat (rewrite nth_error_app2; [|assumption]).
            destruct (IHheap0 n i v l (k - length l0)%nat Heq); [left|right].
            { assert (length st0 ≤ k - length l0). { omega. }
              unfold get_refs in H0. simpl in H0.
              repeat (rewrite nth_error_app2 in H0; [|assumption]).
              assert (k - length st0 - length l0 = k - length l0 - length st0)%nat.
              { omega. }
              rewrite H3. assumption. }
            { assert (length st0 ≤ k - length l0). { omega. }
              unfold get_refs in H0. simpl in H0.
              rewrite nth_error_app2 in H0; [|assumption].
              assert (k - length st0 - length l0 = k - length l0 - length st0)%nat.
              { omega. }
              rewrite H3. assumption. } }
          { rewrite app_assoc. rewrite app_assoc. rewrite nth_error_app1.
            left.
            pattern (nth_error ((st0 ++ l0) ++ flat_map (λ o : object, match o with
                                                  | ObjectDef _ l3 => l3
                                                  end) heap0) k).
            rewrite nth_error_app1. reflexivity.
            rewrite app_length. omega.
            rewrite app_length. omega. }
        * repeat (rewrite nth_error_app1; [|assumption]). left. reflexivity.
  Qed.

  Lemma set_field_heap_split : ∀ s n i v hp,
    set_field (heap s) n i v = Some hp →
    ∃ l1 l2 o o', heap s = l1 ++ o :: l2 ∧ hp = l1 ++ o' :: l2 ∧
                  set_field_aux o i v = Some o' ∧
                  length l1 = n.
  Proof.
    destruct s.
    induction heap0; intros.
    - simpl in H. inversion H.
    - simpl in H. destruct n.
      + destruct (set_field_aux a i v) eqn:Heq.
        exists nil; exists heap0; exists a; exists o; repeat split; simpl; try assumption.
        inversion H. reflexivity.
        inversion H.
      + simpl in IHheap0.
        destruct (set_field heap0 n i v) eqn:Heq.
        simpl.
        destruct (IHheap0 n i v l Heq).
        exists (a :: x).
        destruct H0. exists x0.
        destruct H0. exists x1.
        destruct H0. exists x2.
        destruct H0. destruct H1. destruct H2.
        split. rewrite <- app_comm_cons. rewrite H0. reflexivity.
        split. inversion H. rewrite H1. rewrite <- app_comm_cons. reflexivity.
        split. assumption.
        simpl. rewrite H3. reflexivity.
        inversion H.
  Qed.

  Lemma set_field_get_heap : ∀ s n i v hp k,
    set_field (heap s) n i v = Some hp →
    nth_error hp k = nth_error (heap s) k ∨
      (k = n ∧ ∃ o o', nth_error (heap s) k = Some o ∧
        set_field_aux o i v = Some o' ∧ nth_error hp k = Some o').
  Proof.
    intros. apply set_field_heap_split in H.
    destruct H. destruct H. destruct H. destruct H.
    destruct H. destruct H0. subst. rewrite H.
    destruct H1.
    destruct (Compare_dec.le_lt_dec (length x) k).
    - repeat (rewrite nth_error_app2; [|assumption]).
      destruct (k - length x)%nat eqn:Heq.
      + right. simpl.
        split. subst. omega.
        exists x1. exists x2. auto.
      + left. reflexivity.
    - left. repeat (rewrite nth_error_app1; [|assumption]).
      reflexivity.
  Qed.

  Lemma set_field_aux_def_name_preserved : ∀ l ls n f i x y,
    nth_error l n = Some (f, x) →
    nth_error ls n = Some (i, y) →
    set_field_aux_def l i y = Some ls →
    i = f.
  Proof.
    induction l; intros.
    - destruct n; inversion H.
    - destruct n.
      + simpl in H. inversion H. subst.
        simpl in H1. destruct (string_dec f i).
        subst; reflexivity.
        destruct (set_field_aux_def l i y); inversion H1; subst.
        simpl in H0. inversion H0; subst; reflexivity.
      + simpl in H. destruct a.
        simpl in H1. destruct (string_dec s i).
        inversion H1; subst. simpl in H0. rewrite H0 in H. inversion H; subst; reflexivity.
        destruct (set_field_aux_def l i y) eqn:Heq; inversion H1; subst. clear H1.
        simpl in H0. eapply IHl; eauto.
  Qed.

  Lemma fields_determ : ∀ C f f',
    fields W C f → fields W C f' → f = f'.
  Proof.
    intros C f f' H.
    generalize dependent f'.
    induction H; intros.
    - inversion H. reflexivity.
    - inversion H1; subst.
      rewrite H in H8.
      inversion H8; subst. clear H8.
      apply IHfields in H9. subst. reflexivity.
  Qed.

  Lemma set_fields_aux_def_entry_existed : ∀ l l' n f x v i,
    nth_error l' n = Some (f, x) →
    set_field_aux_def l i v = Some l' →
    ∃ x', nth_error l n = Some (f, x').
  Proof.
    induction l; intros. simpl in H0. inversion H0.
    destruct n. destruct a. simpl in H0.
    destruct (string_dec s i); subst. inversion H0; subst.
    simpl. simpl in H. inversion H; subst. exists v0. reflexivity.
    destruct (set_field_aux_def l i v). inversion H0; subst. simpl.
    simpl in H. inversion H; subst. exists x. reflexivity.
    inversion H0. simpl. destruct a. simpl in H0.
    destruct (string_dec s i); subst. inversion H0; subst. simpl in H.
    exists x. assumption. destruct (set_field_aux_def l i v) eqn:Heq.
    inversion H0; subst. simpl in H. eapply IHl; eauto. inversion H0.
  Qed.

  Lemma set_fields_aux_def_fields_unique : ∀ l l' i v,
    (∀ n n' f v v', nth_error l n = Some (f, v) → nth_error l n' = Some (f, v') → n = n') →
    set_field_aux_def l i v = Some l' →
    (∀ n n' f v v', nth_error l' n = Some (f, v) → nth_error l' n' = Some (f, v') → n = n').
  Proof.
    intros.
    eapply set_fields_aux_def_entry_existed in H1; eauto.
    eapply set_fields_aux_def_entry_existed in H2; eauto.
    destruct H1. destruct H2. eapply H; eauto.
  Qed.

  Lemma set_fields_aux_fields_unique : ∀ l l' s s' i v,
    (∀ n n' f v v', nth_error l n = Some (f, v) → nth_error l n' = Some (f, v') → n = n') →
    set_field_aux (ObjectDef s l) i v = Some (ObjectDef s' l') →
    (∀ n n' f v v', nth_error l' n = Some (f, v) → nth_error l' n' = Some (f, v') → n = n').
  Proof.
    intros. unfold set_field_aux in H0.
    destruct (set_field_aux_def l i v) eqn:Heq. inversion H0; subst.
    eapply set_fields_aux_def_fields_unique; eauto. inversion H0.
  Qed.

  Lemma set_field_fields_unique : ∀ h h' n i v,
    fields_unique h →
    set_field h n i v = Some h' →
    fields_unique h'.
  Proof.
    induction h; intros.
    - simpl in H0. inversion H0.
    - destruct n.
      + simpl in H0. destruct (set_field_aux a i v) eqn:Heq.
        unfold fields_unique. intros. destruct a. unfold fields_unique in H.
        inversion H0; subst.
        simpl in H1. destruct H1. inversion H1; subst.
        eapply set_fields_aux_fields_unique; eauto.
        intros. eapply H; eauto. simpl. left. reflexivity.
        eapply H. simpl. right. apply H1. apply H2. apply H3.
        inversion H0.
      + simpl in H0.
        destruct (set_field h n i v) eqn:Heq; [|simpl in H0; inversion H0].
        inversion H0; subst.
        unfold fields_unique in H. unfold fields_unique.
        intros. destruct H1.
        eapply H; eauto. simpl. left. apply H1.
        eapply IHh; eauto.
        unfold fields_unique. intros. eapply H; eauto.
        simpl. right. apply H4.
  Qed.

  Lemma subtype_trans : ∀ t1 t2 t3,
    S[ t1 <:: t2 ] → S[ t2 <:: t3 ] → S[ t1 <:: t3 ].
  Proof.
    intros t1 t2 t3 H. generalize dependent t3.
    induction H.
    - intros; auto.
    - intros. destruct t3.
      + eapply SubtypeDef; eauto.
      + inversion H1.
      + inversion H1.
  Qed.

  Lemma has_type_subtype : ∀ v t t',
    has_type v t' → S[ t' <:: t] → has_type v t.
  Proof.
    intros. induction H0; auto.
    destruct v; inversion H; subst; simpl; auto.
    eapply SubtypeDef; eauto.
    eapply SubtypeDef. apply H4.
    eapply subtype_trans. apply H5. eapply SubtypeDef; eauto.
  Qed.

  Lemma set_field_state_heap_correct : ∀ s x n l C f t i t' v hp,
    state_heap_correct s →
    has_type (RefVal x n) (Named x) →
    nth_error (heap s) n = Some (ObjectDef x l) →
    get_class W x = Some C →
    fields W C f →
    In (Var t i) f →
    S[ t' <:: t ] →
    has_type v t' →
    set_field (heap s) n i v = Some hp →
    ((∃ j k o, v = RefVal j k ∧ nth_error (heap s) k = Some (ObjectDef j o)) ∨ ∀ j k, v ≠ RefVal j k) →
    state_heap_correct (mkState (st s) hp).
  Proof.
    intros.
    unfold state_heap_correct in *.
    destruct H.
    split.
    - intros. simpl.
      destruct (set_field_get_refs s n i v hp k H7).
      + rewrite H11 in H10.
        destruct (H k x0 i0 n0 H10).
        destruct (set_field_get_heap s n i v hp n0 H7).
        * rewrite H13. eauto.
        * destruct H13. subst. destruct H14. destruct H13. destruct H13.
          assert (Some x2 = Some (ObjectDef i0 x1)).
          { rewrite <- H13. rewrite <- H12. reflexivity. }
          inversion H15. subst. clear H15.  destruct H14. simpl in H14.
          destruct (set_field_aux_def x1 i v); inversion H14.
          rewrite <- H17 in H15. eauto.
      + rewrite H11 in H10. inversion H10. subst. clear H10.
        destruct H8.
        destruct H8. destruct H8. destruct H8. destruct H8. inversion H8; subst. clear H8.
        destruct (set_field_get_heap s n x0 (RefVal x1 x2) hp x2 H7).
        rewrite H8. eauto.
        destruct H8. subst. destruct H12. destruct H8. destruct H8.
        rewrite H10 in H8. inversion H8; subst.
        destruct H12. simpl in H12.
        destruct (set_field_aux_def x3 x0 (RefVal x1 n)); inversion H12; subst. eauto.
        exfalso. eapply H8. eauto.
    - simpl. unfold heap_correct. split. intros.
      unfold heap_correct in H9. destruct H9. clear H14.
      edestruct H9. eapply nth_error_In. eauto. eauto. eauto. eauto.
      destruct H14.
      apply In_nth_error in H10. destruct H10.
      destruct (set_field_get_heap s n i v hp x2 H7).
      + rewrite H15 in H10. eapply H9; eauto. eapply nth_error_In; eauto.
      + destruct H15; subst. destruct H14.
        destruct H16. destruct H16. destruct H16. destruct H17.
        destruct x2. destruct x3.
        assert (Some (ObjectDef s0 ls) = Some (ObjectDef s2 l1)).
        { rewrite <- H10. rewrite <- H18. reflexivity. }
        inversion H19; subst. clear H19. clear H18.
        assert (Some (ObjectDef x l) = Some (ObjectDef s1 l0)).
        { rewrite <- H1. rewrite <- H16. reflexivity. }
        inversion H18; subst. clear H18. clear H1.
        simpl in H17.
        destruct (set_field_aux_def l0 i v) eqn:Heq; inversion H17; subst.
        rewrite H2 in H11. inversion H11; subst. clear H11. clear H17.
        destruct (string_dec i f0); subst.
        * assert (f = fs). { eapply fields_determ; eauto. }
          assert (t0 = t).
          { subst.
            destruct (In_nth_error fs (Var t f0) H4).
            destruct (In_nth_error fs (Var t0 f0) H13).
            assert (x = x2). { eapply Hfield_decl_unique; eauto.
            eapply get_class_In_W; eauto. }
            subst. rewrite H1 in H11. inversion H11; subst. reflexivity. }
          subst. destruct (set_field_aux_def_field_changed l0 l1 f0 v x0 Heq).
          rewrite <- H1 in H14. eauto.
          exists x0. exists v. split; auto.
          eapply has_type_subtype; eauto.
        * assert (f = fs). { eapply fields_determ; eauto. }
          subst.
          edestruct (H9 s2 l0 C0 fs); auto.
          eapply nth_error_In; eauto.
          apply H13.
          destruct H1.
          destruct (set_field_aux_def_field_changed l0 l1 i v x Heq).
          rewrite <- H11 in H1; eauto.
          destruct H1.
          assert (i = f0). { eapply set_field_aux_def_name_preserved; eauto. }
          contradiction.
      + eapply set_field_fields_unique; eauto.
        destruct H9. assumption.
  Qed.

  Lemma apply_state_aux_nth_error : ∀ s h i v,
    apply_state_aux s i = Some v → ∃ n, nth_error (get_refs {| st := s; heap := h |}) n = Some (i, v).
  Proof.
    induction s; intros.
    - inversion H.
    - destruct a. simpl in H.
      destruct (string_dec s0 i).
      + inversion H. subst. clear H.
        exists 0%nat. reflexivity.
      + apply (IHs h) in H. destruct H.
        exists (S x).
        simpl. unfold get_refs in H. simpl in H. apply H.
  Qed.

  Lemma apply_state_nth_error : ∀ s i v,
    apply_state s i = Some v → ∃ n, nth_error (get_refs s) n = Some (i, v).
  Proof.
    destruct s. unfold apply_state. simpl. apply apply_state_aux_nth_error.
  Qed.

  Lemma set_value_apply_state_aux : ∀ st i v v' s τ,
    apply_state_aux st i = Some v →
    has_type v τ →
    (has_type v' τ ∨ s ≠ i) →
    ∃ v'', apply_state_aux (setValue_aux st s v') i = Some v'' ∧ has_type v'' τ.
  Proof.
    intro st. induction st; intros.
    - simpl in H. inversion H.
    - destruct a. destruct H1.
      + simpl in H. simpl.
        destruct (string_dec s0 s); subst. simpl.
        destruct (string_dec s i); subst. exists v'; auto.
        exists v; auto.
        simpl.
        destruct (string_dec s0 i); subst. inversion H; subst. exists v; auto.
        eapply IHst; eauto.
      + simpl.
        destruct (string_dec s0 s); subst. simpl. simpl in H.
        destruct (string_dec s i); subst. contradiction.
        exists v; auto.
        simpl in H. simpl.
        destruct (string_dec s0 i); subst. inversion H; subst. exists v; auto.
        eapply IHst; eauto.
  Qed.

  Lemma nth_error_get_refs_set_value : ∀ st s v k x,
    nth_error (get_refs (setValue st s v)) k = Some x →
    (∃ n, nth_error (get_refs st) n = Some x) ∨ x = (s, v).
  Proof.
    intro st. destruct st.
    induction st0; intros.
    - unfold setValue in H. simpl in H.
      unfold get_refs in H. simpl in H.
      unfold get_refs. simpl.
      destruct k.
      simpl in H. right. inversion H. reflexivity.
      simpl in H. left. eauto.
    - unfold setValue in H. simpl in H. destruct a.
      destruct (string_dec s0 s); subst.
      unfold get_refs in H. simpl in H.
      unfold get_refs. simpl.
      destruct k.
      simpl in H. right. inversion H. reflexivity.
      simpl in H. left. exists (S k). simpl. auto.
      destruct k.
      simpl in H. left. exists 0%nat.
      unfold get_refs. simpl. assumption.
      unfold get_refs in H. simpl in H.
      unfold get_refs. simpl.
      destruct (IHst0 s v k x).
      unfold get_refs. simpl. assumption.
      unfold get_refs in H0. destruct H0. simpl in H0. left. exists (S x0). simpl. auto.
      right. assumption.
  Qed.

  Lemma state_correct_weaken : ∀ Γ x τ s,
    Γ x = None →
    state_correct (Γ,+ x :→ τ) s →
    state_correct Γ s.
  Proof.
    intros.
    destruct H0; split; auto.
    intros.
    destruct (H0 i τ0); [|eauto].
    unfold extend_typing_env.
    destruct (string_dec i x); subst; auto.
    rewrite H in H2. inversion H2.
  Qed.

  Lemma field_in_heap : ∀ s n d l x i m,
    nth_error (heap s) n = Some (ObjectDef d l) →
    In (x, RefVal i m) l →
    ∃ k, nth_error (get_refs s) k = Some (x, RefVal i m).
  Proof.
    intros.
    assert (∃ y, In y (heap s) ∧ In (x, RefVal i m) ((λ o : object, match o with | ObjectDef _ l => l end) y)).
    { exists (ObjectDef d l). split; auto. eapply nth_error_In; eauto. }
    apply in_flat_map in H1.
    unfold get_refs. apply In_nth_error in H1. destruct H1.
    exists (x0 + (length (st s)))%nat. rewrite nth_error_app2.
    rewrite Nat.add_sub. apply H1. omega.
  Qed.

  Lemma set_field_aux_def_some : ∀ k l x v v',
    nth_error l k = Some (x, v') →
    ∃ l', set_field_aux_def l x v = Some l'.
  Proof.
    induction k; intros; destruct l; simpl in H; inversion H; subst; simpl;
    destruct (string_dec x x); eauto; try contradiction.
    destruct p. destruct (string_dec s x); subst; eauto.
    edestruct (IHk l x v); eauto. rewrite H0. eauto.
  Qed.

  Lemma set_field_aux_some : ∀ h d l D f τ1 x v,
    (∃ (n : nat) (v' : val), nth_error l n = Some (x, v') ∧ has_type v' τ1) →
    In (ObjectDef d l) h →
    get_class W d = Some D →
    fields W D f →
    In (Var τ1 x) f →
    ∃ o, set_field_aux (ObjectDef d l) x v = Some o.
  Proof.
    intros. destruct H. destruct H. destruct H.
    edestruct (set_field_aux_def_some x0 l x v); eauto.
    simpl. rewrite H5. eauto.
  Qed.

  Lemma set_field_some_aux : ∀ n h d l D f τ1 x v,
    (∃ (n : nat) (v' : val), nth_error l n = Some (x, v') ∧ has_type v' τ1) →
    nth_error h n = Some (ObjectDef d l) →
    get_class W d = Some D →
    fields W D f →
    In (Var τ1 x) f →
    has_type v τ1 →
    ∃ h', set_field h n x v = Some h'.
  Proof.
    induction n; intros.
    - destruct h eqn:Heq. simpl in H0. inversion H0.
      simpl. simpl in H0. inversion H0. clear H0. rewrite H6 in Heq.
      clear H6.
      assert (In (ObjectDef d l) h).
      { rewrite Heq. simpl. left. reflexivity. }
      destruct (set_field_aux_some h d l D f τ1 x v); auto.
      rewrite H5. eauto.
    - destruct h eqn:Heq. simpl in H0. inversion H0.
      simpl. simpl in H0.
      edestruct IHn; eauto. rewrite H5. eauto.
  Qed.

  Fact has_type_dec : ∀ v τ, { has_type v τ } + { ¬ has_type v τ }.
  Proof.
    intros. destruct v; destruct τ;
    try (left; simpl; auto; fail);
    try (right; intro; inversion H; fail).
    simpl. apply subtype_dec.
  Qed.
End state.
