Require Import PeanoNat.

Section StrongInduction.

  Variable P:nat -> Prop.

  Hypothesis IH : forall m, (forall n, n < m -> P n) -> P m.

  Lemma strong_ind_0 : P 0.
  Proof.
    apply IH; intros.
    exfalso; inversion H.
  Qed.

  Hint Resolve strong_ind_0.

  Hint Resolve le_S_n.

  Lemma strong_induction_le : forall n,
      (forall m, m <= n -> P m).
  Proof.
    induction n; intros;
    inversion H; auto.
  Qed.

  Theorem strong_induction : forall n, P n.
  Proof.
    intro. induction n; auto.
    eapply IH; eauto.
    intros. eapply strong_induction_le; eauto.
  Qed.

End StrongInduction.

Tactic Notation "strong" "induction" ident(n) := induction n using strong_induction.