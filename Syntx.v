Require Import String.
Require Import Int.
Require Import List.
Require Import Utf8.
Require Import Coq.Program.Equality.

Definition int : Set := Z_as_Int.t.

Inductive jType : Set :=
| Named   : string → jType
| Int     : jType
| Boolean : jType
.

Inductive jVar : Set :=
| Var : jType → string → jVar
.

Inductive jAOp : Set :=
| Plus  : jAOp
| Minus : jAOp
| Times : jAOp
| Div   : jAOp
| Mod   : jAOp
.

Inductive jCOp : Set :=
| Lt : jCOp
| Le : jCOp
| Gt : jCOp
| Ge : jCOp
.

Inductive jEqOp : Set :=
| Eq : jEqOp
| Ne : jEqOp
.

Inductive jBOp : Set :=
| And : jBOp
| Or  : jBOp
.

Inductive jExpr : Set :=
| Val        : string → jExpr
| FieldAsgn  : jExpr → string → jExpr → jExpr
| Asgn       : string → jExpr → jExpr
| Call       : jExpr → string → list jExpr → jExpr
| New        : string → list jExpr → jExpr
| Super      : string → list jExpr → jExpr
| Field      : jExpr → string → jExpr
| Num        : int → jExpr
| Aop        : jExpr → jExpr → jAOp → jExpr
| UnaryMinus : jExpr → jExpr
| Cop        : jExpr → jExpr → jCOp → jExpr
| EqOp       : jExpr → jExpr → jEqOp → jExpr
| BConst     : bool → jExpr
| Bop        : jExpr → jExpr → jBOp → jExpr
| Not        : jExpr → jExpr
| Null       : jExpr
| Cast       : jType → jExpr → jExpr
.

Section correct_jExpr_ind.
  Variables
    (P : jExpr → Prop) (Q : list jExpr → Prop).
  Hypotheses
    (H0  : ∀ s, P (Val s))
    (H1  : ∀ e1 s e2, P e1 → P e2 → P (FieldAsgn e1 s e2))
    (H2  : ∀ s e, P e → P (Asgn s e))
    (H3  : ∀ e s l, P e → Q l → P (Call e s l))
    (H4  : ∀ s l, Q l → P (New s l))
    (H5  : ∀ s l, Q l → P (Super s l))
    (H6  : ∀ e s, P e → P (Field e s))
    (H7  : ∀ n, P (Num n))
    (H8  : ∀ e1 e2 o, P e1 → P e2 → P (Aop e1 e2 o))
    (H9  : ∀ e, P e → P (UnaryMinus e))
    (H10 : ∀ e1 e2 o, P e1 → P e2 → P (Cop e1 e2 o))
    (H11 : ∀ e1 e2 o, P e1 → P e2 → P (EqOp e1 e2 o))
    (H12 : ∀ b, P (BConst b))
    (H13 : ∀ e1 e2 o, P e1 → P e2 → P (Bop e1 e2 o))
    (H14 : ∀ e, P e → P (Not e))
    (H15 : P Null)
    (H16 : ∀ t e, P e → P (Cast t e))
    (HL0 : Q nil)
    (HL1 : ∀ e, P e → ∀ l, Q l → Q (cons e l)).
  Fixpoint jExpr_ind2 (e : jExpr) : P e.
    destruct e.
    - apply H0.
    - apply H1; apply jExpr_ind2.
    - apply H2; apply jExpr_ind2.
    - apply H3. apply jExpr_ind2. induction l. apply HL0. apply HL1. apply jExpr_ind2. apply IHl.
    - apply H4. induction l. apply HL0. apply HL1. apply jExpr_ind2. apply IHl.
    - apply H5. induction l. apply HL0. apply HL1. apply jExpr_ind2. apply IHl.
    - apply H6. apply jExpr_ind2.
    - apply H7.
    - apply H8; apply jExpr_ind2.
    - apply H9; apply jExpr_ind2.
    - apply H10; apply jExpr_ind2.
    - apply H11; apply jExpr_ind2.
    - apply H12.
    - apply H13; apply jExpr_ind2.
    - apply H14; apply jExpr_ind2.
    - apply H15.
    - apply H16; apply jExpr_ind2.
  Defined.
End correct_jExpr_ind.

Inductive jSeq : Set :=
| EndSeq      : jSeq
| Declaration : jVar → jSeq → jSeq
| Expression  : jExpr → jSeq → jSeq
| If          : jExpr → jSeq → jSeq → jSeq → jSeq
| While       : jExpr → jSeq → jSeq → jSeq
| Return      : jExpr → jSeq
.

Inductive jMethod : Set :=
| Method : jType → string → list jVar → jSeq → jMethod
.

Inductive jConstructor : Set :=
| Constructor : list jVar → jSeq → jConstructor
.

Inductive jClass : Set :=
| Object : jClass
| Class  : string → string → list jVar → jConstructor → list jMethod → jClass
.

Definition world : Set := list jClass.

Fixpoint get_class (W : world) (n : string) : option jClass :=
  match W with
  | nil     => None
  | c :: cs => match c with
               | Object          => if string_dec n "Object" then Some c else get_class cs n
               | Class i _ _ _ _ => if string_dec n i then Some c else get_class cs n
               end
  end.

Inductive fields (W : world) : jClass → list jVar → Prop :=
| FieldsObject : fields W Object nil
| FieldsClass  : ∀ c d D f g k m,
                 get_class W d = Some D →
                 fields W D g →
                 fields W (Class c d f k m) (g ++ f)
.

Fixpoint fields_f (W : world) (j : jClass) (n : nat) : option (list jVar) :=
  match n with
  | 0   => match j with
           | Object          => Some nil
           | Class _ _ _ _ _ => None
           end
  | S n => match j with
           | Object          => Some nil
           | Class _ d f _ _ => match get_class W d with
                                | Some D => match fields_f W D n with
                                            | Some g => Some (g ++ f)
                                            | None   => None
                                            end
                                | None   => None
                                end
           end
  end.

Lemma fields_f_fields : ∀ W j n f,
  fields_f W j n = Some f →
  fields W j f.
Proof.
  intros W j n.
  generalize dependent j.
  generalize dependent W.
  induction n; intros.
  - simpl in H. destruct j. inversion H; subst. constructor.
    inversion H.
  - simpl in H. destruct j. inversion H; subst. constructor.
    destruct (get_class W s0) eqn:Heq.
    destruct (fields_f W j0 n) eqn:Heq2.
    inversion H; subst. econstructor; eauto.
    inversion H. inversion H.
Qed.

Inductive mtype (W : world) (m : string) : jClass → list jType * jType → Prop :=
| MtypeClass : ∀ c d f k ms a t s,
               get_class W c = Some (Class c d f k ms) →
               In (Method t m a s) ms →
               mtype W m (Class c d f k ms) (map (fun x => match x with Var t _ => t end) a, t)
| MtypeSuper : ∀ c d f k ms t l D,
               (∀ t' a s, ¬ In (Method t' m a s) ms) →
               get_class W d = Some D →
               mtype W m D (l, t) →
               mtype W m (Class c d f k ms) (l, t)
.

Definition ctype (c : jClass) : list jType :=
  match c with
  | Object          => nil
  | Class _ _ _ k _ => match k with
                       | Constructor a _ => map (fun x => match x with Var t _ => t end) a
                       end
  end.

Inductive mbody (W : world) (m : string) : jClass → (list jVar * jSeq) → Prop :=
| MbodyClass : ∀ c d f k ms a t s,
               get_class W c = Some (Class c d f k ms) →
               In (Method t m a s) ms →
               mbody W m (Class c d f k ms) (a, s)
| MbodySuper : ∀ c d f k ms D a s,
               (∀ t' a' s', ¬ In (Method t' m a' s') ms) →
               get_class W d = Some D →
               mbody W m D (a, s) →
               mbody W m (Class c d f k ms) (a, s)
.

Definition cbody (c : jClass) : list jVar * jSeq :=
  match c with
  | Object          => (nil, EndSeq)
  | Class _ _ _ k _ => match k with
                       | Constructor a b => (a, b)
                       end
  end.

Lemma mtype_mbody_eq_types_aux :
  ∀ W,
  (∀ m C b b', mbody W m C b → mbody W m C b' → b = b') →
  ∀ m C t b,
    mtype W m C t →
    mbody W m C b →
    map (fun x => match x with Var t _ => t end) (fst b) = (fst t).
Proof.
  intros W HBody m C t b HT.
  generalize dependent b.
  dependent induction HT; intros.
  - simpl.
    apply (MbodyClass W m c d f k) in H0.
    assert ((a, s) = b). { eapply HBody; eauto. }
    subst. simpl. reflexivity. auto.
  - inversion H1; subst.
    apply H in H9. exfalso. auto.
    assert (Some D = Some D0). { rewrite <- H0. rewrite <- H9. reflexivity. }
    inversion H2; subst. clear H2.
    apply IHHT in H10. auto.
Qed.

Lemma get_class_eq_name : ∀ W c C,
  get_class W c = Some C →
  C = Object ∨ ∃ d f k m, C = Class c d f k m.
Proof.
  induction W; intros.
  - inversion H.
  - simpl in H. destruct a.
    destruct (string_dec c "Object"); subst.
    left; inversion H. reflexivity.
    apply IHW; auto.
    destruct (string_dec c s); subst.
    inversion H; subst. right; eauto.
    apply IHW; auto.
Qed.

Lemma get_class_In_W : ∀ W c C,
  get_class W c = Some C →
  In C W.
Proof.
  induction W; intros.
  - simpl in H. inversion H.
  - destruct a; simpl in H.
    + destruct (string_dec c "Object"). inversion H; subst.
      simpl. left. reflexivity.
      simpl. right. eapply IHW; eauto.
    + destruct (string_dec c s). inversion H; subst.
      simpl. left. reflexivity.
      simpl. right. eapply IHW; eauto.
Qed.
