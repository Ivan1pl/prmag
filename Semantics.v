Require Import String.
Require Import Int.
Require Import List.
Require Import Utf8.
Require Import Omega.
Require Import Coq.Program.Equality.

Require Import Syntx.
Require Import Typing.
Require Import State.
Require Import StrongInduction.

Inductive exc : Set :=
| NullPointerException       : exc
| DivisionByZeroException    : exc
| ClassCastException         : exc
| TimeLimitExceededException : exc
.

Inductive result : Set :=
| ResData : val → result
| ResExc  : exc → result
.

Open Scope Z_scope.

Definition aop (v1 : int) (v2 : int) (o : jAOp) : result :=
  match o with
  | Plus  => ResData (IntVal (v1 + v2))
  | Minus => ResData (IntVal (v1 - v2))
  | Times => ResData (IntVal (v1 * v2))
  | Div   => if (Z_as_Int.eq_dec v2 0) then ResExc DivisionByZeroException else ResData (IntVal (v1 / v2))
  | Mod   => if (Z_as_Int.eq_dec v2 0) then ResExc DivisionByZeroException else ResData (IntVal (v1 mod v2))
  end.

Definition cop (v1 : int) (v2 : int) (o : jCOp) : bool :=
  match o with
  | Lt => if Z_lt_ge_dec v1 v2 then true else false
  | Le => if Z_le_gt_dec v1 v2 then true else false
  | Gt => if Z_gt_le_dec v1 v2 then true else false
  | Ge => if Z_ge_lt_dec v1 v2 then true else false
  end.

Definition eqOp (v1 : val) (v2 : val) (o : jEqOp) : bool :=
  match v1 with
  | IntVal _
  | BoolVal _
  | NullVal    => if val_dec v1 v2 then (if o then true else false)
                                   else (if o then false else true)
  | RefVal _ n => match v2 with
                  | RefVal _ n' => if Nat.eq_dec n n' then (if o then true else false)
                                                      else (if o then false else true)
                  | _           => if o then false else true
                  end
  end.

Section evaluation.
  Variable W : world.

  Notation "'S[' C '<::' D ']'" := (subtype W C D).
  Notation "'T[' Γ '⊢' e ':→' τ ']'" := (expr_typing W Γ e τ).
  Notation "'T[' Γ '⊢' s ':→→' τ ']'" := (seq_typing W Γ s τ).

  Notation "∅" := (empty_typing_env).
  Notation "Γ ',+' x ':→' τ" := (extend_typing_env Γ x τ) (at level 45, left associativity).
  Notation "Γ ',+*' xs" := (extend_typing_env_multi Γ xs) (at level 40, left associativity).

  Reserved Notation "'E[' St0 '⊢' e '⇓' v '|' St '@' t ']'".

  Definition has_type (v : val) (t : jType) : Prop := has_type W v t.
  Definition state_correct (Γ : string → option jType) (s : state) : Prop :=
    state_correct W Γ s.

  Inductive evaluates_expr : state → jExpr → result → state → nat → Prop :=
  | E_Var           : ∀ s x v t,
                      apply_state s x = Some v →
                      E[ s ⊢ Val x ⇓ ResData v | s @ t ]
  | E_FieldAsgn     : ∀ s s' s'' hp e1 c i e2 v f t,
                      E[ s ⊢ e1 ⇓ ResData (RefVal c i) | s' @ t ] →
                      E[ s' ⊢ e2 ⇓ ResData v | s'' @ t ] →
                      set_field (heap s'') i f v = Some hp →
                      E[ s ⊢ FieldAsgn e1 f e2 ⇓ ResData v | mkState (st s'') hp @ t ]
  | E_FieldAsgn_Npe : ∀ s s' e1 e2 f t,
                      E[ s ⊢ e1 ⇓ ResData NullVal | s' @ t ] →
                      E[ s ⊢ FieldAsgn e1 f e2 ⇓ ResExc NullPointerException | s' @ t ]
  | E_FAsgn_Exc_1   : ∀ s s' s'' e1 c i e2 e f t,
                      E[ s ⊢ e1 ⇓ ResData (RefVal c i) | s' @ t ] →
                      E[ s' ⊢ e2 ⇓ ResExc e | s'' @ t ] →
                      E[ s ⊢ FieldAsgn e1 f e2 ⇓ ResExc e | s'' @ t ]
  | E_FAsgn_Exc_2   : ∀ s s' e1 e2 e f t,
                      E[ s ⊢ e1 ⇓ ResExc e | s' @ t ] →
                      E[ s ⊢ FieldAsgn e1 f e2 ⇓ ResExc e | s' @ t ]
  | E_Asgn          : ∀ s s' x e v t,
                      E[ s ⊢ e ⇓ ResData v | s' @ t ] →
                      E[ s ⊢ Asgn x e ⇓ ResData v | setValue s' x v @ t ]
  | E_Asgn_Exc      : ∀ s s' x e e' t,
                      E[ s ⊢ e ⇓ ResExc e' | s' @ t ] →
                      E[ s ⊢ Asgn x e ⇓ ResExc e' | s' @ t ]
  | E_Call          : ∀ s s' s'' s''' s'''' s''''' e c i m args params vs C b v t,
                      E[ s ⊢ e ⇓ ResData (RefVal c i) | s' @ S t ] →
                      evaluates_args s' args vs s'' t →
                      get_class W c = Some C →
                      mbody W m C (params, b) →
                      map_params s'' (RefVal c i) params vs = s''' →
                      evaluates_seq s'''  b (Some v) s'''' t →
                      mkState (st s'') (heap s'''') = s''''' →
                      E[ s ⊢ Call e m args ⇓ v | s''''' @ S t ]
  | E_Call_Npe      : ∀ s s' e m args t,
                      E[ s ⊢ e ⇓ ResData NullVal | s' @ t ] →
                      E[ s ⊢ Call e m args ⇓ ResExc NullPointerException | s' @ t ]
  | E_Call_Exc_1    : ∀ s s' s'' e c i m args ex t v,
                      E[ s ⊢ e ⇓ ResData (RefVal c i) | s' @ S t ] →
                      evaluates_args_exc s' args ex v s'' t →
                      E[ s ⊢ Call e m args ⇓ ResExc ex | s'' @ S t ]
  | E_Call_Exc_2    : ∀ s s' e m args ex t,
                      E[ s ⊢ e ⇓ ResExc ex | s' @ t ] →
                      E[ s ⊢ Call e m args ⇓ ResExc ex | s' @ t ]
  | E_Call_Exc_3    : ∀ s s' e c i m args,
                      E[ s ⊢ e ⇓ ResData (RefVal c i) | s' @ 0%nat ] →
                      E[ s ⊢ Call e m args ⇓ ResExc TimeLimitExceededException | s' @ 0%nat ]
  | E_New           : ∀ s s' s'' s''' s'''' s''''' c args params vs C b t f,
                      evaluates_args s args vs s' t →
                      get_class W c = Some C →
                      cbody C = (params, b) →
                      fields W C f →
                      addObject s' (mkObject W c f) = s'' →
                      map_params s'' (RefVal c (length (heap s'))) params vs = s''' →
                      evaluates_seq s'''  b None s'''' t →
                      mkState (st s'') (heap s'''') = s''''' →
                      E[ s ⊢ New c args ⇓ ResData (RefVal c (length (heap s'))) | s''''' @ S t ]
  | E_New_Exc_1     : ∀ s s' c args ex vs t,
                      evaluates_args_exc s args ex vs s' t →
                      E[ s ⊢ New c args ⇓ ResExc ex | s' @ S t ]
  | E_New_Exc_2     : ∀ s c args,
                      E[ s ⊢ New c args ⇓ ResExc TimeLimitExceededException | s @ 0%nat ]
  | E_New_Exc_3     : ∀ s s' s'' s''' s'''' s''''' c args params vs C b t f ex,
                      evaluates_args s args vs s' t →
                      get_class W c = Some C →
                      cbody C = (params, b) →
                      fields W C f →
                      addObject s' (mkObject W c f) = s'' →
                      map_params s'' (RefVal c (length (heap s'))) params vs = s''' →
                      evaluates_seq s'''  b (Some (ResExc ex)) s'''' t →
                      mkState (st s'') (heap s'''') = s''''' →
                      E[ s ⊢ New c args ⇓ ResExc ex | s''''' @ S t ]
  | E_Super         : ∀ s s' s'' s''' s'''' c c' args params vs D b t d f k m n,
                      apply_state s "this" = Some (RefVal c' n) →
                      get_class W c = Some (Class c d f k m) →
                      evaluates_args s args vs s' t →
                      get_class W d = Some D →
                      cbody D = (params, b) →
                      map_params s' (RefVal c' n) params vs = s'' →
                      evaluates_seq s''  b None s''' t →
                      mkState (st s') (heap s''') = s'''' →
                      E[ s ⊢ Super c args ⇓ ResData (RefVal c' n) | s'''' @ S t ]
  | E_Super_Npe     : ∀ s c args t,
                      apply_state s "this" = Some NullVal →
                      E[ s ⊢ Super c args ⇓ ResExc NullPointerException | s @ t ]
  | E_Super_Exc_1   : ∀ s s' c args ex vs t,
                      evaluates_args_exc s args ex vs s' t →
                      E[ s ⊢ Super c args ⇓ ResExc ex | s' @ S t ]
  | E_Super_Exc_2   : ∀ s c args,
                      E[ s ⊢ Super c args ⇓ ResExc TimeLimitExceededException | s @ 0%nat ]
  | E_Super_Exc_3   : ∀ s s' s'' s''' s'''' c c' args params vs D b t d f k m n ex,
                      apply_state s "this" = Some (RefVal c' n) →
                      get_class W c = Some (Class c d f k m) →
                      evaluates_args s args vs s' t →
                      get_class W d = Some D →
                      cbody D = (params, b) →
                      map_params s' (RefVal c' n) params vs = s'' →
                      evaluates_seq s''  b (Some (ResExc ex)) s''' t →
                      mkState (st s') (heap s''') = s'''' →
                      E[ s ⊢ Super c args ⇓ ResExc ex | s'''' @ S t ]
  | E_Field         : ∀ s s' e c i d l v f t,
                      E[ s ⊢ e ⇓ ResData (RefVal c i) | s' @ t ] →
                      nth_error (heap s') i = Some (ObjectDef d l) →
                      In (f, v) l →
                      E[ s ⊢ Field e f ⇓ ResData v | s' @ t ]
  | E_Field_Npe     : ∀ s s' e f t,
                      E[ s ⊢ e ⇓ ResData NullVal | s' @ t ] →
                      E[ s ⊢ Field e f ⇓ ResExc NullPointerException | s' @ t ]
  | E_Field_Exc     : ∀ s s' e ex f t,
                      E[ s ⊢ e ⇓ ResExc ex | s' @ t ] →
                      E[ s ⊢ Field e f ⇓ ResExc ex | s' @ t ]
  | E_Num           : ∀ s n t,
                      E[ s ⊢ Num n ⇓ ResData (IntVal n) | s @ t ]
  | E_Aop           : ∀ s s' s'' e1 e2 v1 v2 o t,
                      E[ s ⊢ e1 ⇓ ResData (IntVal v1) | s' @ t ] →
                      E[ s' ⊢ e2 ⇓ ResData (IntVal v2) | s'' @ t ] →
                      E[ s ⊢ Aop e1 e2 o ⇓ aop v1 v2 o | s'' @ t ]
  | E_Aop_Exc_1     : ∀ s s' s'' e1 e2 v1 ex o t,
                      E[ s ⊢ e1 ⇓ ResData (IntVal v1) | s' @ t ] →
                      E[ s' ⊢ e2 ⇓ ResExc ex | s'' @ t ] →
                      E[ s ⊢ Aop e1 e2 o ⇓ ResExc ex | s'' @ t ]
  | E_Aop_Exc_2     : ∀ s s' e1 e2 ex o t,
                      E[ s ⊢ e1 ⇓ ResExc ex | s' @ t ] →
                      E[ s ⊢ Aop e1 e2 o ⇓ ResExc ex | s' @ t ]
  | E_UMinus        : ∀ s s' e v t,
                      E[ s ⊢ e ⇓ ResData (IntVal v) | s' @ t ] →
                      E[ s ⊢ UnaryMinus e ⇓ ResData (IntVal (-v)) | s' @ t ]
  | E_UMinus_Exc    : ∀ s s' e ex t,
                      E[ s ⊢ e ⇓ ResExc ex | s' @ t ] →
                      E[ s ⊢ UnaryMinus e ⇓ ResExc ex | s' @ t ]
  | E_Cop           : ∀ s s' s'' e1 e2 v1 v2 o t,
                      E[ s ⊢ e1 ⇓ ResData (IntVal v1) | s' @ t ] →
                      E[ s' ⊢ e2 ⇓ ResData (IntVal v2) | s'' @ t ] →
                      E[ s ⊢ Cop e1 e2 o ⇓ ResData (BoolVal (cop v1 v2 o)) | s'' @ t ]
  | E_Cop_Exc_1     : ∀ s s' s'' e1 e2 v1 ex o t,
                      E[ s ⊢ e1 ⇓ ResData (IntVal v1) | s' @ t ] →
                      E[ s' ⊢ e2 ⇓ ResExc ex | s'' @ t ] →
                      E[ s ⊢ Cop e1 e2 o ⇓ ResExc ex | s'' @ t ]
  | E_Cop_Exc_2     : ∀ s s' e1 e2 ex o t,
                      E[ s ⊢ e1 ⇓ ResExc ex | s' @ t ] →
                      E[ s ⊢ Cop e1 e2 o ⇓ ResExc ex | s' @ t ]
  | E_EqOp          : ∀ s s' s'' e1 e2 v1 v2 o t,
                      E[ s ⊢ e1 ⇓ ResData v1 | s' @ t ] →
                      E[ s' ⊢ e2 ⇓ ResData v2 | s'' @ t ] →
                      E[ s ⊢ EqOp e1 e2 o ⇓ ResData (BoolVal (eqOp v1 v2 o)) | s'' @ t ]
  | E_EqOp_Exc_1    : ∀ s s' s'' e1 e2 v1 ex o t,
                      E[ s ⊢ e1 ⇓ ResData v1 | s' @ t ] →
                      E[ s' ⊢ e2 ⇓ ResExc ex | s'' @ t ] →
                      E[ s ⊢ EqOp e1 e2 o ⇓ ResExc ex | s'' @ t ]
  | E_EqOp_Exc_2    : ∀ s s' e1 e2 ex o t,
                      E[ s ⊢ e1 ⇓ ResExc ex | s' @ t ] →
                      E[ s ⊢ EqOp e1 e2 o ⇓ ResExc ex | s' @ t ]
  | E_BConst        : ∀ s b t,
                      E[ s ⊢ BConst b ⇓ ResData (BoolVal b) | s @ t ]
  | E_Bop_And_True  : ∀ s s' s'' e1 e2 v2 t,
                      E[ s ⊢ e1 ⇓ ResData (BoolVal true) | s' @ t ] →
                      E[ s' ⊢ e2 ⇓ v2 | s'' @ t ] →
                      E[ s ⊢ Bop e1 e2 And ⇓ v2 | s'' @ t ]
  | E_Bop_And_False : ∀ s s' e1 e2 t,
                      E[ s ⊢ e1 ⇓ ResData (BoolVal false) | s' @ t ] →
                      E[ s ⊢ Bop e1 e2 And ⇓ ResData (BoolVal false) | s' @ t ]
  | E_Bop_Or_False  : ∀ s s' s'' e1 e2 v2 t,
                      E[ s ⊢ e1 ⇓ ResData (BoolVal false) | s' @ t ] →
                      E[ s' ⊢ e2 ⇓ v2 | s'' @ t ] →
                      E[ s ⊢ Bop e1 e2 Or ⇓ v2 | s'' @ t ]
  | E_Bop_Or_True   : ∀ s s' e1 e2 t,
                      E[ s ⊢ e1 ⇓ ResData (BoolVal true) | s' @ t ] →
                      E[ s ⊢ Bop e1 e2 Or ⇓ ResData (BoolVal true) | s' @ t ]
  | E_Bop_Exc       : ∀ s s' e1 e2 ex o t,
                      E[ s ⊢ e1 ⇓ ResExc ex | s' @ t ] →
                      E[ s ⊢ Bop e1 e2 o ⇓ ResExc ex | s' @ t ]
  | E_Not           : ∀ s s' e v t,
                      E[ s ⊢ e ⇓ ResData (BoolVal v) | s' @ t ] →
                      E[ s ⊢ Not e ⇓ ResData (BoolVal (negb v)) | s' @ t ]
  | E_Not_Exc       : ∀ s s' e ex t,
                      E[ s ⊢ e ⇓ ResExc ex | s' @ t ] →
                      E[ s ⊢ Not e ⇓ ResExc ex | s' @ t ]
  | E_Null          : ∀ s t,
                      E[ s ⊢ Null ⇓ ResData NullVal | s @ t ]
  | E_Cast_S        : ∀ s s' e v τ t,
                      E[ s ⊢ e ⇓ ResData v | s' @ t ] →
                      has_type v τ →
                      E[ s ⊢ Cast τ e ⇓ ResData v | s' @ t ]
  | E_Cast_IB       : ∀ s s' e v t,
                      E[ s ⊢ e ⇓ ResData (IntVal v) | s' @ t ] →
                      E[ s ⊢ Cast Boolean e ⇓ ResData (BoolVal (if Z_as_Int.eq_dec v 0 then false else true)) | s' @ t ]
  | E_Cast_BI       : ∀ s s' e v t,
                      E[ s ⊢ e ⇓ ResData (BoolVal v) | s' @ t ] →
                      E[ s ⊢ Cast Int e ⇓ ResData (IntVal (if v then 1 else 0)) | s' @ t ]
  | E_Cast_Exc_1    : ∀ s s' e i n τ t,
                      E[ s ⊢ e ⇓ ResData (RefVal i n) | s' @ t ] →
                      ¬ (has_type (RefVal i n) τ) →
                      E[ s ⊢ Cast τ e ⇓ ResExc ClassCastException | s' @ t ]
  | E_Cast_Exc_2    : ∀ s s' e ex τ t,
                      E[ s ⊢ e ⇓ ResExc ex | s' @ t ] →
                      E[ s ⊢ Cast τ e ⇓ ResExc ex | s' @ t ]
  with evaluates_args : state → list jExpr → list val → state → nat → Prop :=
  | Eval_Args_Nil  : ∀ s t, evaluates_args s nil nil s t
  | Eval_Args_Cons : ∀ e es v vs s s' s'' t,
                     E[ s ⊢ e ⇓ ResData v | s' @ t ] →
                     evaluates_args s' es vs s'' t →
                     evaluates_args s (e :: es) (v :: vs) s'' t
  with evaluates_args_exc : state → list jExpr → exc → list val → state → nat → Prop :=
  | Eval_Args_Exc : ∀ l1 v1 e l2 s s' s'' ex t,
                    evaluates_args s l1 v1 s' t →
                    E[ s' ⊢ e ⇓ ResExc ex | s'' @ t ] →
                    evaluates_args_exc s (l1 ++ e :: l2) ex v1 s'' t
  with evaluates_seq : state → jSeq → option result → state → nat → Prop :=
  | Eval_EndSeq            : ∀ s t, evaluates_seq s EndSeq None s t
  | Eval_Declaration_Int   : ∀ n s s' v t seq,
                             evaluates_seq (setValue s n (IntVal Z_as_Int._0)) seq v s' t →
                             evaluates_seq s (Declaration (Var Int n) seq) v s' t
  | Eval_Declaration_Bool  : ∀ n s s' v t seq,
                             evaluates_seq (setValue s n (BoolVal false)) seq v s' t →
                             evaluates_seq s (Declaration (Var Boolean n) seq) v s' t
  | Eval_Declaration_Named : ∀ n i s s' v t seq,
                             evaluates_seq (setValue s n NullVal) seq v s' t →
                             evaluates_seq s (Declaration (Var (Named i) n) seq) v s' t
  | Eval_Expression        : ∀ e s s' s'' v v' t seq,
                             E[ s ⊢ e ⇓ ResData v | s' @ t ] →
                             evaluates_seq s' seq v' s'' t →
                             evaluates_seq s (Expression e seq) v' s'' t
  | Eval_Expression_Exc    : ∀ e s s' v t seq,
                             E[ s ⊢ e ⇓ ResExc v | s' @ t ] →
                             evaluates_seq s (Expression e seq) (Some (ResExc v)) s' t
  | Eval_If_True_None      : ∀ e s s' s'' s''' v t seq1 seq2 seq,
                             E[ s ⊢ e ⇓ ResData (BoolVal true) | s' @ t ] →
                             evaluates_seq s' seq1 None s'' t →
                             evaluates_seq s'' seq v s''' t →
                             evaluates_seq s (If e seq1 seq2 seq) v s''' t
  | Eval_If_True_Some      : ∀ e s s' s'' v t seq1 seq2 seq,
                             E[ s ⊢ e ⇓ ResData (BoolVal true) | s' @ t ] →
                             evaluates_seq s' seq1 (Some v) s'' t →
                             evaluates_seq s (If e seq1 seq2 seq) (Some v) s'' t
  | Eval_If_False_None     : ∀ e s s' s'' s''' v t seq1 seq2 seq,
                             E[ s ⊢ e ⇓ ResData (BoolVal false) | s' @ t ] →
                             evaluates_seq s' seq2 None s'' t →
                             evaluates_seq s'' seq v s''' t →
                             evaluates_seq s (If e seq1 seq2 seq) v s''' t
  | Eval_If_False_Some     : ∀ e s s' s'' v t seq1 seq2 seq,
                             E[ s ⊢ e ⇓ ResData (BoolVal false) | s' @ t ] →
                             evaluates_seq s' seq2 (Some v) s'' t →
                             evaluates_seq s (If e seq1 seq2 seq) (Some v) s'' t
  | Eval_If_Exc            : ∀ e s s' v t seq1 seq2 seq,
                             E[ s ⊢ e ⇓ ResExc v | s' @ t ] →
                             evaluates_seq s (If e seq1 seq2 seq) (Some (ResExc v)) s' t
  | Eval_While_True_None   : ∀ e s s' s'' s''' v t seq seq',
                             E[ s ⊢ e ⇓ ResData (BoolVal true) | s' @ (S t) ] →
                             evaluates_seq s' seq' None s'' (S t) →
                             evaluates_seq s'' (While e seq' seq) v s''' t →
                             evaluates_seq s (While e seq' seq) v s''' (S t)
  | Eval_While_True_Some   : ∀ e s s' s'' v t seq seq',
                             E[ s ⊢ e ⇓ ResData (BoolVal true) | s' @ t ] →
                             evaluates_seq s' seq' (Some v) s'' t →
                             evaluates_seq s (While e seq' seq) (Some v) s'' t
  | Eval_While_True_Exc    : ∀ e s s' s'' seq seq',
                             E[ s ⊢ e ⇓ ResData (BoolVal true) | s' @ 0%nat ] →
                             evaluates_seq s' seq' None s'' 0%nat →
                             evaluates_seq s (While e seq' seq) (Some (ResExc TimeLimitExceededException)) s'' 0%nat
  | Eval_While_False       : ∀ e s s' s'' v t seq seq',
                             E[ s ⊢ e ⇓ ResData (BoolVal false) | s' @ t ] →
                             evaluates_seq s' seq v s'' t →
                             evaluates_seq s (While e seq' seq) v s'' t
  | Eval_While_Exc         : ∀ e s s' v t seq seq',
                             E[ s ⊢ e ⇓ ResExc v | s' @ t ] →
                             evaluates_seq s (While e seq' seq) (Some (ResExc v)) s' t
  | Eval_Return            : ∀ e v s s' t,
                             E[ s ⊢ e ⇓ v | s' @ t ] →
                             evaluates_seq s (Return e) (Some v) s' t
  where "'E[' St0 '⊢' e '⇓' v '|' St '@' t ']'" := (evaluates_expr St0 e v St t).

  Section evaluation_proofs.
    Hypothesis Hclass_name_not_object : ∀ d f k m, ¬ In (Class "Object" d f k m) W.
    Hypothesis Hclass_name_unique : ∀ c d d' f f' k k' m m',
      In (Class c d f k m) W → In (Class c d' f' k' m') W → d = d' ∧ f = f' ∧ k = k' ∧ m = m'.
    Hypothesis Hfield_decl_unique : ∀ C f n n' v t t',
      In C W → fields W C f → nth_error f n = Some (Var t v) → nth_error f n' = Some (Var t' v) → n = n'.
    Hypothesis Hmethod_name_unique : ∀ m ms t a s t' a' s' c d f k,
      get_class W c = Some (Class c d f k ms) → In (Method t m a s) ms → In (Method t' m a' s') ms → t = t' ∧ a = a' ∧ s = s'.
    Hypothesis Hmethod_override_equal_types : ∀ m C t D t' c d,
      mtype W m C t → mtype W m D t' → get_class W c = Some C → get_class W d = Some D →
      S[ Named d <:: Named c ] → t = t'.
    Hypothesis Hmethod_has_type_in_subclass : ∀ m C t D c d,
      mtype W m C t → get_class W c = Some C → get_class W d = Some D → S[ Named d <:: Named c] →
      ∃ t', mtype W m D t'.
    Hypothesis Hmethod_body_correct_type : ∀ m c d f k ms τ b,
      mtype W m (Class c d f k ms) τ →
      mbody W m (Class c d f k ms) b →
      T[ ∅ ,+* (fst b) ,+ "this" :→ Named c ⊢ (snd b) :→→ Some (snd τ) ].
    Hypothesis Hfields_determ : ∀ c C f f',
      get_class W c = Some C →
      fields W C f →
      fields W C f' →
      f = f'.
    Hypothesis Hconstructor_body_correct_type : ∀ c d C τ b,
      get_class W c = Some C →
      ctype C = τ →
      cbody C = b →
      S[ Named d <:: Named c ] →
      T[ ∅ ,+* (fst b) ,+ "this" :→ Named d ⊢ (snd b) :→→ None ].
    Hypothesis Hfields_f_exist : ∀ c C,
      get_class W c = Some C →
      ∃ f, fields_f W C (length W) = Some f.

    Lemma class_has_fields : ∀ c C,
      get_class W c = Some C →
      ∃ f, fields W C f.
    Proof.
      intros.
      apply Hfields_f_exist in H.
      destruct H.
      eexists; eapply fields_f_fields; eauto.
    Qed.

    Lemma mbody_determ : ∀ m C b b', mbody W m C b → mbody W m C b' → b = b'.
    Proof.
      intros m C b b' HB.
      generalize dependent b'.
      induction HB; intros.
      - inversion H1; subst.
        destruct (Hmethod_name_unique m ms t a s t0 a0 s0 c d f k H H0 H9).
        destruct H3. subst. reflexivity.
        apply H8 in H0. exfalso. auto.
      - inversion H1; subst.
        apply H in H9. exfalso. auto.
        assert (Some D = Some D0). { rewrite <- H0. rewrite <- H9. reflexivity. }
        inversion H2; subst. clear H2.
        apply IHHB in H10. assumption.
    Qed.

    Lemma mtype_mbody_eq_types : ∀ m C t b,
      mtype W m C t →
      mbody W m C b →
      map (fun x => match x with Var t _ => t end) (fst b) = (fst t).
    Proof.
      apply mtype_mbody_eq_types_aux.
      apply mbody_determ.
    Qed.

    Lemma mtype_mbody_eq_types_sub : ∀ m C D t b c d,
      get_class W c = Some C →
      get_class W d = Some D →
      mtype W m C t →
      mbody W m D b →
      S[ Named d <:: Named c ] →
      map (fun x => match x with Var t _ => t end) (fst b) = (fst t).
    Proof.
      intros.
      destruct (Hmethod_has_type_in_subclass m C t D c d H1 H H0 H3).
      assert (t = x). { eapply Hmethod_override_equal_types; eauto. }
      subst.
      eapply mtype_mbody_eq_types; eauto.
    Qed.

    Lemma ctype_cbody_eq_types_sub : ∀ C t b,
      ctype C = t →
      cbody C = b →
      map (fun x => match x with Var t _ => t end) (fst b) = t.
    Proof.
      intros.
      destruct C.
      - simpl in *. unfold ctype in *. subst. simpl. reflexivity.
      - simpl in *. destruct j. subst. simpl. reflexivity.
    Qed.

    Definition is_object (v : val) : Prop :=
      match v with
      | RefVal _ _ => True
      | _          => False
      end.

    Hint Unfold is_object.

    Ltac preservation_auto :=
      repeat (
        match goal with
        | [ H : ∃ d, _ |- _ ] => destruct H; subst
        | [ H : _ ∧ _ |- _ ] => destruct H
        | [ |- _ ∧ _ ] => split
        | [ H : _ ∨ _ |- _ ] => destruct H
        | [ H : ResData _ = ResData _ |- _ ] =>
            inversion H; subst; clear H
        | [ H : is_object (RefVal _ _) |- _ ] => clear H
        | [ H : is_object (IntVal _) |- _ ] => exfalso; auto
        | [ H : is_object (BoolVal _) |- _ ] => exfalso; auto
        | [ H : is_object NullVal |- _ ] => exfalso; auto
        | [ H : ¬ is_object (RefVal _ _) |- _ ] => exfalso; auto
        | [ |- (is_object (IntVal _) ∧ _) ∨ _ ] => right
        | [ |- (is_object (BoolVal _) ∧ _) ∨ _ ] => right
        | [ |- (is_object NullVal ∧ _) ∨ _ ] => right
        | [ |- _ ∨ ¬ is_object (RefVal _ _) ] => left
        | [ |- (∃ v, ResExc _ = ResData v ∧ _) ∨ _ ] => right
        | [ |- _ ∨ ((∀ v, ResData _ ≠ ResData v) ∧ _) ] => left
        | [ |- ∃ v, ResData ?V = ResData v ∧ _ ] => exists V
        | [ |- ?X = ?X ∧ _ ] => split; [reflexivity|]
        | [ H : ∀ v : val, ResData _ ≠ ResData v |- _ ] => exfalso; eapply H; eauto
        | [ |- ∀ v : val, ResExc _ ≠ ResData v ] => intro; intro
        | [ H : ResExc _ = ResData _ |- _ ] => inversion H
        | [ |- ∃ x n o, RefVal ?X ?N = RefVal x n ∧ _ ] => exists X; exists N
        | [ H1 : state_correct ?G ?S, H2 : T[ ?G ⊢ ?E :→ ?T ], H3 : E[ ?S ⊢ ?E ⇓ ?R | ?S2 @ ?TT ],
            IH : ∀ Γ τ St0 St r, state_correct Γ St0 → T[ Γ ⊢ ?E :→ τ] → E[ St0 ⊢ ?E ⇓ r | St @ ?TT] → _ |- _ ] =>
            destruct (IH G T S S2 R H1 H2 H3); destruct H1
        end; subst; try assumption; try tauto); try auto.

    Lemma map_params_heap_not_changed : ∀ params s τ n vs,
      heap (map_params s (RefVal τ n) params vs) = heap s.
    Proof.
      induction params; intros; simpl.
      reflexivity.
      destruct vs. simpl. reflexivity.
      destruct a. simpl. unfold map_params in IHparams. simpl in IHparams. apply IHparams; auto.
    Qed.

    Lemma map_params_aux_heap_not_changed : ∀ p s vs,
      heap (map_params_aux s p vs) = heap s.
    Proof.
      induction p; intros; simpl.
      reflexivity.
      destruct vs. simpl. reflexivity.
      destruct a. simpl. apply IHp.
    Qed.

    Lemma nat_ind_strong : ∀ P : nat → Prop,
      P 0%nat →
      (∀ n : nat, (∀ m : nat, (m ≤ n)%nat → P m) → P (S n)) →
      ∀ n : nat, P n.
    Proof.
      intros.
      strong induction n.
      destruct n.
      assumption.
      apply H0.
      intros.
      assert (m < S n)%nat. { omega. }
      apply H1. assumption.
    Qed.

    Lemma eval_seq_heap_entry_remains : ∀ t b s v s' n τ od,
      evaluates_seq s b v s' t →
      nth_error (heap s) n = Some (ObjectDef τ od) →
      (∀ m : nat, m ≤ t →
         ∀ (e : jExpr) (s : state) (v : result) (s' : state) (n : nat) (τ : string) 
           (od : list (string * val)),
           nth_error (heap s) n = Some (ObjectDef τ od)
           → E[ s ⊢ e ⇓ v | s' @ m] → ∃ d : list (string * val), nth_error (heap s') n = Some (ObjectDef τ d)) →
      ∃ d : list (string * val), nth_error (heap s') n = Some (ObjectDef τ d).
    Proof.
      induction t using nat_ind_strong; induction b; intros s v s' n τ od Heval; intros;
      inversion Heval; subst; try eauto.
      - assert (∃ d : list (string * val), nth_error (heap s'0) n = Some (ObjectDef τ d)).
        { eapply H0; eauto. }
        destruct H1.
        eapply IHb; eauto.
      - assert (∃ d : list (string * val), nth_error (heap s'0) n = Some (ObjectDef τ d)).
        { eapply H0; eauto. }
        destruct H1.
        assert (∃ d : list (string * val), nth_error (heap s'') n = Some (ObjectDef τ d)).
        { eapply IHb1; eauto. }
        destruct H2.
        eapply IHb3; eauto.
      - assert (∃ d : list (string * val), nth_error (heap s'0) n = Some (ObjectDef τ d)).
        { eapply H0; eauto. }
        destruct H1.
        eapply IHb1; eauto.
      - assert (∃ d : list (string * val), nth_error (heap s'0) n = Some (ObjectDef τ d)).
        { eapply H0; eauto. }
        destruct H1.
        assert (∃ d : list (string * val), nth_error (heap s'') n = Some (ObjectDef τ d)).
        { eapply IHb2; eauto. }
        destruct H2.
        eapply IHb3; eauto.
      - assert (∃ d : list (string * val), nth_error (heap s'0) n = Some (ObjectDef τ d)).
        { eapply H0; eauto. }
        destruct H1.
        eapply IHb2; eauto.
      - assert (∃ d : list (string * val), nth_error (heap s'0) n = Some (ObjectDef τ d)).
        { eapply H0; eauto. }
        destruct H1.
        eapply IHb1; eauto.
      - assert (∃ d : list (string * val), nth_error (heap s'0) n = Some (ObjectDef τ d)).
        { eapply H0; eauto. }
        destruct H1.
        eapply IHb1; eauto.
      - assert (∃ d : list (string * val), nth_error (heap s'0) n = Some (ObjectDef τ d)).
        { eapply H0; eauto. }
        destruct H1.
        eapply IHb2; eauto.
      - assert (∃ d : list (string * val), nth_error (heap s'0) n = Some (ObjectDef τ d)).
        { eapply H1; eauto. }
        destruct H2.
        eapply IHb; eauto.
      - assert (∃ d : list (string * val), nth_error (heap s'0) n = Some (ObjectDef τ d)).
        { eapply H1; eauto. }
        destruct H2.
        assert (∃ d : list (string * val), nth_error (heap s'') n = Some (ObjectDef τ d)).
        { eapply IHb1; eauto. }
        destruct H3.
        eapply IHb3; eauto.
      - assert (∃ d : list (string * val), nth_error (heap s'0) n = Some (ObjectDef τ d)).
        { eapply H1; eauto. }
        destruct H2.
        eapply IHb1; eauto.
      - assert (∃ d : list (string * val), nth_error (heap s'0) n = Some (ObjectDef τ d)).
        { eapply H1; eauto. }
        destruct H2.
        assert (∃ d : list (string * val), nth_error (heap s'') n = Some (ObjectDef τ d)).
        { eapply IHb2; eauto. }
        destruct H3.
        eapply IHb3; eauto.
      - assert (∃ d : list (string * val), nth_error (heap s'0) n = Some (ObjectDef τ d)).
        { eapply H1; eauto. }
        destruct H2.
        eapply IHb2; eauto.
      - assert (∃ d : list (string * val), nth_error (heap s'0) n = Some (ObjectDef τ d)).
        { eapply H1; eauto. }
        destruct H2.
        assert (∃ d : list (string * val), nth_error (heap s'') n = Some (ObjectDef τ d)).
        { eapply IHb1; eauto. }
        destruct H3.
        eapply H; eauto.
      - assert (∃ d : list (string * val), nth_error (heap s'0) n = Some (ObjectDef τ d)).
        { eapply H1; eauto. }
        destruct H2.
        eapply IHb1; eauto.
      - assert (∃ d : list (string * val), nth_error (heap s'0) n = Some (ObjectDef τ d)).
        { eapply H1; eauto. }
        destruct H2.
        eapply IHb2; eauto.
    Qed.

    Ltac heap_entry_remains_auto :=
      repeat (
          match goal with
          | [ H1 : E[ ?S1 ⊢ ?E ⇓ ?V | ?S2 @ ?T ],
              H2 : nth_error (heap ?S1) ?N = Some (ObjectDef ?C ?D),
              H3 : ∀ (s : state) (v : result) (s' : state) (n : nat) (τ : string) (od : list (string * val)),
                   nth_error (heap s) n = Some (ObjectDef τ od)
                   → E[ s ⊢ ?E ⇓ v | s' @ ?T]
                     → ∃ d : list (string * val), nth_error (heap s') n = Some (ObjectDef τ d) |- _ ] =>
            destruct (H3 S1 V S2 N C D H2 H1); clear H1; eauto
          end
        ).

    Lemma eval_heap_entry_remains : ∀ t e s v s' n τ od,
      nth_error (heap s) n = Some (ObjectDef τ od) →
      E[ s ⊢ e ⇓ v | s' @ t ] →
      ∃ d, nth_error (heap s') n = Some (ObjectDef τ d).
    Proof.
      induction t using nat_ind_strong.
      - induction e using jExpr_ind2 with (Q := fun l : list jExpr =>
          ∀ s vl s' n τ od,
            nth_error (heap s) n = Some (ObjectDef τ od) →
            evaluates_args s l vl s' 0 →
            ∃ d, nth_error (heap s') n = Some (ObjectDef τ d));
        intros; simpl; simpl in *; inversion H0; subst; eauto; simpl; preservation_auto;
        heap_entry_remains_auto.
        eapply set_field_get_heap in H10. destruct H10. rewrite H2 in H3. eauto.
        destruct H3; subst. destruct H4. destruct H3. destruct H3.
        rewrite H2 in H3. inversion H3; subst. clear H3.
        simpl in H4. destruct H4. destruct (set_field_aux_def x0 s v0); inversion H3; subst. eauto.
      - induction e using jExpr_ind2 with (Q := fun l : list jExpr =>
          ∀ s vl ex s' n τ od,
            nth_error (heap s) n = Some (ObjectDef τ od) →
            (evaluates_args s l vl s' t ∨ evaluates_args_exc s l ex vl s' t) →
            ∃ d, nth_error (heap s') n = Some (ObjectDef τ d));
        intros; simpl; simpl in *; inversion H1; subst; simpl; preservation_auto;
        heap_entry_remains_auto; eauto.
        + eapply set_field_get_heap in H11. destruct H11. rewrite H3 in H4. eauto.
          destruct H4; subst. destruct H5. destruct H4. destruct H4.
          rewrite H3 in H4. inversion H4; subst. clear H4.
          simpl in H5. destruct H5. destruct (set_field_aux_def x0 s v0); inversion H4; subst. eauto.
        + assert (∃ d : list (string * val), nth_error (heap s'') n = Some (ObjectDef τ d)).
          { eapply IHe0; eauto. }
          destruct H3.
          assert (∃ d : list (string * val), nth_error (heap (map_params s'' (RefVal c i) params vs)) n = Some (ObjectDef τ d)).
          { rewrite map_params_heap_not_changed. eauto. }
          destruct H4.
          eapply eval_seq_heap_entry_remains; eauto.
        + assert (∃ d : list (string * val), nth_error (heap s'0) n = Some (ObjectDef τ d)).
          { eapply IHe; eauto. }
          destruct H2.
          assert (∃ d : list (string * val), nth_error (heap (addObject s'0 (mkObject W s f))) n = Some (ObjectDef τ d)).
          { exists x. simpl.
            rewrite nth_error_app1. apply H2. apply nth_error_Some. intro.
            rewrite H2 in H3. inversion H3. }
          destruct H3.
          assert (∃ d : list (string * val), nth_error (heap (map_params (addObject s'0 (mkObject W s f)) (RefVal s (Datatypes.length (heap s'0))) params vs)) n = Some (ObjectDef τ d)).
          { rewrite map_params_heap_not_changed. eauto. }
          destruct H4.
          eapply eval_seq_heap_entry_remains; eauto.
        + assert (∃ d : list (string * val), nth_error (heap s'0) n = Some (ObjectDef τ d)).
          { eapply IHe; eauto. }
          destruct H2.
          assert (∃ d : list (string * val), nth_error (heap (addObject s'0 (mkObject W s f))) n = Some (ObjectDef τ d)).
          { exists x. simpl.
            rewrite nth_error_app1. apply H2. apply nth_error_Some. intro.
            rewrite H2 in H3. inversion H3. }
          destruct H3.
          assert (∃ d : list (string * val), nth_error (heap (map_params (addObject s'0 (mkObject W s f)) (RefVal s (Datatypes.length (heap s'0))) params vs)) n = Some (ObjectDef τ d)).
          { rewrite map_params_heap_not_changed. eauto. }
          destruct H4.
          eapply eval_seq_heap_entry_remains; eauto.
        + assert (∃ d : list (string * val), nth_error (heap s'0) n = Some (ObjectDef τ d)).
          { eapply IHe; eauto. }
          destruct H2.
          assert (∃ d : list (string * val), nth_error (heap (map_params s'0 (RefVal s n0) params vs)) n = Some (ObjectDef τ d)).
          { rewrite map_params_heap_not_changed. eauto. }
          destruct H3.
          eapply eval_seq_heap_entry_remains; eauto.
        + assert (∃ d : list (string * val), nth_error (heap s'0) n = Some (ObjectDef τ d)).
          { eapply IHe; eauto. }
          destruct H2.
          assert (∃ d : list (string * val), nth_error (heap (map_params s'0 (RefVal s n0) params vs)) n = Some (ObjectDef τ d)).
          { rewrite map_params_heap_not_changed. eauto. }
          destruct H3.
          eapply eval_seq_heap_entry_remains; eauto.
        + inversion H1; subst. exists od; auto.
        + inversion H1; subst.
          exfalso. eapply app_cons_not_nil; eauto.
        + inversion H2; subst.
          exfalso. eapply app_cons_not_nil; eauto.
        + inversion H1; subst.
          exfalso. eapply app_cons_not_nil; eauto.
        + inversion H1; subst.
          assert (∃ d : list (string * val), nth_error (heap s'0) n = Some (ObjectDef τ d)).
          { eapply H; eauto. }
          destruct H3.
          assert (∃ d : list (string * val), nth_error (heap s') n = Some (ObjectDef τ d)).
          { eapply IHe0; eauto. }
          destruct H4. exists x0; auto.
        + inversion H2; subst.
          assert (∃ d : list (string * val), nth_error (heap s'0) n = Some (ObjectDef τ d)).
          { eapply H; eauto. }
          destruct H3.
          assert (∃ d : list (string * val), nth_error (heap s') n = Some (ObjectDef τ d)).
          { eapply IHe0; eauto. }
          assumption.
        + inversion H1; subst.
          assert (∃ d : list (string * val), nth_error (heap s'0) n = Some (ObjectDef τ d)).
          { eapply H; eauto. }
          destruct H3.
          assert (∃ d : list (string * val), nth_error (heap s') n = Some (ObjectDef τ d)).
          { eapply IHe0; eauto. }
          assumption.
        + inversion H1; subst.
          destruct l1.
          * simpl in H3. inversion H3; subst. inversion H4; subst.
            assert (∃ d : list (string * val), nth_error (heap s') n = Some (ObjectDef τ d)).
            { eapply H; eauto. }
            assumption.
          * simpl in H3. inversion H3; subst. inversion H4; subst.
            assert (evaluates_args_exc s'1 (l1 ++ e0 :: l2) ex vs s' t).
            { econstructor; eauto. }
            assert (∃ d : list (string * val), nth_error (heap s'1) n = Some (ObjectDef τ d)).
            { eapply H; eauto. }
            destruct H7.
            eapply IHe0; eauto.
      Unshelve.
      constructor.
      constructor.
      constructor.
      constructor.
      constructor.
      constructor.
      constructor.
      constructor.
    Qed.

    Lemma eval_args_heap_entry_remains : ∀ t l s v s' n τ od,
      nth_error (heap s) n = Some (ObjectDef τ od) →
      evaluates_args s l v s' t →
      ∃ d, nth_error (heap s') n = Some (ObjectDef τ d).
    Proof.
      intro t. induction l; intros.
      - inversion H0; subst; eauto.
      - inversion H0; subst.
        assert (∃ d : list (string * val), nth_error (heap s'0) n = Some (ObjectDef τ d)).
        { eapply eval_heap_entry_remains; eauto. }
        destruct H1.
        eapply IHl; eauto.
    Qed.

    Definition get_class_t (t : jType) : option jClass :=
      match t with
      | Named x => get_class W x
      | _       => None
      end.

    Lemma class_in_get_class_some : ∀ w,
      (∀ (c d d' : string) (f f' : list jVar) (k k' : jConstructor) (m m' : list jMethod),
        In (Class c d f k m) w → In (Class c d' f' k' m') w → d = d' ∧ f = f' ∧ k = k' ∧ m = m') →
      (∀ (d : string) (f : list jVar) (k : jConstructor) (m : list jMethod), ¬ In (Class "Object" d f k m) w) →
      ∀ c d f k m,
        In (Class c d f k m) w →
        get_class w c = Some (Class c d f k m).
    Proof.
      induction w; intros; inversion H1; subst.
      - simpl. destruct (string_dec c c). reflexivity. contradiction.
      - simpl. destruct a.
        destruct (string_dec c "Object"). subst.
        exfalso. eapply H0. simpl. right. apply H2.
        apply IHw.
          intros. eapply H. simpl. right. apply H3.
          simpl. right. apply H4.
          intros. intro. eapply H0. simpl. right. apply H3.
          apply H2.
        destruct (string_dec c s); subst.
          assert (s0 = d ∧ l = f ∧ j = k ∧ l0 = m).
          { eapply H. simpl. left. reflexivity. simpl. right. assumption. }
          destruct H3. destruct H4. destruct H5. subst. reflexivity.
        apply IHw.
          intros. eapply H; simpl. right; eauto. right; eauto.
          intros. intro. eapply H0. simpl. right. apply H3.
          assumption.
    Qed.

    Lemma class_in_get_class_t_some : ∀ c d f k m,
      In (Class c d f k m) W →
      get_class_t (Named c) = Some (Class c d f k m).
    Proof.
      unfold get_class_t.
      apply class_in_get_class_some; assumption.
    Qed.

    Lemma get_class_subclass_t : ∀ t1 t2 v C f t s x,
      has_type (RefVal x v) t1 →
      has_type (RefVal x v) t2 →
      S[ t1 <:: t2 ] →
      get_class_t t2 = Some C →
      fields W C f →
      In (Var t s) f →
      (∃ X f', get_class_t t1 = Some X ∧
               fields W X f' ∧
               In (Var t s) f').
    Proof.
      intros.
      induction H1.
      - exists C. exists f. auto.
      - destruct IHsubtype; try assumption.
        + simpl. simpl in H.
          eapply subtype_trans. apply H.
          eapply SubtypeDef. apply H1. apply SubtypeRefl.
        + exists (Class C0 D f0 k m).
          destruct H6. destruct H6. destruct H7.
          exists (x1 ++ f0).
          split.
          apply class_in_get_class_t_some. assumption.
          split.
          eapply FieldsClass. simpl in H6. apply H6. apply H7.
          apply in_or_app. left. assumption.
    Qed.

    Lemma get_class_subclass : ∀ x v c C f t s,
      has_type (RefVal x v) (Named c) →
      get_class W c = Some C →
      fields W C f →
      In (Var t s) f →
      (∃ X f', get_class W x = Some X ∧
               fields W X f' ∧
               In (Var t s) f').
    Proof.
      intros.
      destruct (get_class_subclass_t (Named x) (Named c) v C f t s x); auto.
      simpl. constructor.
      destruct H3. simpl in H3. eauto.
    Qed.

    Lemma class_in_get_class_some_2 : ∀ c d f k m,
      In (Class c d f k m) W →
      get_class W c = Some (Class c d f k m).
    Proof.
      apply class_in_get_class_some; assumption.
    Qed.

    Fixpoint preservation_aux_l (vl : list val) (vt : list jType) Γ (s' : state) : Prop :=
      match vl, vt with
      | nil, _           => True
      | v :: vl, τ :: vt =>
        (has_type v τ ∧ state_correct Γ s' ∧
         ((is_object v ∧ (∃ x n o, v = RefVal x n ∧
         nth_error (heap s') n = Some (ObjectDef x o))) ∨ ¬ is_object v)) ∧ preservation_aux_l vl vt Γ s'
      | _, _             => False
      end.

    Lemma apply_state_aux_set_value_aux : ∀ s i v, apply_state_aux (setValue_aux s i v) i = Some v.
    Proof.
      induction s; intros; simpl.
      - destruct (string_dec i i); tauto.
      - destruct a.
        destruct (string_dec s0 i); subst.
        + simpl. destruct (string_dec i i); tauto.
        + simpl. destruct (string_dec s0 i); subst; auto. contradiction.
    Qed.

    Lemma apply_state_aux_set_value_aux_neq : ∀ s i i' v,
      i ≠ i' →
      apply_state_aux (setValue_aux s i v) i' = apply_state_aux s i'.
    Proof.
      induction s; intros; simpl.
      - destruct (string_dec i i'); tauto.
      - destruct a.
        destruct (string_dec s0 i); subst.
        + simpl. destruct (string_dec i i'); tauto.
        + simpl. destruct (string_dec s0 i'); subst; auto.
    Qed.

    Lemma apply_state_aux_map_params_this : ∀ p s v vs,
      apply_state_aux (st (map_params s v p vs)) "this" = Some v.
    Proof.
      induction p; intros.
      - reflexivity.
      - simpl. destruct vs.
        reflexivity. destruct a.
        apply apply_state_aux_set_value_aux.
    Qed.

    Lemma all_subtype_sublist : ∀ l l' x x',
      all_subtype W (x :: l) (x' :: l') →
      all_subtype W l l'.
    Proof.
      induction l; destruct l'; intros.
      - unfold all_subtype.
        split. reflexivity. intros.
        destruct n; inversion H0.
      - destruct H. inversion H.
      - destruct H. inversion H.
      - destruct H. unfold all_subtype. split.
        inversion H. simpl. rewrite H2. reflexivity.
        intros.
        apply (H0 (S n)); simpl; auto.
    Qed.

    Lemma state_correct_map_params_aux : ∀ p vs Γ s l l',
      state_correct Γ s →
      preservation_aux_l vs l' Γ s →
      all_subtype W l' l →
      length vs = length p →
      map (fun x => match x with Var t _ => t end) p = l →
      state_correct (extend_typing_env_multi empty_typing_env p)
                    (map_params_aux s p vs).
    Proof.
      induction p; intros; simpl.
      - unfold state_correct. split.
        + intros. inversion H4.
        + unfold state_heap_correct; split; intros.
          destruct H.
          destruct H5.
          eapply (H5 (k + length (st s))%nat); eauto.
          unfold get_refs. rewrite nth_error_app2.
          rewrite Nat.add_sub. apply H4. omega.
          simpl. destruct H. destruct H4. assumption.
      - destruct a.
        destruct vs.
        inversion H2.
        assert (state_correct (extend_typing_env_multi empty_typing_env p) (map_params_aux s p vs)).
        { simpl in H0. destruct l'. destruct H0. destruct H0.
          eapply IHp; eauto. simpl in H2. inversion H2.
          destruct l. inversion H3. apply all_subtype_sublist in H1.
          simpl in H3. inversion H3; subst. assumption. }
        unfold state_correct. split.
        + intros.
          unfold apply_state. unfold setValue. simpl.
          destruct (string_dec s0 i); subst.
          * rewrite apply_state_aux_set_value_aux.
            unfold extend_typing_env in H5.
            destruct (string_dec i i).
            simpl in H0. destruct l'. exfalso; auto. destruct H0.
            inversion H5; subst.
            destruct H0. inversion H1. exists v; split; auto.
            simpl in H8. eapply has_type_subtype; eauto.
            apply (H8 0%nat); auto.
            contradiction.
          * rewrite apply_state_aux_set_value_aux_neq; [|assumption].
            destruct l'. inversion H0.
            destruct (IHp vs Γ s (map (λ x : jVar, match x with | Var t _ => t end) p) l' H).
            { inversion H0. assumption. }
            { simpl in H1. eapply all_subtype_sublist; eauto. }
            { inversion H2. reflexivity. }
            { reflexivity. }
            unfold extend_typing_env in H5.
            destruct (string_dec i s0); subst; try contradiction.
            unfold apply_state in H3.
            apply H3. assumption.
        + unfold state_heap_correct. split.
          * intros. unfold setValue. simpl.
            destruct H4.
            destruct H6.
            apply nth_error_get_refs_set_value in H5.
            destruct H5. destruct H5.
            eapply H6; eauto.
            inversion H5; subst.
            destruct l'. inversion H0.
            inversion H0. destruct H3. destruct H9. destruct H10. destruct H10.
            destruct H11. destruct H11. destruct H11. destruct H11.
            inversion H11; subst.
            rewrite map_params_aux_heap_not_changed.
            exists x1. auto.
            simpl in H10. contradiction.
          * unfold setValue. simpl.
            rewrite map_params_aux_heap_not_changed.
            destruct H. destruct H5. assumption.
    Qed.

    Lemma state_correct_map_params : ∀ p vs Γ s x xt l l' n t,
      state_correct Γ s →
      preservation_aux_l vs l' Γ s →
      all_subtype W l' l →
      has_type (RefVal t n) xt →
      nth_error (heap s) n = Some (ObjectDef t x) →
      length vs = length p →
      map (fun x => match x with Var t _ => t end) p = l →
      state_correct (extend_typing_env (extend_typing_env_multi empty_typing_env p) "this" xt)
                    (map_params s (RefVal t n) p vs).
    Proof.
      intros.
      assert (state_correct (extend_typing_env_multi empty_typing_env p) (map_params_aux s p vs)).
      { eapply state_correct_map_params_aux; eauto. }
      unfold state_correct. split.
      - intros. unfold apply_state. unfold map_params.
        simpl.
        destruct (string_dec "this" i); subst.
        rewrite apply_state_aux_set_value_aux.
        unfold extend_typing_env in H7.
        destruct (string_dec "this" "this"); try contradiction.
        inversion H7; subst.
        eexists; split; eauto.
        rewrite apply_state_aux_set_value_aux_neq; [|assumption].
        unfold extend_typing_env in H7.
        destruct (string_dec i "this"); subst; try contradiction.
        destruct H6. unfold apply_state in H5. apply H5. assumption.
      - unfold state_heap_correct.
        split.
        + intros.
          unfold map_params in H7.
          apply nth_error_get_refs_set_value in H7.
          destruct H7. destruct H7.
          destruct H6. destruct H8.
          eapply H8; eauto.
          inversion H7; subst.
          rewrite map_params_heap_not_changed. eauto.
        + rewrite map_params_heap_not_changed.
          destruct H. destruct H7. auto.
    Qed.

    Lemma evaluates_args_list_lengths : ∀ l vs s s' t,
      evaluates_args s l vs s' t →
      length l = length vs.
    Proof.
      induction l; destruct vs; intros.
      reflexivity.
      inversion H. inversion H.
      inversion H; subst. apply IHl in H8.
      simpl. rewrite H8. reflexivity.
    Qed.

    Lemma all_subtype_eq_lengths : ∀ l l',
      all_subtype W l l' →
      length l = length l'.
    Proof.
      induction l; destruct l'; intros.
      reflexivity.
      unfold all_subtype in H. destruct H. inversion H.
      unfold all_subtype in H. destruct H. inversion H.
      apply all_subtype_sublist in H. apply IHl in H.
      simpl. rewrite H. reflexivity.
    Qed.

    Lemma preservation_aux_evaluates_seq : ∀ t,
      (∀ m : nat, m ≤ t
                 → ∀ (e : jExpr) (Γ : string → option jType) (τ : jType) (St0 St : state) (r : result), 
                 state_correct Γ St0
                 → T[ Γ ⊢ e :→ τ]
                   → E[ St0 ⊢ e ⇓ r | St @ m]
                     → (∃ v : val, r = ResData v
                                   ∧ has_type v τ
                                     ∧ state_correct Γ St
                                       ∧ (is_object v
                                          ∧ (∃ (x : string) (n : nat) (o : list (string * val)), 
                                             v = RefVal x n
                                             ∧ nth_error (heap St) n = Some (ObjectDef x o))
                                          ∨ ¬ is_object v))
                      ∨ (∀ v : val, r ≠ ResData v) ∧ state_correct Γ St) →
      ∀ b Γ s tt o St,
        state_correct Γ s →
        T[ Γ ⊢ b :→→ tt] →
        evaluates_seq s b o St t →
        (o = None ∧ state_correct Γ St) ∨
        (∃ r, o = Some r ∧
          ((∃ v τ, r = ResData v ∧ tt = Some τ ∧
                has_type v τ ∧ state_correct Γ St ∧
                ((is_object v ∧ (∃ x n o, v = RefVal x n ∧
                  nth_error (heap St) n = Some (ObjectDef x o))) ∨ ¬ is_object v)) ∨
              ((∀ v, r ≠ ResData v) ∧ state_correct Γ St))).
    Proof.
      induction t;
      intros Ht b Γ s tt r St HScorrect Htyping;
      generalize dependent HScorrect;
      generalize dependent St;
      generalize dependent r;
      generalize dependent s;
      induction Htyping; intros.
      - inversion H; subst; left; split; auto.
      - inversion H0; subst.
        edestruct IHHtyping; eauto.
        + split. intros.
          * unfold extend_typing_env in H1.
            destruct (string_dec i x); subst; inversion H1; subst.
            unfold apply_state. unfold setValue. simpl.
            rewrite apply_state_aux_set_value_aux.
            exists (IntVal Z_as_Int._0). simpl; auto.
            unfold apply_state. unfold setValue. simpl.
            rewrite apply_state_aux_set_value_aux_neq; auto.
            destruct HScorrect. apply H2; auto.
          * split.
            { intros. apply nth_error_get_refs_set_value in H1.
              destruct H1. unfold setValue. simpl.
              destruct H1. destruct HScorrect.
              destruct H3. eapply H3; eauto.
              inversion H1. }
            { unfold setValue. simpl. destruct HScorrect. destruct H2. auto. }
        + left. split; destruct H1; auto. eapply state_correct_weaken; eauto.
        + right. destruct H1. exists x0.
          destruct H1; split; auto. subst.
          destruct H2; [left|right]; auto.
          destruct H1. exists x1.
          destruct H1. exists x2.
          destruct H1; split; auto.
          destruct H2; split; auto.
          destruct H3; split; auto.
          destruct H4; split; auto.
          eapply state_correct_weaken; eauto.
          destruct H1; split; auto.
          eapply state_correct_weaken; eauto.
        + edestruct IHHtyping; eauto.
          * split. intros.
            { unfold extend_typing_env in H1.
              destruct (string_dec i x); subst; inversion H1; subst.
              unfold apply_state. unfold setValue. simpl.
              rewrite apply_state_aux_set_value_aux.
              exists (BoolVal false). simpl; auto.
              unfold apply_state. unfold setValue. simpl.
              rewrite apply_state_aux_set_value_aux_neq; auto.
              destruct HScorrect. apply H2; auto. }
            split.
            { intros. apply nth_error_get_refs_set_value in H1.
              destruct H1. unfold setValue. simpl.
              destruct H1. destruct HScorrect.
              destruct H3. eapply H3; eauto.
              inversion H1. }
            { unfold setValue. simpl. destruct HScorrect. destruct H2. auto. }
          * left. destruct H1; split; auto. eapply state_correct_weaken; eauto.
          * right. destruct H1. exists x0. destruct H1; split; auto.
            destruct H2; [left|right]; auto.
            destruct H2. exists x1.
            destruct H2. exists x2.
            destruct H2; split; auto.
            destruct H3; split; auto.
            destruct H4; split; auto.
            destruct H5; split; auto.
            eapply state_correct_weaken; eauto.
            destruct H2; split; auto.
            eapply state_correct_weaken; eauto.
        + edestruct IHHtyping; eauto.
          * split. intros.
            { unfold extend_typing_env in H1.
              destruct (string_dec i0 x); subst; inversion H1; subst.
              unfold apply_state. unfold setValue. simpl.
              rewrite apply_state_aux_set_value_aux.
              exists NullVal. simpl; auto.
              unfold apply_state. unfold setValue. simpl.
              rewrite apply_state_aux_set_value_aux_neq; auto.
              destruct HScorrect. apply H2; auto. }
            split.
            { intros. apply nth_error_get_refs_set_value in H1.
              destruct H1. unfold setValue. simpl.
              destruct H1. destruct HScorrect.
              destruct H3. eapply H3; eauto.
              inversion H1. }
            { unfold setValue. simpl. destruct HScorrect. destruct H2. auto. }
          * left. destruct H1; split; auto. eapply state_correct_weaken; eauto.
          * right. destruct H1. exists x0. destruct H1; split; auto.
            destruct H2; [left|right]; auto.
            destruct H2. exists x1.
            destruct H2. exists x2.
            destruct H2; split; auto.
            destruct H3; split; auto.
            destruct H4; split; auto.
            destruct H5; split; auto.
            eapply state_correct_weaken; eauto.
            destruct H2; split; auto.
            eapply state_correct_weaken; eauto.
      - inversion H0; subst.
        + edestruct Ht; eauto.
          destruct H1. destruct H1. destruct H2. destruct H3.
          eapply IHHtyping; eauto.
          exfalso. eapply H1. reflexivity.
        + right. exists (ResExc v). split; auto. right.
          intros. split. intros. intro. inversion H1.
          edestruct Ht; eauto.
          destruct H1. destruct H1. inversion H1.
          destruct H1; auto.
      - inversion H1; subst.
        + edestruct Ht; eauto; left.
          destruct H2. destruct H2. inversion H2; subst.
          destruct H3. destruct H4. inversion H12; subst.
          edestruct IHHtyping1; eauto.
          split; destruct H6; auto.
          destruct H6. inversion H6.
          destruct H2. exfalso. eapply H2; eauto.
        + edestruct Ht; eauto; right; exists v; split; auto.
          destruct H2. destruct H2. inversion H2; subst.
          destruct H3. destruct H4.
          edestruct IHHtyping1; eauto.
          destruct H6. inversion H6.
          destruct H6. destruct H6. inversion H6; subst.
          destruct H7; [left|right]. destruct H7. destruct H7. destruct H7. subst.
          exists x0. destruct H8. inversion H8; subst. exists τ2.
          repeat (split; auto). inversion H7; subst.
          destruct x0; destruct x1; inversion H9; inversion H0; auto; subst.
          simpl. constructor. simpl. econstructor; eauto. simpl.
          eapply subtype_trans. eapply SubtypeDef; eauto. econstructor; eauto.
          destruct x. destruct H7. exfalso. eapply H7; eauto.
          apply H7.
          destruct H2. exfalso. eapply H2; eauto.
        + edestruct Ht; eauto.
          destruct H2. destruct H2. inversion H2; subst.
          destruct H3. destruct H4.
          eapply IHHtyping2; eauto. inversion H12; subst. apply H11.
          destruct H2. exfalso. eapply H2; eauto.
        + edestruct Ht; eauto.
          destruct H2. destruct H2. inversion H2; subst.
          destruct H3. destruct H4.
          eapply IHHtyping2; eauto.
          destruct H2. exfalso. eapply H2; eauto.
        + edestruct Ht; eauto; left.
          destruct H2. destruct H2. inversion H2.
      - inversion H1; subst.
        + edestruct Ht; eauto; left.
          destruct H2. destruct H2. inversion H2; subst.
          destruct H3. destruct H4. inversion H12; subst.
          edestruct IHHtyping1; eauto.
          split; destruct H6; auto.
          destruct H6. inversion H6.
          destruct H2. exfalso. eapply H2; eauto.
        + edestruct Ht; eauto.
          destruct H2. destruct H2. inversion H2; subst.
          destruct H3. destruct H4.
          eapply IHHtyping1; eauto.
          destruct H2. exfalso. eapply H2; eauto.
        + edestruct Ht; eauto.
          destruct H2. destruct H2. destruct H3. destruct H4.
          left. inversion H12; subst. split; auto.
          edestruct IHHtyping2; eauto. destruct H6. apply H7.
          destruct H6. destruct H6. inversion H6.
          destruct H2. exfalso. eapply H2; eauto.
        + edestruct Ht; eauto; right; exists v; split; auto.
          destruct H2. destruct H2. inversion H2; subst.
          destruct H3. destruct H4.
          edestruct IHHtyping2; eauto.
          destruct H6. inversion H6.
          destruct H6. destruct H6. inversion H6; subst.
          destruct H7; [left|right]. destruct H7. destruct H7. destruct H7. subst.
          exists x0. destruct H8. inversion H8; subst. exists τ1.
          repeat (split; auto). inversion H7; subst.
          destruct x0; destruct x1; inversion H9; inversion H0; auto; subst.
          simpl. constructor. simpl. econstructor; eauto. simpl.
          eapply subtype_trans. eapply SubtypeDef; eauto. econstructor; eauto.
          destruct x. destruct H7. exfalso. eapply H7; eauto.
          apply H7.
          destruct H2. exfalso. eapply H2; eauto.
        + edestruct Ht; eauto; left.
          destruct H2. destruct H2. inversion H2.
      - inversion H0; subst.
        + edestruct Ht; eauto.
          destruct H1. destruct H1. destruct H2. destruct H3.
          edestruct IHHtyping1; eauto. destruct H5.
          eapply IHHtyping3; eauto.
          destruct H5. destruct H5. inversion H5.
          destruct H1. exfalso. eapply H1; eauto.
        + edestruct Ht; eauto.
          destruct H1. destruct H1. destruct H2. destruct H3.
          edestruct IHHtyping1; eauto.
          right. destruct H5. destruct H5. inversion H5; subst.
          exists x0. split; auto.
          destruct H6; [left|right].
          destruct H6. destruct H6. destruct H6. destruct H7. inversion H7.
          apply H6.
          destruct H1. exfalso. eapply H1; eauto.
        + edestruct Ht; eauto.
          destruct H1. destruct H1. destruct H2. destruct H3.
          edestruct IHHtyping2; eauto. destruct H5.
          eapply IHHtyping3; eauto.
          destruct H5. destruct H5. inversion H5.
          destruct H1. exfalso. eapply H1; eauto.
        + edestruct Ht; eauto.
          destruct H1. destruct H1. destruct H2. destruct H3.
          edestruct IHHtyping2; eauto. destruct H5. destruct H5.
          inversion H5; subst. right. exists x0. split; auto.
          destruct H6. destruct H6. destruct H6. destruct H6. destruct H7. inversion H7.
          right. assumption.
          destruct H1. exfalso. eapply H1; eauto.
        + edestruct Ht; eauto; right; exists (ResExc v); split; auto.
          destruct H1. destruct H1. inversion H1.
      - inversion H0; subst.
        + edestruct Ht; eauto.
          destruct H1. destruct H1. inversion H1; subst.
          destruct H2. destruct H3.
          edestruct IHHtyping1; eauto.
          destruct H5. destruct H5. destruct H6. destruct H6. destruct H6.
          destruct H6. destruct H7. inversion H7.
          destruct H6. right. exists v. split; auto. right.
          split; auto. inversion H5; subst. assumption.
          destruct H1. exfalso. eapply H1; eauto.
        + edestruct Ht; eauto.
          destruct H1. destruct H1. inversion H1; subst.
          destruct H2. destruct H3. edestruct IHHtyping1.
          apply H3. apply H8. destruct H5. subst.
          right. eexists. split. reflexivity. right. split; auto.
          intros. intro. inversion H9.
          destruct H5. destruct H5. subst. right.
          eexists. split. reflexivity. right.
          assert (state_correct Γ St).
          { preservation_auto. }
          split; auto. preservation_auto.
          destruct H1. exfalso. eapply H1. reflexivity.
        + edestruct Ht; eauto.
          destruct H1. destruct H1. inversion H1; subst.
          destruct H2. destruct H3. eapply IHHtyping2; eauto.
          destruct H1. eapply IHHtyping2; eauto.
        + edestruct Ht; eauto.
          destruct H1. destruct H1. inversion H1.
      - inversion H0; subst.
        edestruct Ht; eauto.
        right. exists v. split; auto.
        destruct H1. destruct H1. subst.
        left. exists x. exists τ. split; auto.
      - inversion H; subst; left; split; auto.
      - inversion H0; subst.
        edestruct IHHtyping; eauto.
        + split. intros.
          * unfold extend_typing_env in H1.
            destruct (string_dec i x); subst; inversion H1; subst.
            unfold apply_state. unfold setValue. simpl.
            rewrite apply_state_aux_set_value_aux.
            exists (IntVal Z_as_Int._0). simpl; auto.
            unfold apply_state. unfold setValue. simpl.
            rewrite apply_state_aux_set_value_aux_neq; auto.
            destruct HScorrect. apply H2; auto.
          * split.
            { intros. apply nth_error_get_refs_set_value in H1.
              destruct H1. unfold setValue. simpl.
              destruct H1. destruct HScorrect.
              destruct H3. eapply H3; eauto.
              inversion H1. }
            { unfold setValue. simpl. destruct HScorrect. destruct H2. auto. }
        + left. split; destruct H1; auto. eapply state_correct_weaken; eauto.
        + right. destruct H1. exists x0.
          destruct H1; split; auto. subst.
          destruct H2; [left|right]; auto.
          destruct H1. exists x1.
          destruct H1. exists x2.
          destruct H1; split; auto.
          destruct H2; split; auto.
          destruct H3; split; auto.
          destruct H4; split; auto.
          eapply state_correct_weaken; eauto.
          destruct H1; split; auto.
          eapply state_correct_weaken; eauto.
        + edestruct IHHtyping; eauto.
          * split. intros.
            { unfold extend_typing_env in H1.
              destruct (string_dec i x); subst; inversion H1; subst.
              unfold apply_state. unfold setValue. simpl.
              rewrite apply_state_aux_set_value_aux.
              exists (BoolVal false). simpl; auto.
              unfold apply_state. unfold setValue. simpl.
              rewrite apply_state_aux_set_value_aux_neq; auto.
              destruct HScorrect. apply H2; auto. }
            split.
            { intros. apply nth_error_get_refs_set_value in H1.
              destruct H1. unfold setValue. simpl.
              destruct H1. destruct HScorrect.
              destruct H3. eapply H3; eauto.
              inversion H1. }
            { unfold setValue. simpl. destruct HScorrect. destruct H2. auto. }
          * left. destruct H1; split; auto. eapply state_correct_weaken; eauto.
          * right. destruct H1. exists x0. destruct H1; split; auto.
            destruct H2; [left|right]; auto.
            destruct H2. exists x1.
            destruct H2. exists x2.
            destruct H2; split; auto.
            destruct H3; split; auto.
            destruct H4; split; auto.
            destruct H5; split; auto.
            eapply state_correct_weaken; eauto.
            destruct H2; split; auto.
            eapply state_correct_weaken; eauto.
        + edestruct IHHtyping; eauto.
          * split. intros.
            { unfold extend_typing_env in H1.
              destruct (string_dec i0 x); subst; inversion H1; subst.
              unfold apply_state. unfold setValue. simpl.
              rewrite apply_state_aux_set_value_aux.
              exists NullVal. simpl; auto.
              unfold apply_state. unfold setValue. simpl.
              rewrite apply_state_aux_set_value_aux_neq; auto.
              destruct HScorrect. apply H2; auto. }
            split.
            { intros. apply nth_error_get_refs_set_value in H1.
              destruct H1. unfold setValue. simpl.
              destruct H1. destruct HScorrect.
              destruct H3. eapply H3; eauto.
              inversion H1. }
            { unfold setValue. simpl. destruct HScorrect. destruct H2. auto. }
          * left. destruct H1; split; auto. eapply state_correct_weaken; eauto.
          * right. destruct H1. exists x0. destruct H1; split; auto.
            destruct H2; [left|right]; auto.
            destruct H2. exists x1.
            destruct H2. exists x2.
            destruct H2; split; auto.
            destruct H3; split; auto.
            destruct H4; split; auto.
            destruct H5; split; auto.
            eapply state_correct_weaken; eauto.
            destruct H2; split; auto.
            eapply state_correct_weaken; eauto.
      - inversion H0; subst.
        + edestruct Ht; eauto.
          destruct H1. destruct H1. destruct H2. destruct H3.
          eapply IHHtyping; eauto.
          exfalso. eapply H1. reflexivity.
        + right. exists (ResExc v). split; auto. right.
          intros. split. intros. intro. inversion H1.
          edestruct Ht; eauto.
          destruct H1. destruct H1. inversion H1.
          destruct H1; auto.
      - inversion H1; subst.
        + edestruct Ht; eauto; left.
          destruct H2. destruct H2. inversion H2; subst.
          destruct H3. destruct H4. inversion H12; subst.
          edestruct IHHtyping1; eauto.
          split; destruct H6; auto.
          destruct H6. inversion H6.
          destruct H2. exfalso. eapply H2; eauto.
        + edestruct Ht; eauto; right; exists v; split; auto.
          destruct H2. destruct H2. inversion H2; subst.
          destruct H3. destruct H4.
          edestruct IHHtyping1; eauto.
          destruct H6. inversion H6.
          destruct H6. destruct H6. inversion H6; subst.
          destruct H7; [left|right]. destruct H7. destruct H7. destruct H7. subst.
          exists x0. destruct H8. inversion H8; subst. exists τ2.
          repeat (split; auto). inversion H7; subst.
          destruct x0; destruct x1; inversion H9; inversion H0; auto; subst.
          simpl. constructor. simpl. econstructor; eauto. simpl.
          eapply subtype_trans. eapply SubtypeDef; eauto. econstructor; eauto.
          destruct x. destruct H7. exfalso. eapply H7; eauto.
          apply H7.
          destruct H2. exfalso. eapply H2; eauto.
        + edestruct Ht; eauto.
          destruct H2. destruct H2. inversion H2; subst.
          destruct H3. destruct H4.
          eapply IHHtyping2; eauto. inversion H12; subst. apply H11.
          destruct H2. exfalso. eapply H2; eauto.
        + edestruct Ht; eauto.
          destruct H2. destruct H2. inversion H2; subst.
          destruct H3. destruct H4.
          eapply IHHtyping2; eauto.
          destruct H2. exfalso. eapply H2; eauto.
        + edestruct Ht; eauto; left.
          destruct H2. destruct H2. inversion H2.
      - inversion H1; subst.
        + edestruct Ht; eauto; left.
          destruct H2. destruct H2. inversion H2; subst.
          destruct H3. destruct H4. inversion H12; subst.
          edestruct IHHtyping1; eauto.
          split; destruct H6; auto.
          destruct H6. inversion H6.
          destruct H2. exfalso. eapply H2; eauto.
        + edestruct Ht; eauto.
          destruct H2. destruct H2. inversion H2; subst.
          destruct H3. destruct H4.
          eapply IHHtyping1; eauto.
          destruct H2. exfalso. eapply H2; eauto.
        + edestruct Ht; eauto.
          destruct H2. destruct H2. destruct H3. destruct H4.
          left. inversion H12; subst. split; auto.
          edestruct IHHtyping2; eauto. destruct H6. apply H7.
          destruct H6. destruct H6. inversion H6.
          destruct H2. exfalso. eapply H2; eauto.
        + edestruct Ht; eauto; right; exists v; split; auto.
          destruct H2. destruct H2. inversion H2; subst.
          destruct H3. destruct H4.
          edestruct IHHtyping2; eauto.
          destruct H6. inversion H6.
          destruct H6. destruct H6. inversion H6; subst.
          destruct H7; [left|right]. destruct H7. destruct H7. destruct H7. subst.
          exists x0. destruct H8. inversion H8; subst. exists τ1.
          repeat (split; auto). inversion H7; subst.
          destruct x0; destruct x1; inversion H9; inversion H0; auto; subst.
          simpl. constructor. simpl. econstructor; eauto. simpl.
          eapply subtype_trans. eapply SubtypeDef; eauto. econstructor; eauto.
          destruct x. destruct H7. exfalso. eapply H7; eauto.
          apply H7.
          destruct H2. exfalso. eapply H2; eauto.
        + edestruct Ht; eauto; left.
          destruct H2. destruct H2. inversion H2.
      - inversion H0; subst.
        + edestruct Ht; eauto.
          destruct H1. destruct H1. destruct H2. destruct H3.
          edestruct IHHtyping1; eauto. destruct H5.
          eapply IHHtyping3; eauto.
          destruct H5. destruct H5. inversion H5.
          destruct H1. exfalso. eapply H1; eauto.
        + edestruct Ht; eauto.
          destruct H1. destruct H1. destruct H2. destruct H3.
          edestruct IHHtyping1; eauto.
          right. destruct H5. destruct H5. inversion H5; subst.
          exists x0. split; auto.
          destruct H6; [left|right].
          destruct H6. destruct H6. destruct H6. destruct H7. inversion H7.
          apply H6.
          destruct H1. exfalso. eapply H1; eauto.
        + edestruct Ht; eauto.
          destruct H1. destruct H1. destruct H2. destruct H3.
          edestruct IHHtyping2; eauto. destruct H5.
          eapply IHHtyping3; eauto.
          destruct H5. destruct H5. inversion H5.
          destruct H1. exfalso. eapply H1; eauto.
        + edestruct Ht; eauto.
          destruct H1. destruct H1. destruct H2. destruct H3.
          edestruct IHHtyping2; eauto. destruct H5. destruct H5.
          inversion H5; subst. right. exists x0. split; auto.
          destruct H6. destruct H6. destruct H6. destruct H6. destruct H7. inversion H7.
          right. assumption.
          destruct H1. exfalso. eapply H1; eauto.
        + edestruct Ht; eauto; right; exists (ResExc v); split; auto.
          destruct H1. destruct H1. inversion H1.
      - inversion H0; subst.
        + edestruct Ht; eauto.
          destruct H1. destruct H1. inversion H1; subst.
          destruct H2. destruct H3.
          edestruct IHHtyping1; eauto. destruct H5.
          eapply IHt. eauto. apply H6.
          2: apply H10.
          econstructor; eauto.
          destruct H5. destruct H5. inversion H5.
          destruct H1. exfalso. eapply H1; eauto.
        + edestruct Ht; eauto.
          destruct H1. destruct H1. inversion H1; subst.
          destruct H2. destruct H3.
          edestruct IHHtyping1; eauto.
          destruct H5. destruct H5. destruct H6. destruct H6. destruct H6.
          destruct H6. destruct H7. inversion H7.
          destruct H6. right. exists v. split; auto. right.
          split; auto. inversion H5; subst. assumption.
          destruct H1. exfalso. eapply H1; eauto.
        + edestruct Ht; eauto.
          destruct H1. destruct H1. inversion H1; subst.
          destruct H2. destruct H3. eapply IHHtyping2; eauto.
          destruct H1. eapply IHHtyping2; eauto.
        + edestruct Ht; eauto.
          destruct H1. destruct H1. inversion H1.
      - inversion H0; subst.
        edestruct Ht; eauto.
        right. exists v. split; auto.
        destruct H1. destruct H1. subst.
        left. exists x. exists τ. split; auto.
    Qed.

    Lemma flat_map_app { A B : Type } : ∀ l1 l2 f,
      flat_map f (l1 ++ l2) = flat_map f l1 ++ flat_map f l2 (A := A) (B := B).
    Proof.
      induction l1; intros.
      - simpl. reflexivity.
      - simpl. rewrite IHl1. rewrite app_assoc. reflexivity.
    Qed.

    Lemma get_refs_new_object : ∀ f s c,
      get_refs (addObject s (mkObject W c f)) = get_refs s ++ get_fields_init_vals f.
    Proof.
      intros.
      destruct s. unfold get_refs. simpl.
      rewrite flat_map_app. simpl. unfold get_fields_init_vals.
      rewrite <- app_nil_end. rewrite app_assoc. reflexivity.
    Qed.

    Lemma no_refs_in_new_object : ∀ f k x i n,
      nth_error (get_fields_init_vals f) k ≠ Some (x, RefVal i n).
    Proof.
      induction f; intros.
      - simpl. destruct k; simpl; intro; inversion H.
      - simpl. destruct k; destruct a; simpl. destruct j; simpl; intro; inversion H.
        apply IHf.
    Qed.

    Lemma fields_present_in_new_object : ∀ f t x ls,
      In (Var t x) f →
      get_fields_init_vals f = ls →
      ∃ n v, nth_error ls n = Some (x, v) ∧ has_type v t.
    Proof.
      induction f; intros.
      - inversion H.
      - simpl in H. simpl in H0. destruct a.
        destruct H. exists 0%nat. inversion H; subst. simpl. destruct t.
        exists NullVal. split; auto. constructor.
        exists (IntVal Z_as_Int._0). split; auto. constructor.
        exists (BoolVal false). split; auto. constructor.
        destruct ls; inversion H0; subst.
        apply (IHf t x (get_fields_init_vals f)) in H; auto.
        destruct H. exists (S x0). simpl. apply H.
    Qed.

    Lemma get_fields_init_vals_nth : ∀ f n x v,
      nth_error (get_fields_init_vals f) n = Some (x, v) →
      ∃ t, nth_error f n = Some (Var t x).
    Proof.
      induction f; intros.
      simpl in H. destruct n; inversion H.
      destruct n.
      - destruct a. simpl in H. inversion H; subst. simpl. exists j. reflexivity.
      - simpl in H. simpl. eapply IHf; eauto.
    Qed.

    Lemma get_field_init_vals_fields_unique : ∀ f n n' x v v',
      (∀ n n' t t' v, nth_error f n = Some (Var t v) → nth_error f n' = Some (Var t' v) → n = n') →
      nth_error (get_fields_init_vals f) n = Some (x, v) →
      nth_error (get_fields_init_vals f) n' = Some (x, v') →
      n = n'.
    Proof.
      induction f; intros.
      simpl in *. destruct n; inversion H0.
      destruct n; destruct n'; auto.
      simpl in H0. simpl in H1. destruct a. inversion H0; subst.
        apply get_fields_init_vals_nth in H1. destruct H1.
        apply (H 0%nat (S n') j x0 x); auto.
      simpl in H0. simpl in H1. destruct a. inversion H1; subst.
        apply get_fields_init_vals_nth in H0. destruct H0.
        apply (H (S n) 0%nat x0 j x); auto.
      simpl in H0. simpl in H1. erewrite (IHf n n'); eauto.
      intros.
      assert (S n0 = S n'0).
      { eapply H; eauto. }
      inversion H4; subst. reflexivity.
    Qed.

    Lemma fields_unique_new_object : ∀ f c s ls n n' v v' x,
      (∀ n n' t t' v, nth_error f n = Some (Var t v) → nth_error f n' = Some (Var t' v) → n = n') →
      mkObject W c f = ObjectDef s ls →
      nth_error ls n = Some (x, v) →
      nth_error ls n' = Some (x, v') →
      n = n'.
    Proof.
      intros. unfold mkObject in H0. inversion H0; subst.
      eapply get_field_init_vals_fields_unique; eauto.
    Qed.

    Lemma state_correct_add_new_object : ∀ Γ s c C f,
      get_class W c = Some C →
      fields W C f →
      state_correct Γ s →
      state_correct Γ (addObject s (mkObject W c f)).
    Proof.
      intros.
      destruct H1; split; auto.
      destruct H2. split.
      - intros. rewrite get_refs_new_object in H4.
        destruct (lt_dec k (length (get_refs s))).
        rewrite nth_error_app1 in H4; auto.
        apply H2 in H4. destruct H4.
        unfold addObject. simpl. exists x0. rewrite nth_error_app1; auto.
        eapply nth_error_Some; eauto. intro. rewrite H4 in H5. inversion H5.
        rewrite nth_error_app2 in H4. apply no_refs_in_new_object in H4. destruct H4.
        omega.
      - unfold addObject. simpl.
        unfold heap_correct. split. intros.
        apply in_app_or in H4. destruct H4.
        eapply H3; eauto.
        simpl in H4. destruct H4; [|contradiction].
        unfold mkObject in H4. inversion H4; subst.
        rewrite H in H5. inversion H5; subst.
        eapply fields_present_in_new_object; eauto.
        assert (f = fs). { eapply Hfields_determ; eauto. }
        subst. reflexivity.
        unfold fields_unique. intros.
        apply in_app_or in H4. destruct H4.
        destruct H3. unfold fields_unique in H7. eapply H7; eauto.
        simpl in H4. destruct H4; [|destruct H4].
        eapply fields_unique_new_object; eauto.
        intros. eapply Hfield_decl_unique; eauto.
        eapply get_class_In_W; eauto.
    Qed.

    Lemma preservation_aux_l_add_new_object : ∀ vs vt Γ s c C f,
      get_class W c = Some C →
      fields W C f →
      preservation_aux_l vs vt Γ s →
      preservation_aux_l vs vt Γ (addObject s (mkObject W c f)).
    Proof.
      induction vs; intros. simpl. auto.
      simpl in H1. destruct vt. destruct H1. destruct H1.
      simpl. split; preservation_auto.
      - eapply state_correct_add_new_object; eauto.
      - eapply state_correct_add_new_object; eauto.
      - exists x1. split; auto.
        rewrite nth_error_app1; auto.
        eapply nth_error_Some; eauto. intro. rewrite H6 in H4. inversion H4.
      - eapply IHvs; eauto.
      - eapply IHvs; eauto.
    Qed.

    Lemma aop_is_int : ∀ v1 v2 o v, aop v1 v2 o = ResData v → has_type v Int.
    Proof.
      intros. destruct o; simpl in H; inversion H; simpl; auto.
      destruct (Z_as_Int.eq_dec v2 0); inversion H; simpl; auto.
      destruct (Z_as_Int.eq_dec v2 0); inversion H; simpl; auto.
    Qed.

    Lemma preservation_aux : ∀ t e Γ τ St0 St r,
                             state_correct Γ St0 →
                             T[ Γ ⊢ e :→ τ ] →
                             E[ St0 ⊢ e ⇓ r | St @ t ] →
                             (∃ v, r = ResData v ∧
                               has_type v τ ∧ state_correct Γ St ∧
                               ((is_object v ∧ (∃ x n o, v = RefVal x n ∧
                                 nth_error (heap St) n = Some (ObjectDef x o))) ∨ ¬ is_object v)) ∨
                             ((∀ v, r ≠ ResData v) ∧ state_correct Γ St).
    Proof.
      induction t using nat_ind_strong.
      - induction e using jExpr_ind2 with
        (Q := fun l : list jExpr =>
          ∀ s vl vt s' Γ,
            state_correct Γ s →
            (∀ n a, nth_error l n = Some a → ∃ tt, nth_error vt n = Some tt ∧ T[ Γ ⊢ a :→ tt ]) →
            evaluates_args s l vl s' 0  →
            preservation_aux_l vl vt Γ s');
        try (intros Γ τ St0 St r HS HT HE; destruct r;
             inversion HT; inversion HE; subst; intros; preservation_auto);
        intros.
        + unfold state_correct in HS.
          destruct HS. apply H in H0. destruct H0. destruct H0. rewrite H5 in H0.
          inversion H0; subst. assumption.
        + destruct v; preservation_auto.
          unfold state_correct in HS. destruct HS.
          destruct H1.
          apply apply_state_nth_error in H5.
          destruct H5. apply H1 in H3. destruct H3. eauto.
        + destruct H13. destruct H13.
          unfold state_correct. split.
          * assumption.
          * destruct (eval_heap_entry_remains 0 e2 s' (ResData (RefVal x3 x4)) s'' x1 x0 x2 H12 H17).
            unfold state_heap_correct in H10.
            destruct (get_class_subclass x0 x1 c C f τ1 s H6 H3 H4 H5).
            destruct H20. destruct H20. destruct H21. (* destruct H22.*)
            eapply set_field_state_heap_correct; eauto.
            unfold state_heap_correct. split; assumption.
            simpl. constructor.
            left. eauto.
        + destruct (eval_heap_entry_remains 0 e2 s' (ResData x) s'' x1 x0 x2 H12 H17).
          destruct H13.
          unfold state_correct. split.
          * assumption.
          * unfold state_heap_correct in H10.
            destruct (get_class_subclass x0 x1 c C f τ1 s H6 H3 H4 H5).
            destruct H19. destruct H19. destruct H20. (* destruct H22.*)
            eapply set_field_state_heap_correct; eauto.
            simpl. constructor.
            right. intros. intro; subst. simpl in H15. auto.
        + simpl.
          destruct (set_field_get_heap s'' x1 s (RefVal x3 x4) hp x4 H18).
          exists x5. split; auto. rewrite H; auto.
          destruct H; subst. destruct H15. destruct H. destruct H.
          rewrite H19 in H. inversion H; subst. clear H.
          destruct H15. simpl in H.
          destruct (set_field_aux_def x5 s (RefVal x3 x1)); inversion H; subst.
          exists l; auto.
        + unfold state_correct. split; simpl; intros.
          * unfold state_correct in H6.
            unfold apply_state. simpl.
            destruct s'. simpl.
            destruct (string_dec s i); subst.
            rewrite H1 in H. inversion H; subst.
            apply H6 in H1. destruct H1. destruct H1. unfold apply_state in H1.
            simpl in H1. eapply set_value_apply_state_aux; eauto.
            left. simpl. simpl in H5. eapply subtype_trans; eauto.
            apply H6 in H. destruct H. destruct H. unfold apply_state in H.
            simpl in H. eapply set_value_apply_state_aux; eauto.
          * unfold state_heap_correct. split; intros.
            unfold state_correct in H6.
            destruct H6. unfold state_heap_correct in H7. destruct H7.
            unfold setValue. simpl.
            destruct (nth_error_get_refs_set_value s' s (RefVal x0 x1) k (x, RefVal i n)); auto.
            destruct H10. eapply H7; eauto.
            inversion H10; subst. eauto.
            destruct H6. destruct H6. unfold setValue. simpl. assumption.
        + unfold state_correct. split; simpl; intros.
          * unfold state_correct in H6.
            unfold apply_state. simpl.
            destruct s'. simpl.
            destruct (string_dec s i); subst.
            rewrite H1 in H. inversion H; subst.
            apply H6 in H1. destruct H1. destruct H1. unfold apply_state in H1.
            simpl in H1. eapply set_value_apply_state_aux; eauto.
            left. simpl. eapply has_type_subtype; eauto.
            apply H6 in H. destruct H. destruct H. unfold apply_state in H.
            simpl in H. eapply set_value_apply_state_aux; eauto.
          * unfold state_heap_correct. split; intros.
            unfold state_correct in H6.
            destruct H6. unfold state_heap_correct in H8. destruct H8.
            unfold setValue. simpl.
            destruct (nth_error_get_refs_set_value s' s x k (x0, RefVal i n)); auto.
            destruct H10. eapply H8; eauto.
            inversion H10; subst. simpl in H7. contradiction.
            destruct H6. destruct H6. unfold setValue. simpl. assumption.
        + exists x2. split. reflexivity.
          unfold setValue; simpl. assumption.
        + destruct H7. destruct H7. unfold heap_correct in H8.
          simpl in H6.
          edestruct get_class_subclass; eauto.
          destruct H9. destruct H9. destruct H13.
          rewrite H11 in H12. inversion H12; subst.
          apply nth_error_In in H11. destruct H8. edestruct H8; eauto.
          destruct H17. destruct H17. apply In_nth_error in H14. destruct H14.
          assert (x4 = x6).
          { eapply H16; eauto. }
          subst. rewrite H14 in H17. inversion H17; subst. assumption.
        + destruct v; preservation_auto.
          destruct H7. destruct H7.
          assert (∃ k : nat, nth_error (get_refs St) k = Some (s, RefVal s0 n)).
          { eapply field_in_heap; eauto. rewrite H11 in H12. inversion H12; subst.
            assumption. }
          destruct H9. edestruct H7; eauto.
        + simpl. auto.
        + left. rewrite H10. preservation_auto. eapply aop_is_int; eauto.
          right. apply aop_is_int in H10. destruct v; inversion H10. preservation_auto.
        + right. rewrite H10. preservation_auto.
        + simpl. auto.
        + simpl. auto.
        + exists x2. split; auto.
        + exists x2. split; auto.
        + simpl. auto.
        + exists x2. split; auto.
        + exists x2. split; auto.
        + simpl. auto.
        + simpl. auto.
        + simpl. auto.
        + simpl. auto.
        + exists x2. split; auto.
        + exists x2. split; auto.
        + inversion H1; subst. simpl. auto.
        + inversion H1; subst. destruct vt. simpl.
          destruct (H0 0%nat e). simpl. reflexivity.
          destruct H2. simpl in H2. inversion H2.
          simpl. split.
          * destruct (H0 0%nat e).
            reflexivity.
            simpl in H2.
            destruct H2. inversion H2. subst. clear H2.
            destruct (IHe Γ x s s'0 (ResData v) H H3 H5).
            destruct H2. destruct H2. inversion H2; subst. clear H2. destruct H4.
            split. assumption. split.
            assert (preservation_aux_l vs vt Γ s').
            { apply (IHe0 s'0 vs vt s' Γ). destruct H4. assumption.
              intro n. apply (H0 (S n)). assumption. }
            destruct vs; simpl in H6.
            inversion H9; subst. destruct H4. assumption.
            destruct vt. inversion H6.
            destruct H6. destruct H6. destruct H8. assumption.
            destruct H4. destruct H6.
            left. destruct H6. split. assumption. destruct H7. exists x1.
            destruct H7. exists x2. destruct H7. destruct H7.
            edestruct (eval_args_heap_entry_remains 0 l s'0 vs s').
            apply H8. auto. exists x4. split; assumption.
            right. assumption.
            exfalso. eapply H2. auto.
          * destruct (H0 0%nat e).
            reflexivity.
            simpl in H2.
            destruct H2. inversion H2. subst. clear H2.
            destruct (IHe Γ x s s'0 (ResData v) H H3 H5).
            destruct H2. destruct H2. inversion H2; subst. clear H2. destruct H4.
            destruct H4.
            eapply IHe0; eauto. intro n. apply (H0 (S n)).
            exfalso. eapply H2. auto.
      - induction e using jExpr_ind2 with
        (Q := fun l : list jExpr =>
          ∀ s vl ex vt s' Γ,
            state_correct Γ s →
            (∀ n a, nth_error l n = Some a → ∃ tt, nth_error vt n = Some tt ∧ T[ Γ ⊢ a :→ tt ]) →
            (evaluates_args s l vl s' t ∨ evaluates_args_exc s l ex vl s' t)  →
            preservation_aux_l vl vt Γ s');
        try (intros Γ τ St0 St r HS HT HE; destruct r;
             inversion HT; inversion HE; subst; intros; preservation_auto);
        intros.
        + unfold state_correct in HS.
          destruct HS. apply H0 in H1. destruct H1. destruct H1. rewrite H6 in H1.
          inversion H1; subst. assumption.
        + destruct v; preservation_auto.
          unfold state_correct in HS. destruct HS.
          destruct H2.
          apply apply_state_nth_error in H6.
          destruct H6. apply H2 in H4. destruct H4. eauto.
        + destruct H14. destruct H14.
          unfold state_correct. split.
          * assumption.
          * destruct (eval_heap_entry_remains (S t) e2 s' (ResData (RefVal x3 x4)) s'' x1 x0 x2 H13 H18).
            unfold state_heap_correct in H11.
            destruct (get_class_subclass x0 x1 c C f τ1 s H7 H4 H5 H6).
            destruct H21. destruct H21. destruct H22. (* destruct H22.*)
            eapply set_field_state_heap_correct; eauto.
            unfold state_heap_correct. split; assumption.
            simpl. constructor.
            left. eauto.
        + destruct (eval_heap_entry_remains (S t) e2 s' (ResData x) s'' x1 x0 x2 H13 H18).
          destruct H14.
          unfold state_correct. split.
          * assumption.
          * unfold state_heap_correct in H11.
            destruct (get_class_subclass x0 x1 c C f τ1 s H7 H4 H5 H6).
            destruct H20. destruct H20. destruct H21. (* destruct H22.*)
            eapply set_field_state_heap_correct; eauto.
            simpl. constructor.
            right. intros. intro; subst. simpl in H16. auto.
        + simpl.
          destruct (set_field_get_heap s'' x1 s (RefVal x3 x4) hp x4 H19).
          exists x5. split; auto. rewrite H0; auto.
          destruct H0; subst. destruct H16. destruct H0. destruct H0.
          rewrite H20 in H0. inversion H0; subst. clear H0.
          destruct H16. simpl in H0.
          destruct (set_field_aux_def x5 s (RefVal x3 x1)); inversion H0; subst.
          exists l; auto.
        + unfold state_correct. split; simpl; intros.
          * unfold state_correct in H7.
            unfold apply_state. simpl.
            destruct s'. simpl.
            destruct (string_dec s i); subst.
            rewrite H2 in H0. inversion H0; subst.
            apply H7 in H2. destruct H2. destruct H2. unfold apply_state in H2.
            simpl in H2. eapply set_value_apply_state_aux; eauto.
            left. simpl. simpl in H6. eapply subtype_trans; eauto.
            apply H7 in H0. destruct H0. destruct H0. unfold apply_state in H0.
            simpl in H0. eapply set_value_apply_state_aux; eauto.
          * unfold state_heap_correct. split; intros.
            unfold state_correct in H7.
            destruct H7. unfold state_heap_correct in H8. destruct H8.
            unfold setValue. simpl.
            destruct (nth_error_get_refs_set_value s' s (RefVal x0 x1) k (x, RefVal i n)); auto.
            destruct H11. eapply H8; eauto.
            inversion H11; subst. eauto.
            destruct H7. destruct H7. unfold setValue. simpl. assumption.
        + unfold state_correct. split; simpl; intros.
          * unfold state_correct in H7.
            unfold apply_state. simpl.
            destruct s'. simpl.
            destruct (string_dec s i); subst.
            rewrite H2 in H0. inversion H0; subst.
            apply H7 in H2. destruct H2. destruct H2. unfold apply_state in H2.
            simpl in H2. eapply set_value_apply_state_aux; eauto.
            left. simpl. eapply has_type_subtype; eauto.
            apply H7 in H0. destruct H0. destruct H0. unfold apply_state in H0.
            simpl in H0. eapply set_value_apply_state_aux; eauto.
          * unfold state_heap_correct. split; intros.
            unfold state_correct in H7.
            destruct H7. unfold state_heap_correct in H9. destruct H9.
            unfold setValue. simpl.
            destruct (nth_error_get_refs_set_value s' s x k (x0, RefVal i n)); auto.
            destruct H11. eapply H9; eauto.
            inversion H11; subst. simpl in H8. contradiction.
            destruct H7. destruct H7. unfold setValue. simpl. assumption.
        + exists x2. split. reflexivity.
          unfold setValue; simpl. assumption.
        + assert (preservation_aux_l vs l' Γ s'').
          { eapply IHe0; eauto. }
          assert (state_correct Γ s'').
          { destruct vs; simpl in H0.
            inversion H15; subst; assumption.
            destruct l'. contradiction.
            destruct H0. destruct H0. destruct H12. assumption. }
          assert (map (λ x3 : jVar, match x3 with | Var t0 _ => t0 end) (fst (params, b)) = fst (l0, τ)).
          { eapply mtype_mbody_eq_types_sub. apply H4. apply H16. eauto. eauto.
            unfold has_type in H7. assumption. }
          assert (state_correct
                    (extend_typing_env (extend_typing_env_multi empty_typing_env params) "this" (Named x0))
                    (map_params s'' (RefVal x0 x1) params vs)).
          { edestruct eval_args_heap_entry_remains; eauto.
            eapply state_correct_map_params; eauto.
            unfold has_type. constructor.
            simpl in H12.
            apply evaluates_args_list_lengths in H15. rewrite <- H15.
            assert (length (map (λ x3 : jVar, match x3 with | Var t0 _ => t0 end) params) = length l0).
            { rewrite H12. reflexivity. }
            rewrite map_length in H19. rewrite H19. rewrite H6.
            apply all_subtype_eq_lengths; auto. }
          assert(S[ Named x0 <:: Named c ]). { auto. }
          assert(mtype W s C0 (l0, τ)).
          { destruct (Hmethod_has_type_in_subclass s C (l0, τ) C0 c x0); auto.
            erewrite Hmethod_override_equal_types; eauto. }
          assert (T[ ∅,+* fst (params, b),+ "this" :→ Named x0 ⊢ snd (params, b) :→→ Some (snd (l0, τ))]).
          { apply get_class_eq_name in H16. destruct H16; subst; [inversion H18|]; subst.
            destruct H16. destruct H16. destruct H16. destruct H16. subst.
            eapply Hmethod_body_correct_type; eauto. }
          simpl in H21.
          edestruct preservation_aux_evaluates_seq; eauto.
          destruct H23. inversion H23.
          destruct H23. destruct H23. inversion H23; subst.
          destruct H24. destruct H24. destruct H24. destruct H24. inversion H24; subst.
          destruct H25. inversion H25; subst.
          destruct H26; assumption.
          destruct H24. exfalso. eapply H24; eauto.
        + assert (preservation_aux_l vs l' Γ s'').
          { eapply IHe0; eauto. }
          assert (state_correct Γ s'').
          { destruct vs; simpl in H0.
            inversion H15; subst; assumption.
            destruct l'. contradiction.
            destruct H0. destruct H0. destruct H12. assumption. }
          assert (map (λ x3 : jVar, match x3 with | Var t0 _ => t0 end) (fst (params, b)) = fst (l0, τ)).
          { eapply mtype_mbody_eq_types_sub. apply H4. apply H16. eauto. eauto.
            unfold has_type in H7. assumption. }
          assert (state_correct
                    (extend_typing_env (extend_typing_env_multi empty_typing_env params) "this" (Named x0))
                    (map_params s'' (RefVal x0 x1) params vs)).
          { edestruct eval_args_heap_entry_remains; eauto.
            eapply state_correct_map_params; eauto.
            unfold has_type. constructor.
            simpl in H12.
            apply evaluates_args_list_lengths in H15. rewrite <- H15.
            assert (length (map (λ x3 : jVar, match x3 with | Var t0 _ => t0 end) params) = length l0).
            { rewrite H12. reflexivity. }
            rewrite map_length in H19. rewrite H19. rewrite H6.
            apply all_subtype_eq_lengths; auto. }
          assert(S[ Named x0 <:: Named c ]). { auto. }
          assert(mtype W s C0 (l0, τ)).
          { destruct (Hmethod_has_type_in_subclass s C (l0, τ) C0 c x0); auto.
            erewrite Hmethod_override_equal_types; eauto. }
          assert (T[ ∅,+* fst (params, b),+ "this" :→ Named x0 ⊢ snd (params, b) :→→ Some (snd (l0, τ))]).
          { apply get_class_eq_name in H16. destruct H16; subst; [inversion H18|]; subst.
            destruct H16. destruct H16. destruct H16. destruct H16. subst.
            eapply Hmethod_body_correct_type; eauto. }
          simpl in H21.
          edestruct preservation_aux_evaluates_seq; eauto.
          destruct H23. inversion H23.
          split.
          * intros. unfold apply_state. simpl.
            destruct H11. unfold apply_state in H11. apply H11. assumption.
          * assert (state_correct (∅,+* params,+ "this" :→ Named x0) s'''').
            { destruct H23. destruct H23. destruct H24. destruct H24. destruct H24.
              destruct H24. destruct H25. destruct H26. destruct H27. assumption.
              destruct H24. assumption. }
            destruct H24. destruct H25.
            split; simpl; auto. intros. unfold get_refs in H27. simpl in H27.
            destruct (lt_dec k (length (st s''))).
            rewrite nth_error_app1 in H27; auto.
            destruct H11. destruct H28.
            destruct (H28 k x i n).
            unfold get_refs. rewrite nth_error_app1; auto.
            assert (nth_error (heap (map_params s'' (RefVal x0 x1) params vs)) n = Some (ObjectDef i x3)).
            { rewrite map_params_heap_not_changed. assumption. }
            eapply eval_seq_heap_entry_remains; eauto. intros.
            eapply eval_heap_entry_remains; eauto.
            apply not_lt in n0.
            rewrite nth_error_app2 in H27; auto.
            apply (H25 (k - length (st s'') + length (st s''''))%nat x).
            unfold get_refs. rewrite nth_error_app2. rewrite Nat.add_sub. assumption.
            omega.
        + assert (preservation_aux_l vs l' Γ s'').
          { eapply IHe0; eauto. }
          assert (state_correct Γ s'').
          { destruct vs; simpl in H0.
            inversion H15; subst; assumption.
            destruct l'. contradiction.
            destruct H0. destruct H0. destruct H12. assumption. }
          assert (map (λ x3 : jVar, match x3 with | Var t0 _ => t0 end) (fst (params, b)) = fst (l0, τ)).
          { eapply mtype_mbody_eq_types_sub. apply H4. apply H16. eauto. eauto.
            unfold has_type in H7. assumption. }
          assert (state_correct
                    (extend_typing_env (extend_typing_env_multi empty_typing_env params) "this" (Named x0))
                    (map_params s'' (RefVal x0 x1) params vs)).
          { edestruct eval_args_heap_entry_remains; eauto.
            eapply state_correct_map_params; eauto.
            unfold has_type. constructor.
            simpl in H12.
            apply evaluates_args_list_lengths in H15. rewrite <- H15.
            assert (length (map (λ x3 : jVar, match x3 with | Var t0 _ => t0 end) params) = length l0).
            { rewrite H12. reflexivity. }
            rewrite map_length in H19. rewrite H19. rewrite H6.
            apply all_subtype_eq_lengths; auto. }
          assert(S[ Named x0 <:: Named c ]). { auto. }
          assert(mtype W s C0 (l0, τ)).
          { destruct (Hmethod_has_type_in_subclass s C (l0, τ) C0 c x0); auto.
            erewrite Hmethod_override_equal_types; eauto. }
          assert (T[ ∅,+* fst (params, b),+ "this" :→ Named x0 ⊢ snd (params, b) :→→ Some (snd (l0, τ))]).
          { apply get_class_eq_name in H16. destruct H16; subst; [inversion H18|]; subst.
            destruct H16. destruct H16. destruct H16. destruct H16. subst.
            eapply Hmethod_body_correct_type; eauto. }
          simpl in H21.
          edestruct preservation_aux_evaluates_seq; eauto.
          destruct H23. inversion H23.
          destruct H23. destruct H23. inversion H23; subst.
          destruct H24. destruct H24. destruct H24. destruct H24. inversion H24; subst.
          destruct H25. inversion H25; subst. tauto.
          destruct H24. exfalso. eapply H24; eauto.
        + assert (preservation_aux_l vs l' Γ s'').
          { eapply IHe0; eauto. }
          assert (state_correct Γ s'').
          { destruct vs; simpl in H0.
            inversion H15; subst; assumption.
            destruct l'. contradiction.
            destruct H0. destruct H0. destruct H12. assumption. }
          assert (map (λ x3 : jVar, match x3 with | Var t0 _ => t0 end) (fst (params, b)) = fst (l0, τ)).
          { eapply mtype_mbody_eq_types_sub. apply H4. apply H16. eauto. eauto.
            unfold has_type in H7. assumption. }
          assert (state_correct
                    (extend_typing_env (extend_typing_env_multi empty_typing_env params) "this" (Named x0))
                    (map_params s'' (RefVal x0 x1) params vs)).
          { edestruct eval_args_heap_entry_remains; eauto.
            eapply state_correct_map_params; eauto.
            unfold has_type. constructor.
            simpl in H12.
            apply evaluates_args_list_lengths in H15. rewrite <- H15.
            assert (length (map (λ x3 : jVar, match x3 with | Var t0 _ => t0 end) params) = length l0).
            { rewrite H12. reflexivity. }
            rewrite map_length in H19. rewrite H19. rewrite H6.
            apply all_subtype_eq_lengths; auto. }
          assert(S[ Named x0 <:: Named c ]). { auto. }
          assert(mtype W s C0 (l0, τ)).
          { destruct (Hmethod_has_type_in_subclass s C (l0, τ) C0 c x0); auto.
            erewrite Hmethod_override_equal_types; eauto. }
          assert (T[ ∅,+* fst (params, b),+ "this" :→ Named x0 ⊢ snd (params, b) :→→ Some (snd (l0, τ))]).
          { apply get_class_eq_name in H16. destruct H16; subst; [inversion H18|]; subst.
            destruct H16. destruct H16. destruct H16. destruct H16. subst.
            eapply Hmethod_body_correct_type; eauto. }
          simpl in H21.
          edestruct preservation_aux_evaluates_seq; eauto.
          destruct H23. inversion H23.
          split.
          * intros. unfold apply_state. simpl.
            destruct H11. unfold apply_state in H11. apply H11. assumption.
          * assert (state_correct (∅,+* params,+ "this" :→ Named x0) s'''').
            { destruct H23. destruct H23. destruct H24. destruct H24. destruct H24.
              destruct H24. destruct H25. destruct H26. destruct H27. assumption.
              destruct H24. assumption. }
            destruct H24. destruct H25.
            split; simpl; auto. intros. unfold get_refs in H27. simpl in H27.
            destruct (lt_dec k (length (st s''))).
            rewrite nth_error_app1 in H27; auto.
            destruct H11. destruct H28.
            destruct (H28 k x i n).
            unfold get_refs. rewrite nth_error_app1; auto.
            assert (nth_error (heap (map_params s'' (RefVal x0 x1) params vs)) n = Some (ObjectDef i x3)).
            { rewrite map_params_heap_not_changed. assumption. }
            eapply eval_seq_heap_entry_remains; eauto. intros.
            eapply eval_heap_entry_remains; eauto.
            apply not_lt in n0.
            rewrite nth_error_app2 in H27; auto.
            apply (H25 (k - length (st s'') + length (st s''''))%nat x).
            unfold get_refs. rewrite nth_error_app2. rewrite Nat.add_sub. assumption.
            omega.
        + inversion H18; subst.
          destruct l1.
          * inversion H0; subst.
            destruct (H8 0%nat e1); auto.
            destruct H12.
            edestruct H; eauto; preservation_auto.
          * simpl in H18.
            inversion H0; subst.
            simpl in H8.
            destruct (H8 0%nat j); auto.
            destruct H12.
            edestruct H; eauto; preservation_auto.
            assert (preservation_aux_l (RefVal x4 x5 :: vs) l' Γ St).
            { eapply IHe0. apply H10. assumption. right. simpl. apply H18. }
            destruct l1.
              inversion H0; subst. inversion H22; subst. inversion H31; subst.
              destruct (H8 1%nat e1); auto. destruct H21.
              edestruct H; eauto; preservation_auto.
              inversion H0; subst. inversion H31; subst.
              destruct l'; simpl in H15. inversion H15. preservation_auto.
            assert (preservation_aux_l (x3 :: vs) l' Γ St).
            { eapply IHe0. apply H10. assumption. right. simpl. apply H18. }
            destruct l1.
              inversion H0; subst. inversion H22; subst. inversion H31; subst.
              destruct (H8 1%nat e1); auto. destruct H23.
              edestruct H; eauto; preservation_auto.
              inversion H0; subst. inversion H31; subst.
              destruct l'; simpl in H15. inversion H15. preservation_auto.
        + simpl. constructor.
        + assert (preservation_aux_l vs l' Γ s').
          { eapply IHe; eauto. }
          assert (state_correct Γ s').
          { destruct vs; simpl in H0.
            inversion H12; subst; assumption.
            destruct l'. contradiction.
            preservation_auto. }
          assert (map (λ x3 : jVar, match x3 with | Var t0 _ => t0 end) (fst (cbody C0)) = (ctype C0)).
          { eapply ctype_cbody_eq_types_sub; reflexivity. }
          assert (state_correct Γ (addObject s' (mkObject W s f))).
          { eapply state_correct_add_new_object; eauto. }
          rewrite H2 in H13. inversion H13; subst.
          assert (state_correct
                    (extend_typing_env (extend_typing_env_multi empty_typing_env params) "this" (Named s))
                    (map_params (addObject s' (mkObject W s f)) (RefVal s (length (heap s'))) params vs)).
          { eapply state_correct_map_params; eauto.
            eapply preservation_aux_l_add_new_object; eauto.
            unfold has_type. constructor.
            unfold addObject. simpl. rewrite nth_error_app2; auto. rewrite <- minus_diag_reverse. simpl.
            unfold mkObject. reflexivity.
            simpl in H7.
            apply evaluates_args_list_lengths in H12. rewrite <- H12.
            assert (length (map (λ x3 : jVar, match x3 with | Var t0 _ => t0 end) params) = length (ctype C0)).
            { assert (fst (cbody C0) = params). { rewrite H14. reflexivity. }
              rewrite <- H8. rewrite H3. reflexivity. }
            rewrite map_length in H8. rewrite H8. rewrite H4.
            apply all_subtype_eq_lengths; auto. rewrite H14 in H3. simpl in H3. apply H3. }
          assert (T[ ∅,+* fst (params, b),+ "this" :→ Named s ⊢ snd (params, b) :→→ None]).
          { eapply Hconstructor_body_correct_type; eauto. constructor. }
          simpl in H9.
          edestruct preservation_aux_evaluates_seq; eauto;
          destruct H10; inversion H10.
          split.
          * intros. unfold apply_state. simpl.
            destruct H1. unfold apply_state in H1. apply H1. assumption.
          * assert (state_correct (∅,+* params,+ "this" :→ Named s) s'''').
            { preservation_auto. }
            destruct H11. destruct H16.
            split; simpl; auto. intros. unfold get_refs in H19. simpl in H19.
            destruct (lt_dec k (length (st s'))).
            rewrite nth_error_app1 in H19; auto.
            destruct H1. destruct H21.
            destruct (H21 k x i n).
            unfold get_refs. rewrite nth_error_app1; auto.
            assert (nth_error (heap (addObject s' (mkObject W s f))) n = Some (ObjectDef i x0)).
            { unfold addObject. simpl. rewrite nth_error_app1; auto. apply nth_error_Some; auto.
              intro. rewrite H23 in H24. inversion H24. }
            assert (nth_error (heap (map_params (addObject s' (mkObject W s f)) (RefVal s (length (heap s'))) params vs)) n = Some (ObjectDef i x0)).
            { rewrite map_params_heap_not_changed. assumption. }
            eapply eval_seq_heap_entry_remains; eauto. intros.
            eapply eval_heap_entry_remains; eauto.
            apply not_lt in n0.
            rewrite nth_error_app2 in H19; auto.
            destruct H18. apply (H18 (k - length (st s') + length (st s''''))%nat x).
            unfold get_refs. rewrite nth_error_app2. rewrite Nat.add_sub. assumption.
            omega. destruct H18. assumption.
          * inversion H11.
        + assert (preservation_aux_l vs l' Γ s').
          { eapply IHe; eauto. }
          assert (state_correct Γ s').
          { destruct vs; simpl in H0.
            inversion H12; subst; assumption.
            destruct l'. contradiction.
            preservation_auto. }
          assert (map (λ x3 : jVar, match x3 with | Var t0 _ => t0 end) (fst (cbody C0)) = (ctype C0)).
          { eapply ctype_cbody_eq_types_sub; reflexivity. }
          assert (state_correct Γ (addObject s' (mkObject W s f))).
          { eapply state_correct_add_new_object; eauto. }
          rewrite H2 in H13. inversion H13; subst.
          assert (state_correct
                    (extend_typing_env (extend_typing_env_multi empty_typing_env params) "this" (Named s))
                    (map_params (addObject s' (mkObject W s f)) (RefVal s (length (heap s'))) params vs)).
          { eapply state_correct_map_params; eauto.
            eapply preservation_aux_l_add_new_object; eauto.
            unfold has_type. constructor.
            unfold addObject. simpl. rewrite nth_error_app2; auto. rewrite <- minus_diag_reverse. simpl.
            unfold mkObject. reflexivity.
            simpl in H7.
            apply evaluates_args_list_lengths in H12. rewrite <- H12.
            assert (length (map (λ x3 : jVar, match x3 with | Var t0 _ => t0 end) params) = length (ctype C0)).
            { assert (fst (cbody C0) = params). { rewrite H14. reflexivity. }
              rewrite <- H8. rewrite H3. reflexivity. }
            rewrite map_length in H8. rewrite H8. rewrite H4.
            apply all_subtype_eq_lengths; auto. rewrite H14 in H3. simpl in H3. apply H3. }
          assert (T[ ∅,+* fst (params, b),+ "this" :→ Named s ⊢ snd (params, b) :→→ None]).
          { eapply Hconstructor_body_correct_type; eauto. constructor. }
          simpl in H9.
          edestruct preservation_aux_evaluates_seq; eauto.
          destruct H10. inversion H10.
          preservation_auto. inversion H11. inversion H11.
          assert (nth_error (heap (addObject s' (mkObject W s f))) (length (heap s')) = Some (mkObject W s f)).
          { unfold addObject. simpl. rewrite nth_error_app2; auto. rewrite <- minus_diag_reverse.
            simpl. reflexivity. }
          assert (nth_error (heap (map_params (addObject s' (mkObject W s f)) (RefVal s (length (heap s'))) params vs)) (length (heap s')) = Some (mkObject W s f)).
            { rewrite map_params_heap_not_changed. assumption. }
          unfold mkObject in H18.
          edestruct eval_seq_heap_entry_remains; eauto.
          intros. eapply eval_heap_entry_remains; eauto.
          destruct H10. destruct H10. inversion H10.
        + inversion H13; subst.
          destruct l1.
          * inversion H0; subst.
            destruct (H5 0%nat e0); auto.
            destruct H3.
            edestruct H; eauto; preservation_auto.
          * simpl in H13.
            inversion H0; subst.
            simpl in H5.
            destruct (H5 0%nat j); auto.
            destruct H3.
            edestruct H; eauto; preservation_auto.
            assert (preservation_aux_l (RefVal x1 x2 :: vs0) l' Γ St).
            { eapply IHe. apply HS. assumption. right. simpl. apply H13. }
            destruct l'. simpl in H8. destruct H8.
            simpl in H8. preservation_auto.
            assert (preservation_aux_l (x0 :: vs0) l' Γ St).
            { eapply IHe. apply HS. assumption. right. simpl. apply H13. }
            destruct l'. simpl in H8. destruct H8.
            simpl in H8. preservation_auto.
        + assert (preservation_aux_l vs l' Γ s').
          { eapply IHe; eauto. }
          assert (state_correct Γ s').
          { destruct vs; simpl in H0.
            inversion H12; subst; assumption.
            destruct l'. contradiction.
            preservation_auto. }
          assert (map (λ x3 : jVar, match x3 with | Var t0 _ => t0 end) (fst (cbody C0)) = (ctype C0)).
          { eapply ctype_cbody_eq_types_sub; reflexivity. }
          assert (state_correct Γ (addObject s' (mkObject W s f))).
          { eapply state_correct_add_new_object; eauto. }
          rewrite H2 in H13. inversion H13; subst.
          assert (state_correct
                    (extend_typing_env (extend_typing_env_multi empty_typing_env params) "this" (Named s))
                    (map_params (addObject s' (mkObject W s f)) (RefVal s (length (heap s'))) params vs)).
          { eapply state_correct_map_params; eauto.
            eapply preservation_aux_l_add_new_object; eauto.
            unfold has_type. constructor.
            unfold addObject. simpl. rewrite nth_error_app2; auto. rewrite <- minus_diag_reverse. simpl.
            unfold mkObject. reflexivity.
            simpl in H7.
            apply evaluates_args_list_lengths in H12. rewrite <- H12.
            assert (length (map (λ x3 : jVar, match x3 with | Var t0 _ => t0 end) params) = length (ctype C0)).
            { assert (fst (cbody C0) = params). { rewrite H14. reflexivity. }
              rewrite <- H8. rewrite H3. reflexivity. }
            rewrite map_length in H8. rewrite H8. rewrite H4.
            apply all_subtype_eq_lengths; auto. rewrite H14 in H3. simpl in H3. apply H3. }
          assert (T[ ∅,+* fst (params, b),+ "this" :→ Named s ⊢ snd (params, b) :→→ None]).
          { eapply Hconstructor_body_correct_type; eauto. constructor. }
          simpl in H9.
          edestruct preservation_aux_evaluates_seq; eauto;
          destruct H10; inversion H10.
          split.
          * intros. unfold apply_state. simpl.
            destruct H1. unfold apply_state in H1. apply H1. assumption.
          * assert (state_correct (∅,+* params,+ "this" :→ Named s) s'''').
            { preservation_auto. }
            inversion H11; subst. clear H11.
            destruct H17.
            split; simpl; auto. intros. unfold get_refs in H18. simpl in H18.
            destruct (lt_dec k (length (st s'))).
            rewrite nth_error_app1 in H18; auto.
            destruct H1. destruct H19.
            destruct (H19 k x i n).
            unfold get_refs. rewrite nth_error_app1; auto.
            assert (nth_error (heap (addObject s' (mkObject W s f))) n = Some (ObjectDef i x0)).
            { unfold addObject. simpl. rewrite nth_error_app1; auto. apply nth_error_Some; auto.
              intro. rewrite H22 in H23. inversion H23. }
            assert (nth_error (heap (map_params (addObject s' (mkObject W s f)) (RefVal s (length (heap s'))) params vs)) n = Some (ObjectDef i x0)).
            { rewrite map_params_heap_not_changed. assumption. }
            eapply eval_seq_heap_entry_remains; eauto. intros.
            eapply eval_heap_entry_remains; eauto.
            apply not_lt in n0.
            rewrite nth_error_app2 in H18; auto.
            destruct H17. apply (H17 (k - length (st s') + length (st s''''))%nat x).
            unfold get_refs. rewrite nth_error_app2. rewrite Nat.add_sub. assumption.
            omega. destruct H17. assumption.
        + destruct HS. destruct H1. apply H0 in H2. destruct H2. destruct H2.
          rewrite H14 in H2. inversion H2; subst. assumption.
        + assert (preservation_aux_l vs l' Γ s').
          { eapply IHe; eauto. }
          assert (state_correct Γ s').
          { destruct vs; simpl in H0.
            inversion H16; subst; assumption.
            destruct l'. contradiction.
            preservation_auto. }
          rewrite H3 in H15. inversion H15; subst.
          rewrite H4 in H17. inversion H17; subst.
          assert (map (λ x3 : jVar, match x3 with | Var t0 _ => t0 end) (fst (cbody D0)) = (ctype D0)).
          { eapply ctype_cbody_eq_types_sub; reflexivity. }
          assert (state_correct
                    (extend_typing_env (extend_typing_env_multi empty_typing_env params) "this" (Named s))
                    (map_params s' (RefVal c' n) params vs)).
          { destruct HS. destruct H10.
            apply H8 in H2. destruct H2. destruct H2.
            rewrite H14 in H2. inversion H2; subst.
            apply apply_state_nth_error in H14. destruct H14.
            edestruct H10; eauto.
            edestruct eval_args_heap_entry_remains; eauto.
            eapply state_correct_map_params; eauto.
            apply evaluates_args_list_lengths in H16. rewrite <- H16.
            assert (length (map (λ x3 : jVar, match x3 with | Var t0 _ => t0 end) params) = length (ctype D0)).
            { assert (fst (cbody D0) = params). { rewrite H19. reflexivity. }
              rewrite <- H5. rewrite H19. reflexivity. }
            rewrite map_length in H20. rewrite H20. rewrite H6.
            apply all_subtype_eq_lengths; auto. rewrite H19 in H5. simpl in H5. apply H5. }
          assert (T[ ∅,+* fst (params, b),+ "this" :→ Named s ⊢ snd (params, b) :→→ None]).
          { eapply Hconstructor_body_correct_type. apply H4. eauto. assumption.
            apply get_class_In_W in H3. econstructor; eauto. constructor. }
          simpl in H10.
          edestruct preservation_aux_evaluates_seq; eauto;
          destruct H11; inversion H11.
          split.
          * intros. unfold apply_state. simpl.
            destruct H1. unfold apply_state in H1. apply H1. assumption.
          * assert (state_correct (∅,+* params,+ "this" :→ Named s) s''').
            { preservation_auto. }
            destruct H12. destruct H18.
            split; simpl; auto. intros. unfold get_refs in H21. simpl in H21.
            destruct (lt_dec k (length (st s'))).
            rewrite nth_error_app1 in H21; auto.
            destruct H1. destruct H23.
            destruct (H23 k x i n0).
            unfold get_refs. rewrite nth_error_app1; auto.
            assert (nth_error (heap (map_params s' (RefVal s n) params vs)) n0 = Some (ObjectDef i x0)).
            { rewrite map_params_heap_not_changed. assumption. }
            eapply eval_seq_heap_entry_remains; eauto. intros.
            eapply eval_heap_entry_remains; eauto.
            apply not_lt in n1.
            rewrite nth_error_app2 in H21; auto.
            apply (H18 (k - length (st s') + length (st s'''))%nat x).
            unfold get_refs. rewrite nth_error_app2. rewrite Nat.add_sub. assumption.
            omega.
          * inversion H12.
        + assert (preservation_aux_l vs l' Γ s').
          { eapply IHe; eauto. }
          assert (state_correct Γ s').
          { destruct vs; simpl in H0.
            inversion H16; subst; assumption.
            destruct l'. contradiction.
            preservation_auto. }
          rewrite H3 in H15. inversion H15; subst.
          rewrite H4 in H17. inversion H17; subst.
          assert (map (λ x3 : jVar, match x3 with | Var t0 _ => t0 end) (fst (cbody D0)) = (ctype D0)).
          { eapply ctype_cbody_eq_types_sub; reflexivity. }
          assert (state_correct
                    (extend_typing_env (extend_typing_env_multi empty_typing_env params) "this" (Named s))
                    (map_params s' (RefVal c' n) params vs)).
          { destruct HS. destruct H10.
            apply H8 in H2. destruct H2. destruct H2.
            rewrite H14 in H2. inversion H2; subst.
            apply apply_state_nth_error in H14. destruct H14.
            edestruct H10; eauto.
            edestruct eval_args_heap_entry_remains; eauto.
            eapply state_correct_map_params; eauto.
            apply evaluates_args_list_lengths in H16. rewrite <- H16.
            assert (length (map (λ x3 : jVar, match x3 with | Var t0 _ => t0 end) params) = length (ctype D0)).
            { assert (fst (cbody D0) = params). { rewrite H19. reflexivity. }
              rewrite <- H5. rewrite H19. reflexivity. }
            rewrite map_length in H20. rewrite H20. rewrite H6.
            apply all_subtype_eq_lengths; auto. rewrite H19 in H5. simpl in H5. apply H5. }
          assert (T[ ∅,+* fst (params, b),+ "this" :→ Named s ⊢ snd (params, b) :→→ None]).
          { eapply Hconstructor_body_correct_type. apply H4. eauto. assumption.
            apply get_class_In_W in H3. econstructor; eauto. constructor. }
          simpl in H10.
          edestruct preservation_aux_evaluates_seq; eauto;
          destruct H11; inversion H11.
          apply apply_state_nth_error in H14. destruct H14.
          destruct HS. destruct H18. apply H18 in H13. destruct H13.
          edestruct eval_args_heap_entry_remains; eauto.
          assert (nth_error (heap (map_params s' (RefVal c' n) params vs)) n = Some (ObjectDef c' x1)).
          { rewrite map_params_heap_not_changed. assumption. }
          edestruct eval_seq_heap_entry_remains; eauto.
          intros. eapply eval_heap_entry_remains; eauto.
          inversion H12.
        + inversion H15; subst.
          destruct l1.
          * inversion H0; subst.
            destruct (H7 0%nat e0); auto.
            destruct H5.
            edestruct H; eauto; preservation_auto.
          * simpl in H15.
            inversion H0; subst.
            simpl in H7.
            destruct (H7 0%nat j); auto.
            destruct H5.
            edestruct H; eauto; preservation_auto.
            assert (preservation_aux_l (RefVal x1 x2 :: vs0) l' Γ St).
            { eapply IHe. apply HS. assumption. right. simpl. apply H15. }
            destruct l'. simpl in H10. destruct H10.
            simpl in H10. preservation_auto.
            assert (preservation_aux_l (x0 :: vs0) l' Γ St).
            { eapply IHe. apply HS. assumption. right. simpl. apply H15. }
            destruct l'. simpl in H10. destruct H10.
            simpl in H10. preservation_auto.
        + assert (preservation_aux_l vs l' Γ s').
          { eapply IHe; eauto. }
          assert (state_correct Γ s').
          { destruct vs; simpl in H0.
            inversion H16; subst; assumption.
            destruct l'. contradiction.
            preservation_auto. }
          rewrite H3 in H15. inversion H15; subst.
          rewrite H4 in H17. inversion H17; subst.
          assert (map (λ x3 : jVar, match x3 with | Var t0 _ => t0 end) (fst (cbody D0)) = (ctype D0)).
          { eapply ctype_cbody_eq_types_sub; reflexivity. }
          assert (state_correct
                    (extend_typing_env (extend_typing_env_multi empty_typing_env params) "this" (Named s))
                    (map_params s' (RefVal c' n) params vs)).
          { destruct HS. destruct H10.
            apply H8 in H2. destruct H2. destruct H2.
            rewrite H14 in H2. inversion H2; subst.
            apply apply_state_nth_error in H14. destruct H14.
            edestruct H10; eauto.
            edestruct eval_args_heap_entry_remains; eauto.
            eapply state_correct_map_params; eauto.
            apply evaluates_args_list_lengths in H16. rewrite <- H16.
            assert (length (map (λ x3 : jVar, match x3 with | Var t0 _ => t0 end) params) = length (ctype D0)).
            { assert (fst (cbody D0) = params). { rewrite H19. reflexivity. }
              rewrite <- H5. rewrite H19. reflexivity. }
            rewrite map_length in H20. rewrite H20. rewrite H6.
            apply all_subtype_eq_lengths; auto. rewrite H19 in H5. simpl in H5. apply H5. }
          assert (T[ ∅,+* fst (params, b),+ "this" :→ Named s ⊢ snd (params, b) :→→ None]).
          { eapply Hconstructor_body_correct_type. apply H4. eauto. assumption.
            apply get_class_In_W in H3. econstructor; eauto. constructor. }
          simpl in H10.
          edestruct preservation_aux_evaluates_seq; eauto;
          destruct H11; inversion H11.
          split.
          * intros. unfold apply_state. simpl.
            destruct H1. unfold apply_state in H1. apply H1. assumption.
          * assert (state_correct (∅,+* params,+ "this" :→ Named s) s''').
            { preservation_auto. }
            inversion H12; subst. clear H12.
            destruct H18.
            split; simpl; auto. intros. unfold get_refs in H20. simpl in H20.
            destruct (lt_dec k (length (st s'))).
            rewrite nth_error_app1 in H20; auto.
            destruct H1. destruct H21.
            destruct (H21 k x i n0).
            unfold get_refs. rewrite nth_error_app1; auto.
            assert (nth_error (heap (map_params s' (RefVal s n) params vs)) n0 = Some (ObjectDef i x0)).
            { rewrite map_params_heap_not_changed. assumption. }
            eapply eval_seq_heap_entry_remains; eauto. intros.
            eapply eval_heap_entry_remains; eauto.
            apply not_lt in n1.
            rewrite nth_error_app2 in H20; auto.
            destruct H18. apply (H18 (k - length (st s') + length (st s'''))%nat x).
            unfold get_refs. rewrite nth_error_app2. rewrite Nat.add_sub. assumption.
            omega.
            destruct H18. assumption.
        + destruct H8. destruct H8. unfold heap_correct in H9.
          simpl in H7.
          edestruct get_class_subclass; eauto.
          destruct H10. destruct H10. destruct H14.
          rewrite H12 in H13. inversion H13; subst.
          apply nth_error_In in H12. destruct H9. edestruct H9; eauto.
          destruct H18. destruct H18. apply In_nth_error in H15. destruct H15.
          assert (x4 = x6).
          { eapply H17; eauto. }
          subst. rewrite H15 in H18. inversion H18; subst. assumption.
        + destruct v; preservation_auto.
          destruct H8. destruct H8.
          assert (∃ k : nat, nth_error (get_refs St) k = Some (s, RefVal s0 n)).
          { eapply field_in_heap; eauto. rewrite H12 in H13. inversion H13; subst.
            assumption. }
          destruct H10. edestruct H8; eauto.
        + simpl. auto.
        + left. rewrite H11. preservation_auto. eapply aop_is_int; eauto.
          right. apply aop_is_int in H11. destruct v; inversion H11. preservation_auto.
        + right. rewrite H11. preservation_auto.
        + simpl. auto.
        + simpl. auto.
        + exists x2. split; auto.
        + exists x2. split; auto.
        + simpl. auto.
        + exists x2. split; auto.
        + exists x2. split; auto.
        + simpl. auto.
        + simpl. auto.
        + simpl. auto.
        + simpl. auto.
        + exists x2. split; auto.
        + exists x2. split; auto.
        + destruct H2.
          inversion H2; subst.
          unfold preservation_aux_l. auto.
          inversion H2; subst.
          exfalso. eapply app_cons_not_nil; eauto.
        + destruct H2.
          * inversion H2; subst.
            destruct vt; simpl.
            edestruct (H1 0%nat); eauto. simpl. reflexivity.
            simpl in H3. destruct H3. inversion H3.
            destruct (H1 0%nat e); auto. destruct H3. simpl in H3. inversion H3; subst.
            edestruct H; eauto.
            destruct H5. destruct H5. inversion H5; subst.
            destruct H7. destruct H8.
            assert (preservation_aux_l vs vt Γ s').
            { eapply IHe0; eauto. intros. destruct (H1 (S n) a); auto.
              simpl in H12. exists x1; auto. }
            split; auto. split; auto. split.
            destruct vs. inversion H10; subst. assumption.
            destruct vt; simpl in H11. contradiction.
            destruct H11. destruct H11. destruct H13. assumption.
            destruct H9; [left|right; auto].
            destruct H9; split; auto.
            destruct H12. destruct H12. destruct H12. destruct H12. subst.
            exists x1. exists x2.
            edestruct (eval_args_heap_entry_remains); eauto.
            destruct H5. exfalso. eapply H5; eauto.
          * inversion H2; subst.
            destruct l1. simpl in H3. inversion H3; subst.
            inversion H4; subst. simpl. auto.
            simpl in H3. inversion H3; subst.
            inversion H4; subst.
            destruct (H1 0%nat e); auto. destruct H5.
            edestruct H; eauto; preservation_auto.
            destruct vt. inversion H5. simpl in H5. inversion H5; subst.
            assert (preservation_aux_l vs vt Γ s').
            { eapply IHe0; eauto. intros. destruct (H1 (S n) a); auto.
              simpl in H12. exists x0. apply H12. right.
              econstructor; eauto. }
            simpl. simpl in H10. split; auto. split; auto.
            assert (state_correct Γ s').
            { destruct l1; inversion H13; subst.
              destruct (H1 1%nat e0); auto. destruct H12. edestruct H; eauto; preservation_auto.
              destruct vt; simpl in H8. destruct H8. preservation_auto. }
            split; auto. left. split; auto. preservation_auto.
            eapply eval_args_heap_entry_remains in H15; eauto. destruct H15.
            eapply eval_heap_entry_remains in H6; eauto.
            destruct H6. exists x4. split; auto.
            destruct vt. inversion H5. simpl in H5. inversion H5; subst.
            assert (preservation_aux_l vs vt Γ s').
            { eapply IHe0; eauto. intros. destruct (H1 (S n) a); auto.
              simpl in H14. exists x1. apply H14. right.
              econstructor; eauto. }
            simpl. split; auto. split; auto.
            assert (state_correct Γ s').
            { destruct l1; inversion H13; subst.
              destruct (H1 1%nat e0); auto. destruct H14. edestruct H; eauto; preservation_auto.
              destruct vt; simpl in H8. destruct H8. preservation_auto. }
            split; auto.
    Unshelve.
      constructor.
      constructor.
      constructor.
      constructor.
      constructor.
      constructor.
      constructor.
      constructor.
      constructor.
      constructor.
      constructor.
      constructor.
      constructor.
    Qed.

    Theorem preservation : ∀ t e Γ τ St0 St v,
                           state_correct Γ St0 →
                           T[ Γ ⊢ e :→ τ ] →
                           E[ St0 ⊢ e ⇓ ResData v | St @ t ] →
                           has_type v τ ∧ state_correct Γ St.
    Proof.
      intros.
      destruct (preservation_aux t e Γ τ St0 St (ResData v)); preservation_auto.
    Qed.

    Lemma set_field_some : ∀ Γ s d n l c C f τ1 x v,
      state_correct Γ s →
      nth_error (heap s) n = Some (ObjectDef d l) →
      has_type (RefVal d n) (Named c) →
      get_class W c = Some C →
      fields W C f →
      In (Var τ1 x) f →
      has_type v τ1 →
      ∃ h, set_field (heap s) n x v = Some h.
    Proof.
      intros.
      edestruct get_class_subclass; eauto.
      destruct H6. destruct H6. destruct H7.
      destruct H. destruct H9. destruct H10.
      eapply set_field_some_aux; eauto.
      eapply H10; eauto.
      eapply nth_error_In; eauto.
    Qed.

    Hint Constructors evaluates_expr.

    Lemma mbody_exists : ∀ m C t,
      mtype W m C t →
      ∃ b, mbody W m C b.
    Proof.
      intros.
      induction H.
      eexists; econstructor; eauto.
      destruct IHmtype. destruct x.
      eexists. eapply MbodySuper; eauto.
    Qed.

    Lemma preservation_l : ∀ l l' Γ s s' vs t,
      state_correct Γ s →
      (∀ n e', nth_error l n = Some e' → ∃ τ', nth_error l' n = Some τ' ∧ T[ Γ ⊢ e' :→ τ']) →
      evaluates_args s l vs s' t →
      preservation_aux_l vs l' Γ s'.
    Proof.
      induction l; intros.
      inversion H1; subst. simpl. auto.
      destruct vs; simpl in H1. inversion H1. inversion H1; subst.
      destruct l'; simpl.
      destruct (H0 0%nat a); auto. simpl in H2. destruct H2. inversion H2.
      destruct (H0 0%nat a); auto. simpl in H2. destruct H2. inversion H2; subst.
      edestruct preservation_aux; eauto. destruct H4. destruct H4. inversion H4; subst.
      destruct H5. destruct H6.
      assert (preservation_aux_l vs l' Γ s').
      { eapply IHl; eauto. intros. destruct (H0 (S n) e'); auto.
        simpl in H11. eexists. eauto. }
      split; auto. split; auto.
      assert (state_correct Γ s').
      { destruct vs; simpl in H8. inversion H10; subst. apply H6.
        destruct l'. destruct H8. preservation_auto. }
      split; auto. destruct H7; [left|right].
      destruct H7. split; auto.
      destruct H12. destruct H12. destruct H12. destruct H12. subst.
      edestruct eval_args_heap_entry_remains; eauto. apply H7.
      destruct H4. exfalso. eapply H4; eauto.
    Qed.

    Hint Constructors evaluates_seq.

    Lemma progress_seq_aux : ∀ t b Γ s τ,
      (∀ m, m ≤ t → ∀ e Γ τ St0, state_correct Γ St0 → T[ Γ ⊢ e :→ τ] → ∃ v St, E[ St0 ⊢ e ⇓ v | St @ m]) →
      state_correct Γ s →
      T[ Γ ⊢ b :→→ τ] →
      (∃ r s', evaluates_seq s b r s' t ∧ (τ = None ∧ r = None ∨ (τ = None ∧ ∃ ex, r = Some (ResExc ex)) ∨ τ ≠ None ∧ r ≠ None)).
    Proof.
      induction t; induction b; intros.
      - inversion H1; subst.
        eexists; eexists; split; eauto.
      - inversion H1; subst.
        destruct t.
        assert (state_correct (Γ,+ x :→ Named s0) (setValue s x NullVal)).
        { split. intros.
          { unfold extend_typing_env in H2.
            destruct (string_dec i x); subst; inversion H2; subst.
            unfold apply_state. unfold setValue. simpl.
            rewrite apply_state_aux_set_value_aux.
            exists NullVal. simpl; auto.
            unfold apply_state. unfold setValue. simpl.
            rewrite apply_state_aux_set_value_aux_neq; auto.
            destruct H0. apply H0; auto. }
          { split.
            { intros. apply nth_error_get_refs_set_value in H2.
              destruct H2. unfold setValue. simpl.
              destruct H2. destruct H0.
              destruct H4. eapply H3; eauto.
              inversion H2. }
            { unfold setValue. simpl. destruct H0. destruct H2. auto. } } }
        edestruct IHb. eauto. apply H2. apply H6. destruct H3. destruct H3.
        eauto.
        assert (state_correct (Γ,+ x :→ Int) (setValue s x (IntVal 0))).
        { split. intros.
          { unfold extend_typing_env in H2.
            destruct (string_dec i x); subst; inversion H2; subst.
            unfold apply_state. unfold setValue. simpl.
            rewrite apply_state_aux_set_value_aux.
            exists (IntVal 0). simpl; auto.
            unfold apply_state. unfold setValue. simpl.
            rewrite apply_state_aux_set_value_aux_neq; auto.
            destruct H0. apply H0; auto. }
          { split.
            { intros. apply nth_error_get_refs_set_value in H2.
              destruct H2. unfold setValue. simpl.
              destruct H2. destruct H0.
              destruct H4. eapply H3; eauto.
              inversion H2. }
            { unfold setValue. simpl. destruct H0. destruct H2. auto. } } }
        edestruct IHb. eauto. apply H2. apply H6. destruct H3. destruct H3.
        eauto.
        assert (state_correct (Γ,+ x :→ Boolean) (setValue s x (BoolVal false))).
        { split. intros.
          { unfold extend_typing_env in H2.
            destruct (string_dec i x); subst; inversion H2; subst.
            unfold apply_state. unfold setValue. simpl.
            rewrite apply_state_aux_set_value_aux.
            exists (BoolVal false). simpl; auto.
            unfold apply_state. unfold setValue. simpl.
            rewrite apply_state_aux_set_value_aux_neq; auto.
            destruct H0. apply H0; auto. }
          { split.
            { intros. apply nth_error_get_refs_set_value in H2.
              destruct H2. unfold setValue. simpl.
              destruct H2. destruct H0.
              destruct H4. eapply H3; eauto.
              inversion H2. }
            { unfold setValue. simpl. destruct H0. destruct H2. auto. } } }
        edestruct IHb. eauto. apply H2. apply H6. destruct H3. destruct H3.
        eauto.
      - inversion H1; subst.
        edestruct H; eauto. destruct H2.
        destruct x.
        edestruct preservation; eauto.
        edestruct IHb; eauto. destruct H7. destruct H7. eauto.
        eexists; eexists; split; eauto. right. destruct τ; [right|left].
        split; intro; inversion H3. split; eauto.
      - inversion H1; subst.
        + edestruct H; eauto. destruct H2.
          destruct x.
          edestruct preservation; eauto.
          destruct v; simpl in H3; inversion H3.
          destruct b.
          edestruct IHb1; eauto. destruct H5. destruct H5.
          destruct H7. destruct H7. inversion H7.
          destruct H7. destruct H7. inversion H7.
          destruct H7. destruct x; [|contradiction].
          eexists; eexists; split; eauto.
          right. right. split; auto. intro. inversion H12.
          edestruct IHb2; eauto. destruct H5. destruct H5.
          destruct H7. destruct H7. inversion H7.
          destruct H7. destruct H7. inversion H7.
          destruct H7. destruct x; [|contradiction].
          eexists; eexists; split; eauto.
          eexists; eexists; split; eauto.
          right. right. split; intro; inversion H3.
        + edestruct H; eauto. destruct H2.
          destruct x.
          edestruct preservation; eauto.
          destruct v; simpl in H3; inversion H3.
          destruct b.
          edestruct IHb1; eauto. destruct H5. destruct H5.
          destruct H7. destruct H7. inversion H7.
          destruct H7. destruct H7. inversion H7.
          destruct H7. destruct x; [|contradiction].
          eexists; eexists; split; eauto.
          edestruct IHb2; eauto. destruct H5. destruct H5.
          destruct H7. destruct H7. inversion H7.
          destruct H7. destruct H7. inversion H7.
          destruct H7. destruct x; [|contradiction].
          eexists; eexists; split; eauto.
          right. right. split; auto. intro. inversion H12.
          eexists; eexists; split; eauto.
          right. right. split; intro; inversion H3.
        + edestruct H; eauto. destruct H2.
          destruct x.
          edestruct preservation; eauto.
          destruct v; simpl in H3; inversion H3.
          destruct b.
          edestruct IHb1; eauto. destruct H5. destruct H5.
          assert (state_correct Γ x1).
          { edestruct preservation_aux_evaluates_seq.
            intros; eauto. eapply preservation_aux; eauto. apply H4. apply H6. apply H5.
            destruct H11. assumption. preservation_auto. }
          edestruct IHb3; eauto. destruct H12. destruct H12.
          destruct x.
          destruct H7. destruct H7. inversion H14.
          destruct H7. destruct H7. destruct H14. inversion H14; subst.
          eexists; eexists; split; eauto. right. destruct τ; [right|left]; split; auto.
          intro. inversion H15.
          intro. inversion H15.
          eauto.
          destruct H7. contradiction.
          eexists; eexists; split; eauto.
          edestruct IHb2; eauto. destruct H5. destruct H5.
          assert (state_correct Γ x1).
          { edestruct preservation_aux_evaluates_seq.
            intros; eauto. eapply preservation_aux; eauto. apply H4. apply H8. apply H5.
            destruct H11. assumption. preservation_auto. }
          edestruct IHb3; eauto. destruct H12. destruct H12.
          destruct x.
          destruct H7. destruct H7. inversion H14.
          destruct H7. destruct H7. destruct H14. inversion H14; subst.
          eexists; eexists; split; eauto. right. destruct τ; [right|left]; split; auto.
          intro. inversion H15.
          intro. inversion H15.
          eauto.
          destruct H7. contradiction.
          eexists; eexists; split; eauto.
          eexists; eexists; split; eauto.
          right. destruct τ; [right|left]; split; auto.
          intro. inversion H3.
          intro. inversion H3.
          eauto.
      - inversion H1; subst.
        edestruct H; eauto. destruct H2.
        destruct x.
        edestruct preservation; eauto.
        destruct v; simpl in H3; inversion H3.
        destruct b.
        edestruct IHb1; eauto. destruct H6. destruct H6.
        assert (state_correct Γ x1).
        { edestruct preservation_aux_evaluates_seq.
          intros; eauto. eapply preservation_aux; eauto. apply H4. apply H5. apply H6.
          destruct H10. assumption. preservation_auto. }
        edestruct IHb2; eauto. destruct H11. destruct H11.
        destruct x.
        destruct H9. destruct H9. inversion H13.
        destruct H9.
        eexists; eexists; split; eauto. right.
        destruct τ; [right|left]; split; auto.
        intro. inversion H13.
        intro. inversion H13.
        destruct H9. assumption.
        destruct H9. contradiction.
        eexists; eexists; split; eauto. right.
        destruct τ; [right|left]; split; auto.
        intro. inversion H13.
        intro. inversion H13.
        eauto.
        edestruct IHb2; eauto. destruct H6. destruct H6.
        eexists; eexists; split; eauto.
        eexists; eexists; split; eauto. right.
        destruct τ; [right|left]; split; auto.
        intro. inversion H3.
        intro. inversion H3.
        eauto.
      - inversion H1; subst.
        edestruct H; eauto. destruct H2.
        eexists; eexists; split; eauto.
        right. right. split; intro; inversion H4.
      - inversion H1; subst.
        eexists; eexists; split; eauto.
      - inversion H1; subst.
        destruct t0.
        assert (state_correct (Γ,+ x :→ Named s0) (setValue s x NullVal)).
        { split. intros.
          { unfold extend_typing_env in H2.
            destruct (string_dec i x); subst; inversion H2; subst.
            unfold apply_state. unfold setValue. simpl.
            rewrite apply_state_aux_set_value_aux.
            exists NullVal. simpl; auto.
            unfold apply_state. unfold setValue. simpl.
            rewrite apply_state_aux_set_value_aux_neq; auto.
            destruct H0. apply H0; auto. }
          { split.
            { intros. apply nth_error_get_refs_set_value in H2.
              destruct H2. unfold setValue. simpl.
              destruct H2. destruct H0.
              destruct H4. eapply H3; eauto.
              inversion H2. }
            { unfold setValue. simpl. destruct H0. destruct H2. auto. } } }
        edestruct IHb. eauto. apply H2. apply H6. destruct H3. destruct H3.
        eauto.
        assert (state_correct (Γ,+ x :→ Int) (setValue s x (IntVal 0))).
        { split. intros.
          { unfold extend_typing_env in H2.
            destruct (string_dec i x); subst; inversion H2; subst.
            unfold apply_state. unfold setValue. simpl.
            rewrite apply_state_aux_set_value_aux.
            exists (IntVal 0). simpl; auto.
            unfold apply_state. unfold setValue. simpl.
            rewrite apply_state_aux_set_value_aux_neq; auto.
            destruct H0. apply H0; auto. }
          { split.
            { intros. apply nth_error_get_refs_set_value in H2.
              destruct H2. unfold setValue. simpl.
              destruct H2. destruct H0.
              destruct H4. eapply H3; eauto.
              inversion H2. }
            { unfold setValue. simpl. destruct H0. destruct H2. auto. } } }
        edestruct IHb. eauto. apply H2. apply H6. destruct H3. destruct H3.
        eauto.
        assert (state_correct (Γ,+ x :→ Boolean) (setValue s x (BoolVal false))).
        { split. intros.
          { unfold extend_typing_env in H2.
            destruct (string_dec i x); subst; inversion H2; subst.
            unfold apply_state. unfold setValue. simpl.
            rewrite apply_state_aux_set_value_aux.
            exists (BoolVal false). simpl; auto.
            unfold apply_state. unfold setValue. simpl.
            rewrite apply_state_aux_set_value_aux_neq; auto.
            destruct H0. apply H0; auto. }
          { split.
            { intros. apply nth_error_get_refs_set_value in H2.
              destruct H2. unfold setValue. simpl.
              destruct H2. destruct H0.
              destruct H4. eapply H3; eauto.
              inversion H2. }
            { unfold setValue. simpl. destruct H0. destruct H2. auto. } } }
        edestruct IHb. eauto. apply H2. apply H6. destruct H3. destruct H3.
        eauto.
      - inversion H1; subst.
        edestruct H; eauto. destruct H2.
        destruct x.
        edestruct preservation; eauto.
        edestruct IHb; eauto. destruct H7. destruct H7. eauto.
        eexists; eexists; split; eauto. right. destruct τ; [right|left].
        split; intro; inversion H3. split; eauto.
      - inversion H1; subst.
        + edestruct H; eauto. destruct H2.
          destruct x.
          edestruct preservation; eauto.
          destruct v; simpl in H3; inversion H3.
          destruct b.
          edestruct IHb1; eauto. destruct H5. destruct H5.
          destruct H7. destruct H7. inversion H7.
          destruct H7. destruct H7. inversion H7.
          destruct H7. destruct x; [|contradiction].
          eexists; eexists; split; eauto.
          right. right. split; auto. intro. inversion H12.
          edestruct IHb2; eauto. destruct H5. destruct H5.
          destruct H7. destruct H7. inversion H7.
          destruct H7. destruct H7. inversion H7.
          destruct H7. destruct x; [|contradiction].
          eexists; eexists; split; eauto.
          eexists; eexists; split; eauto.
          right. right. split; intro; inversion H3.
        + edestruct H; eauto. destruct H2.
          destruct x.
          edestruct preservation; eauto.
          destruct v; simpl in H3; inversion H3.
          destruct b.
          edestruct IHb1; eauto. destruct H5. destruct H5.
          destruct H7. destruct H7. inversion H7.
          destruct H7. destruct H7. inversion H7.
          destruct H7. destruct x; [|contradiction].
          eexists; eexists; split; eauto.
          edestruct IHb2; eauto. destruct H5. destruct H5.
          destruct H7. destruct H7. inversion H7.
          destruct H7. destruct H7. inversion H7.
          destruct H7. destruct x; [|contradiction].
          eexists; eexists; split; eauto.
          right. right. split; auto. intro. inversion H12.
          eexists; eexists; split; eauto.
          right. right. split; intro; inversion H3.
        + edestruct H; eauto. destruct H2.
          destruct x.
          edestruct preservation; eauto.
          destruct v; simpl in H3; inversion H3.
          destruct b.
          edestruct IHb1; eauto. destruct H5. destruct H5.
          assert (state_correct Γ x1).
          { edestruct preservation_aux_evaluates_seq.
            intros; eauto. eapply preservation_aux; eauto. apply H4. apply H6. apply H5.
            destruct H11. assumption. preservation_auto. }
          edestruct IHb3; eauto. destruct H12. destruct H12.
          destruct x.
          destruct H7. destruct H7. inversion H14.
          destruct H7. destruct H7. destruct H14. inversion H14; subst.
          eexists; eexists; split; eauto. right. destruct τ; [right|left]; split; auto.
          intro. inversion H15.
          intro. inversion H15.
          eauto.
          destruct H7. contradiction.
          eexists; eexists; split; eauto.
          edestruct IHb2; eauto. destruct H5. destruct H5.
          assert (state_correct Γ x1).
          { edestruct preservation_aux_evaluates_seq.
            intros; eauto. eapply preservation_aux; eauto. apply H4. apply H8. apply H5.
            destruct H11. assumption. preservation_auto. }
          edestruct IHb3; eauto. destruct H12. destruct H12.
          destruct x.
          destruct H7. destruct H7. inversion H14.
          destruct H7. destruct H7. destruct H14. inversion H14; subst.
          eexists; eexists; split; eauto. right. destruct τ; [right|left]; split; auto.
          intro. inversion H15.
          intro. inversion H15.
          eauto.
          destruct H7. contradiction.
          eexists; eexists; split; eauto.
          eexists; eexists; split; eauto.
          right. destruct τ; [right|left]; split; auto.
          intro. inversion H3.
          intro. inversion H3.
          eauto.
      - inversion H1; subst.
        edestruct H; eauto. destruct H2.
        destruct x.
        edestruct preservation; eauto.
        destruct v; simpl in H3; inversion H3.
        destruct b.
        edestruct IHb1; eauto. destruct H6. destruct H6.
        assert (state_correct Γ x1).
        { edestruct preservation_aux_evaluates_seq.
          intros; eauto. eapply preservation_aux; eauto. apply H4. apply H5. apply H6.
          destruct H10. assumption. preservation_auto. }
        destruct x.
        eexists; eexists; split; eauto. right.
        destruct H9. destruct H9. inversion H11.
        destruct H9. destruct H9. destruct H11. inversion H11; subst.
        destruct τ; [right|left]; split; auto.
        intro. inversion H12.
        intro. inversion H12.
        eauto.
        destruct H9. contradiction.
        edestruct IHt. intros; eauto. apply H10. apply H1.
        destruct H11. destruct H11.
        eexists; eexists; split; eauto.
        edestruct IHb2; eauto. destruct H6. destruct H6.
        eexists; eexists; split; eauto.
        eexists; eexists; split; eauto. right.
        destruct τ; [right|left]; split; auto.
        intro. inversion H3.
        intro. inversion H3.
        eauto.
      - inversion H1; subst.
        edestruct H; eauto. destruct H2.
        eexists; eexists; split; eauto.
        right. right. split; intro; inversion H4.
    Qed.

    Lemma progress_seq_aux_Some : ∀ t b Γ s τ,
      (∀ m, m ≤ t → ∀ e Γ τ St0, state_correct Γ St0 → T[ Γ ⊢ e :→ τ] → ∃ v St, E[ St0 ⊢ e ⇓ v | St @ m]) →
      state_correct Γ s →
      T[ Γ ⊢ b :→→ (Some τ)] →
      ∃ r s', evaluates_seq s b (Some r) s' t.
    Proof.
      intros. edestruct progress_seq_aux; eauto.
      destruct H2. destruct H2.
      destruct H3. destruct H3. inversion H3.
      destruct H3. destruct H3. inversion H3.
      destruct H3. destruct x. eauto. contradiction.
    Qed.

    Lemma cbody_exists : ∀ C,
      ∃ b, cbody C = b.
    Proof.
      intros.
      destruct C. simpl. eauto.
      simpl. destruct j. eauto.
    Qed.

    Theorem progress : ∀ t e Γ τ St0,
                       state_correct Γ St0 →
                       T[ Γ ⊢ e :→ τ ] →
                       ∃ v St, E[ St0 ⊢ e ⇓ v | St @ t ].
    Proof.
      induction t as [ | t IHt ] using nat_ind_strong.
      - induction e using jExpr_ind2 with
        (Q := fun l : list jExpr => True);
        try (intros Γ τ St0 HS HT;
             inversion HT; subst; intros);
        intros;
        try (edestruct IHe; eauto; destruct H; destruct x; eauto; fail).
        + destruct HS. apply H in H0. destruct H0. destruct H0.
          exists (ResData x); eexists St0; constructor; auto.
        + edestruct IHe1; eauto. destruct H.
          destruct x.
          edestruct preservation_aux. apply HS. apply H2. apply H.
          destruct H0. destruct H0. inversion H0; subst.
          destruct H1. destruct H6. destruct H9. destruct H9.
          destruct H10. destruct H10. destruct H10. destruct H10. subst.
          edestruct IHe2; eauto. destruct H10. destruct x.
          edestruct preservation. apply H6. apply H7. apply H10.
          exists (ResData v). edestruct eval_heap_entry_remains; eauto.
          edestruct (set_field_some Γ x4 x1 x2 x c C f); eauto.
          eapply has_type_subtype; eauto.
          eauto.
          destruct x; simpl in H1; destruct H1.
          eauto.
          simpl in H9. contradiction.
          simpl in H9. contradiction.
          destruct H0. exfalso. eapply H0; reflexivity.
          eauto.
        + edestruct IHe; eauto; destruct H; destruct x.
          edestruct preservation_aux. apply HS. apply H2. apply H.
          preservation_auto; eauto.
          destruct x; simpl in H1; destruct H1.
          eauto.
          simpl in H9. contradiction.
          simpl in H9. contradiction.
          destruct H0. exfalso. eapply H0; eauto.
          eauto.
        + edestruct IHe; eauto; destruct H; destruct x.
          edestruct preservation_aux. apply HS. apply H1. apply H.
          preservation_auto; eauto.
          destruct H6. destruct H6. destruct H7.
          edestruct get_class_subclass; eauto.
          destruct H10. destruct H10. destruct H11.
          edestruct H7; eauto. eapply nth_error_In. apply H9.
          destruct H13. destruct H13.
          eexists; eexists; eapply E_Field; eauto. eapply nth_error_In; eauto.
          destruct x; simpl in H4; destruct H4.
          eauto.
          simpl in H7. contradiction.
          simpl in H7. contradiction.
          destruct H0. exfalso. eapply H0; eauto.
          eauto.
        + eauto.
        + edestruct IHe1; eauto; destruct H; destruct x; eauto.
          edestruct preservation_aux. apply HS. apply H3. apply H.
          preservation_auto.
          simpl in H1. inversion H1.
          destruct x; simpl in H1; inversion H1.
          edestruct IHe2; eauto; destruct H0; destruct x; eauto.
          edestruct preservation_aux. apply H2. apply H4. apply H0.
          preservation_auto.
          simpl in H7. inversion H7.
          destruct x; simpl in H7; inversion H7.
          eauto.
          destruct H6. exfalso. eapply H6; eauto.
          destruct H0. exfalso. eapply H0; eauto.
        + edestruct IHe; eauto; destruct H; destruct x; eauto.
          edestruct preservation_aux. apply HS. apply H0. apply H.
          preservation_auto.
          simpl in H2. inversion H2.
          destruct x; simpl in H2; inversion H2.
          eauto.
          destruct H1. exfalso. eapply H1; eauto.
        + edestruct IHe1; eauto; destruct H; destruct x; eauto.
          edestruct preservation_aux. apply HS. apply H3. apply H.
          preservation_auto.
          simpl in H1. inversion H1.
          destruct x; simpl in H1; inversion H1.
          edestruct IHe2; eauto; destruct H0; destruct x; eauto.
          edestruct preservation_aux. apply H2. apply H4. apply H0.
          preservation_auto.
          simpl in H7. inversion H7.
          destruct x; simpl in H7; inversion H7.
          eauto.
          destruct H6. exfalso. eapply H6; eauto.
          destruct H0. exfalso. eapply H0; eauto.
        + edestruct IHe1; eauto; destruct H; destruct x; eauto.
          edestruct preservation. apply HS. apply H3. apply H.
          edestruct IHe2; eauto; destruct H2; destruct x; eauto.
        + eauto.
        + edestruct IHe1; eauto; destruct H; destruct x; eauto.
          edestruct preservation_aux. apply HS. apply H3. apply H.
          preservation_auto.
          simpl in H1. inversion H1.
          destruct x; simpl in H1; inversion H1.
          edestruct IHe2; eauto; destruct H0; destruct x; eauto.
          edestruct preservation_aux. apply H2. apply H4. apply H0.
          preservation_auto.
          simpl in H7. inversion H7.
          destruct x; simpl in H7; inversion H7.
          destruct o; destruct b; destruct b0; eauto.
          destruct H6. exfalso. eapply H6; eauto.
          destruct o; destruct b; eauto.
          destruct H0. exfalso. eapply H0; eauto.
        + edestruct IHe; eauto; destruct H; destruct x; eauto.
          edestruct preservation_aux. apply HS. apply H0. apply H.
          preservation_auto.
          simpl in H2. inversion H2.
          destruct x; simpl in H2; inversion H2.
          eauto.
          destruct H1. exfalso. eapply H1; eauto.
        + eauto.
        + edestruct IHe; eauto; destruct H; destruct x; eauto.
          edestruct preservation_aux. apply HS. apply H1. apply H.
          preservation_auto.
          eexists; eexists; eapply E_Cast_S; eauto.
          simpl in H2. simpl. eapply subtype_trans; eauto.
          destruct (has_type_dec W (RefVal x1 x2) τ); eauto.
          destruct τ1; destruct x; simpl in H2; inversion H2.
          eexists; eexists; eapply E_Cast_S; eauto. destruct τ; simpl; auto;
          inversion H3.
          simpl in H5. contradiction.
          simpl in H5. contradiction.
          inversion H3; subst. eauto.
          inversion H3; subst. eauto.
          destruct τ1; destruct x; simpl in H2; inversion H2.
          eexists; eexists; eapply E_Cast_S; eauto. destruct τ; simpl; auto;
          inversion H3.
          simpl in H5. contradiction.
          simpl in H5. contradiction.
          inversion H3; subst. eauto.
          inversion H3; subst. eauto.
          destruct H0. exfalso. eapply H0; eauto.
        + edestruct IHe; eauto; destruct H; destruct x; eauto.
          edestruct preservation. apply HS. apply H2. apply H.
          destruct v; simpl in H0; inversion H0.
          eauto.
        + edestruct IHe; eauto; destruct H; destruct x; eauto.
          edestruct preservation. apply HS. apply H2. apply H.
          destruct v; simpl in H0; inversion H0.
          eauto.
        + auto.
        + auto.
      - induction e using jExpr_ind2 with
        (Q := fun l : list jExpr =>
          ∀ s vt Γ,
            state_correct Γ s →
            (∀ n a, nth_error l n = Some a → ∃ tt, nth_error vt n = Some tt ∧ T[ Γ ⊢ a :→ tt ]) →
            (∃ vl s', evaluates_args s l vl s' t) ∨ (∃ ex vl s', evaluates_args_exc s l ex vl s' t));
        try (intros Γ τ St0 HS HT;
             inversion HT; subst; intros);
        intros;
        try (edestruct IHe; eauto; destruct H; destruct x; eauto; fail).
        + destruct HS. apply H in H0. destruct H0. destruct H0.
          exists (ResData x); eexists St0; constructor; auto.
        + edestruct IHe1; eauto. destruct H.
          destruct x.
          edestruct preservation_aux. apply HS. apply H2. apply H.
          destruct H0. destruct H0. inversion H0; subst.
          destruct H1. destruct H6. destruct H9. destruct H9.
          destruct H10. destruct H10. destruct H10. destruct H10. subst.
          edestruct IHe2; eauto. destruct H10. destruct x.
          edestruct preservation. apply H6. apply H7. apply H10.
          exists (ResData v). edestruct eval_heap_entry_remains; eauto.
          edestruct (set_field_some Γ x4 x1 x2 x c C f); eauto.
          eapply has_type_subtype; eauto.
          eauto.
          destruct x; simpl in H1; destruct H1.
          eauto.
          simpl in H9. contradiction.
          simpl in H9. contradiction.
          destruct H0. exfalso. eapply H0; reflexivity.
          eauto.
        + edestruct IHe; eauto; destruct H; destruct x; eauto.
          edestruct preservation_aux. apply HS. apply H2. apply H.
          preservation_auto; eauto.
          edestruct IHe0; eauto. destruct H0. destruct H0.
          assert (∃ D, get_class W x1 = Some D).
          { simpl in H1. inversion H1; subst; eauto.
            eexists; eapply class_in_get_class_some_2; eauto. }
          destruct H9.
          edestruct Hmethod_has_type_in_subclass; eauto.
          destruct (mbody_exists s x5 x6); auto.
          erewrite <- Hmethod_override_equal_types in H10.
          2: apply H4. 2: apply H10. 2: apply H3. 2: apply H9. 2: simpl in H1. 2: apply H1.
          assert (map (λ x, match x with | Var t _ => t end) (fst x7) = fst (l0, τ)).
          { eapply mtype_mbody_eq_types; eauto. }
          assert (preservation_aux_l x l' Γ x4).
          { eapply preservation_l; eauto. }
          assert (state_correct Γ x4).
          { destruct x. inversion H0; subst. apply H6.
            destruct l'. inversion H14. simpl in H14.
            preservation_auto. }
          destruct x7. simpl in H13.
          edestruct eval_args_heap_entry_remains; eauto.
          assert (state_correct (∅,+* l1,+ "this" :→ Named x1) (map_params x4 (RefVal x1 x2) l1 x)).
          { eapply state_correct_map_params; eauto. simpl. constructor.
            erewrite <- evaluates_args_list_lengths; eauto.
            rewrite H5. erewrite all_subtype_eq_lengths; eauto.
            rewrite <- H13. rewrite map_length. reflexivity. }
          assert (T[ ∅,+* fst (l1, j),+ "this" :→ Named x1 ⊢ snd (l1, j) :→→ Some (snd (l0, τ))]).
          { apply get_class_eq_name in H9. destruct H9. subst. inversion H12.
            destruct H9. destruct H9. destruct H9. destruct H9.
            eapply Hmethod_body_correct_type; eauto.
            rewrite H9 in H10. apply H10. rewrite H9 in H12. apply H12. }
          simpl in H18.
          edestruct progress_seq_aux_Some; eauto. destruct H19.
          eauto.
          destruct H0. destruct H0. destruct H0. eauto.
          destruct x; simpl in H1; destruct H1.
          eauto.
          simpl in H9. contradiction.
          simpl in H9. contradiction.
          destruct H0. exfalso. eapply H0; eauto.
        + edestruct IHe; eauto.
          destruct H. destruct H.
          destruct (cbody_exists C).
          assert (map (λ x, match x with | Var t _ => t end) (fst x1) = ctype C).
          { eapply ctype_cbody_eq_types_sub; eauto. }
          assert (preservation_aux_l x l' Γ x0).
          { eapply preservation_l; eauto. }
          assert (state_correct Γ x0).
          { destruct x. inversion H; subst. apply HS.
            destruct l'. inversion H5. simpl in H5.
            preservation_auto. }
          destruct x1. simpl in H2.
          edestruct class_has_fields; eauto.
          assert (state_correct Γ (addObject x0 (mkObject W s x1))).
          { eapply state_correct_add_new_object; eauto. }
          assert (∃ d, nth_error (heap (addObject x0 (mkObject W s x1))) (length (heap x0)) = Some (ObjectDef s d)).
          { unfold addObject. simpl. rewrite nth_error_app2; auto.
            rewrite Nat.sub_diag. simpl. unfold mkObject. eauto. }
          destruct H10.
          assert (state_correct (∅,+* l0,+ "this" :→ Named s) (map_params (addObject x0 (mkObject W s x1)) (RefVal s (length (heap x0))) l0 x)).
          { eapply state_correct_map_params; eauto.
            eapply preservation_aux_l_add_new_object; eauto.
            simpl. constructor.
            erewrite <- evaluates_args_list_lengths; eauto.
            rewrite H3. erewrite all_subtype_eq_lengths; eauto.
            rewrite <- H2. rewrite map_length. reflexivity. }
          assert (T[ ∅,+* fst (l0, j),+ "this" :→ Named s ⊢ snd (l0, j) :→→ None]).
          { eapply Hconstructor_body_correct_type; eauto. constructor. }
          edestruct progress_seq_aux; eauto. destruct H13. destruct H13.
          destruct H14. destruct H14. subst. eauto.
          destruct H14. destruct H14. destruct H15. subst.
          eauto.
          destruct H14. contradiction.
          destruct H. destruct H. destruct H. eauto.
        + edestruct IHe; eauto.
          destruct H. destruct H.
          destruct (cbody_exists D).
          assert (map (λ x, match x with | Var t _ => t end) (fst x1) = ctype D).
          { eapply ctype_cbody_eq_types_sub; eauto. }
          assert (preservation_aux_l x l' Γ x0).
          { eapply preservation_l; eauto. }
          assert (state_correct Γ x0).
          { destruct x. inversion H; subst. apply HS.
            destruct l'. inversion H7. simpl in H7.
            preservation_auto. }
          destruct x1. simpl in H4.
          assert (∃ v : val, apply_state St0 "this" = Some v ∧ has_type v (Named s)).
          { destruct HS. eapply H10; eauto. }
          destruct H10. destruct H10.
          destruct x1.
          simpl in H11. inversion H11.
          simpl in H11. inversion H11.
          eauto.
          edestruct apply_state_nth_error; eauto.
          assert (∃ l, nth_error (heap St0) n = Some (ObjectDef s0 l)).
          { destruct HS. destruct H14. eapply H14; eauto. }
          destruct H13.
          edestruct eval_args_heap_entry_remains; eauto.
          assert (state_correct (∅,+* l0,+ "this" :→ Named s) (map_params x0 (RefVal s0 n) l0 x)).
          { eapply state_correct_map_params; eauto.
            erewrite <- evaluates_args_list_lengths; eauto.
            rewrite H5. erewrite all_subtype_eq_lengths; eauto.
            rewrite <- H4. rewrite map_length. reflexivity. }
          assert (T[ ∅,+* fst (l0, j),+ "this" :→ Named s ⊢ snd (l0, j) :→→ None]).
          { eapply Hconstructor_body_correct_type; eauto.
            apply get_class_In_W in H2.
            econstructor; eauto. constructor. }
          edestruct progress_seq_aux; eauto. destruct H17. destruct H17.
          destruct H18. destruct H18. subst. eauto.
          destruct H18. destruct H18. destruct H19. subst. eauto.
          destruct H18. contradiction.
          destruct H. destruct H. destruct H. eauto.
        + edestruct IHe; eauto; destruct H; destruct x.
          edestruct preservation_aux. apply HS. apply H1. apply H.
          preservation_auto; eauto.
          destruct H6. destruct H6. destruct H7.
          edestruct get_class_subclass; eauto.
          destruct H10. destruct H10. destruct H11.
          edestruct H7; eauto. eapply nth_error_In. apply H9.
          destruct H13. destruct H13.
          eexists; eexists; eapply E_Field; eauto. eapply nth_error_In; eauto.
          destruct x; simpl in H4; destruct H4.
          eauto.
          simpl in H7. contradiction.
          simpl in H7. contradiction.
          destruct H0. exfalso. eapply H0; eauto.
          eauto.
        + eauto.
        + edestruct IHe1; eauto; destruct H; destruct x; eauto.
          edestruct preservation_aux. apply HS. apply H3. apply H.
          preservation_auto.
          simpl in H1. inversion H1.
          destruct x; simpl in H1; inversion H1.
          edestruct IHe2; eauto; destruct H0; destruct x; eauto.
          edestruct preservation_aux. apply H2. apply H4. apply H0.
          preservation_auto.
          simpl in H7. inversion H7.
          destruct x; simpl in H7; inversion H7.
          eauto.
          destruct H6. exfalso. eapply H6; eauto.
          destruct H0. exfalso. eapply H0; eauto.
        + edestruct IHe; eauto; destruct H; destruct x; eauto.
          edestruct preservation_aux. apply HS. apply H0. apply H.
          preservation_auto.
          simpl in H2. inversion H2.
          destruct x; simpl in H2; inversion H2.
          eauto.
          destruct H1. exfalso. eapply H1; eauto.
        + edestruct IHe1; eauto; destruct H; destruct x; eauto.
          edestruct preservation_aux. apply HS. apply H3. apply H.
          preservation_auto.
          simpl in H1. inversion H1.
          destruct x; simpl in H1; inversion H1.
          edestruct IHe2; eauto; destruct H0; destruct x; eauto.
          edestruct preservation_aux. apply H2. apply H4. apply H0.
          preservation_auto.
          simpl in H7. inversion H7.
          destruct x; simpl in H7; inversion H7.
          eauto.
          destruct H6. exfalso. eapply H6; eauto.
          destruct H0. exfalso. eapply H0; eauto.
        + edestruct IHe1; eauto; destruct H; destruct x; eauto.
          edestruct preservation. apply HS. apply H3. apply H.
          edestruct IHe2; eauto; destruct H2; destruct x; eauto.
        + eauto.
        + edestruct IHe1; eauto; destruct H; destruct x; eauto.
          edestruct preservation_aux. apply HS. apply H3. apply H.
          preservation_auto.
          simpl in H1. inversion H1.
          destruct x; simpl in H1; inversion H1.
          edestruct IHe2; eauto; destruct H0; destruct x; eauto.
          edestruct preservation_aux. apply H2. apply H4. apply H0.
          preservation_auto.
          simpl in H7. inversion H7.
          destruct x; simpl in H7; inversion H7.
          destruct o; destruct b; destruct b0; eauto.
          destruct H6. exfalso. eapply H6; eauto.
          destruct o; destruct b; eauto.
          destruct H0. exfalso. eapply H0; eauto.
        + edestruct IHe; eauto; destruct H; destruct x; eauto.
          edestruct preservation_aux. apply HS. apply H0. apply H.
          preservation_auto.
          simpl in H2. inversion H2.
          destruct x; simpl in H2; inversion H2.
          eauto.
          destruct H1. exfalso. eapply H1; eauto.
        + eauto.
        + edestruct IHe; eauto; destruct H; destruct x; eauto.
          edestruct preservation_aux. apply HS. apply H1. apply H.
          preservation_auto.
          eexists; eexists; eapply E_Cast_S; eauto.
          simpl in H2. simpl. eapply subtype_trans; eauto.
          destruct (has_type_dec W (RefVal x1 x2) τ); eauto.
          destruct τ1; destruct x; simpl in H2; inversion H2.
          eexists; eexists; eapply E_Cast_S; eauto. destruct τ; simpl; auto;
          inversion H3.
          simpl in H5. contradiction.
          simpl in H5. contradiction.
          inversion H3; subst. eauto.
          inversion H3; subst. eauto.
          destruct τ1; destruct x; simpl in H2; inversion H2.
          eexists; eexists; eapply E_Cast_S; eauto. destruct τ; simpl; auto;
          inversion H3.
          simpl in H5. contradiction.
          simpl in H5. contradiction.
          inversion H3; subst. eauto.
          inversion H3; subst. eauto.
          destruct H0. exfalso. eapply H0; eauto.
        + edestruct IHe; eauto; destruct H; destruct x; eauto.
          edestruct preservation. apply HS. apply H2. apply H.
          destruct v; simpl in H0; inversion H0.
          eauto.
        + edestruct IHe; eauto; destruct H; destruct x; eauto.
          edestruct preservation. apply HS. apply H2. apply H.
          destruct v; simpl in H0; inversion H0.
          eauto.
        + left. eexists; eexists; constructor.
        + destruct (H0 0%nat e); auto.
          destruct H1.
          destruct vt. simpl in H1. inversion H1.
          simpl in H1. inversion H1; subst.
          edestruct IHt; eauto. destruct H3.
          destruct x0.
          edestruct preservation; eauto.
          edestruct (IHe0 x1 vt); eauto.
          intros. eapply (H0 (S n) a); eauto.
          destruct H6. destruct H6.
          left. eexists. eexists. econstructor; eauto.
          destruct H6. destruct H6. destruct H6.
          right. inversion H6; subst.
          assert (evaluates_args s (e :: l1) (v :: x2) s' t).
          { econstructor; eauto. }
          eexists. eexists. eexists. rewrite app_comm_cons. econstructor; eauto.
          right. eexists. eexists. eexists.
          replace (e :: l) with (nil ++ e :: l). econstructor; eauto. econstructor.
          simpl. reflexivity.
    Qed.

  End evaluation_proofs.

End evaluation.
