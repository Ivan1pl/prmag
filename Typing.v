Require Import String.
Require Import List.
Require Import Utf8.

Require Import Syntx.

Section typing.
  Variable W : world.

  Reserved Notation "'S[' C '<::' D ']'".

  Inductive subtype : jType → jType → Prop :=
  | SubtypeRefl  : ∀ t, S[ t <:: t ]
(*  | SubtypeTrans : ∀ C D E, S[ C <:: D ] → S[ D <:: E ] → S[ C <:: E ] *)
  | SubtypeDef   : ∀ C D E f k m,
                   In (Class C D f k m) W → S[ Named D <:: Named E] → S[ Named C <:: Named E ]
  where "'S[' C '<::' D ']'" := (subtype C D).

  Axiom subtype_dec : ∀ C D, { S[ C <:: D ] } + { ¬ S[ C <:: D ] }.

  Definition all_subtype (l1 : list jType) (l2 : list jType) : Prop :=
    length l1 = length l2 ∧
    (∀ n C D, nth_error l1 n = Some C → nth_error l2 n = Some D → S[ C <:: D ]).

  Notation "'S[' l1 '<::+' l2 ']'" := (all_subtype l1 l2).

  Definition empty_typing_env : string → option jType :=
    fun i => None.

  Definition extend_typing_env (Γ : string → option jType) (x : string) (τ : jType)
    : string → option jType :=
    fun i => if string_dec i x then Some τ else Γ i.

  Fixpoint extend_typing_env_multi (Γ : string → option jType) (xs : list jVar)
    : string → option jType :=
    match xs with
    | nil             => Γ
    | (Var t x) :: xs => extend_typing_env (extend_typing_env_multi Γ xs) x t
    end.

  Notation "∅" := (empty_typing_env).
  Notation "Γ ',+' x ':→' τ" := (extend_typing_env Γ x τ) (at level 45, left associativity).
  Notation "Γ ',+*' xs" := (extend_typing_env_multi Γ xs) (at level 40, left associativity).

  Reserved Notation "'T[' Γ '⊢' e ':→' τ ']'".

  Inductive expr_typing (Γ : string → option jType) : jExpr → jType → Prop :=
  | T_Var       : ∀ x τ,
                  Γ x = Some τ →
                  T[ Γ ⊢ Val x :→ τ ]
  | T_FieldAsgn : ∀ e1 e2 c C f x τ1 τ2,
                  T[ Γ ⊢ e1 :→ Named c ] →
                  get_class W c = Some C →
                  fields W C f →
                  In (Var τ1 x) f →
                  T[ Γ ⊢ e2 :→ τ2 ] →
                  S[ τ2 <:: τ1 ] →
                  T[ Γ ⊢ FieldAsgn e1 x e2 :→ τ2 ]
  | T_Asgn      : ∀ x e τ1 τ2,
                  Γ x = Some τ1 →
                  T[ Γ ⊢ e :→ τ2 ] →
                  S[ τ2 <:: τ1 ] →
                  T[ Γ ⊢ Asgn x e :→ τ2 ]
  | T_Invk      : ∀ e c C m l l' τ a,
                  T[ Γ ⊢ e :→ Named c ] →
                  get_class W c = Some C →
                  mtype W m C (l, τ) →
                  length a = length l' →
                  (∀ n e', nth_error a n = Some e' → ∃ τ', nth_error l' n = Some τ' ∧ T[ Γ ⊢ e' :→ τ' ]) →
                  S[ l' <::+ l ] →
                  T[ Γ ⊢ Call e m a :→ τ ]
  | T_New       : ∀ c C l l' a,
                  get_class W c = Some C →
                  ctype C = l →
                  length a = length l' →
                  (∀ n e', nth_error a n = Some e' → ∃ τ', nth_error l' n = Some τ' ∧ T[ Γ ⊢ e' :→ τ' ]) →
                  S[ l' <::+ l ] →
                  T[ Γ ⊢ New c a :→ Named c ]
  | T_Super     : ∀ c d f k m D l l' a,
                  Γ "this"%string = Some (Named c) →
                  get_class W c = Some (Class c d f k m) →
                  get_class W d = Some D →
                  ctype D = l →
                  length a = length l' →
                  (∀ n e', nth_error a n = Some e' → ∃ τ', nth_error l' n = Some τ' ∧ T[ Γ ⊢ e' :→ τ' ]) →
                  S[ l' <::+ l ] →
                  T[ Γ ⊢ Super c a :→ Named c ]
  | T_Field     : ∀ e c C f x τ,
                  T[ Γ ⊢ e :→ Named c ] →
                  get_class W c = Some C →
                  fields W C f →
                  In (Var τ x) f →
                  T[ Γ ⊢ Field e x :→ τ ]
  | T_Num       : ∀ n,
                  T[ Γ ⊢ Num n :→ Int ]
  | T_Aop       : ∀ e1 e2 o,
                  T[ Γ ⊢ e1 :→ Int ] →
                  T[ Γ ⊢ e2 :→ Int ] →
                  T[ Γ ⊢ Aop e1 e2 o :→ Int ]
  | T_UMinus    : ∀ e,
                  T[ Γ ⊢ e :→ Int ] →
                  T[ Γ ⊢ UnaryMinus e :→ Int ]
  | T_Cop       : ∀ e1 e2 o,
                  T[ Γ ⊢ e1 :→ Int ] →
                  T[ Γ ⊢ e2 :→ Int ] →
                  T[ Γ ⊢ Cop e1 e2 o :→ Boolean ]
  | T_EqOp      : ∀ e1 e2 τ1 τ2 o,
                  T[ Γ ⊢ e1 :→ τ1 ] →
                  T[ Γ ⊢ e2 :→ τ2 ] →
                  T[ Γ ⊢ EqOp e1 e2 o :→ Boolean ]
  | T_BConst    : ∀ b,
                  T[ Γ ⊢ BConst b :→ Boolean ]
  | T_Bop       : ∀ e1 e2 o,
                  T[ Γ ⊢ e1 :→ Boolean ] →
                  T[ Γ ⊢ e2 :→ Boolean ] →
                  T[ Γ ⊢ Bop e1 e2 o :→ Boolean ]
  | T_Not       : ∀ e,
                  T[ Γ ⊢ e :→ Boolean ] →
                  T[ Γ ⊢ Not e :→ Boolean ]
  | T_Null      : ∀ c,
                  T[ Γ ⊢ Null :→ Named c ]
  | T_Cast_Sub  : ∀ e τ1 τ2,
                  T[ Γ ⊢ e :→ τ1 ] →
                  (S[ τ1 <:: τ2 ] ∨ S[ τ2 <:: τ1 ]) →
                  T[ Γ ⊢ Cast τ2 e :→ τ2 ]
  | T_Cast_Int  : ∀ e,
                  T[ Γ ⊢ e :→ Int ] →
                  T[ Γ ⊢ Cast Boolean e :→ Boolean ]
  | T_Cast_Bool : ∀ e,
                  T[ Γ ⊢ e :→ Boolean ] →
                  T[ Γ ⊢ Cast Int e :→ Int ]
  where "'T[' Γ '⊢' e ':→' τ ']'" := (expr_typing Γ e τ).

  Reserved Notation "'T[' Γ '⊢' s ':→→' τ ']'".

  Inductive seq_typing (Γ : string → option jType) : jSeq → option jType → Prop :=
  | T_EndSeq      : T[ Γ ⊢ EndSeq :→→ None ]
  | T_Declaration : ∀ x t s τ,
                    Γ x = None →
                    T[ Γ ,+ x :→ t ⊢ s :→→ τ ] →
                    T[ Γ ⊢ Declaration (Var t x) s :→→ τ ]
  | T_Expression  : ∀ e s τ τ',
                    T[ Γ ⊢ s :→→ τ ] →
                    T[ Γ ⊢ e :→ τ' ] →
                    T[ Γ ⊢ Expression e s :→→ τ ]
  | T_If_End1     : ∀ e s1 s2 τ1 τ2,
                    T[ Γ ⊢ s1 :→→ Some τ1 ] →
                    T[ Γ ⊢ s2 :→→ Some τ2 ] →
                    T[ Γ ⊢ e :→ Boolean ] →
                    S[ τ1 <:: τ2 ] →
                    T[ Γ ⊢ If e s1 s2 EndSeq :→→ Some τ2 ]
  | T_If_End2     : ∀ e s1 s2 τ1 τ2,
                    T[ Γ ⊢ s1 :→→ Some τ1 ] →
                    T[ Γ ⊢ s2 :→→ Some τ2 ] →
                    T[ Γ ⊢ e :→ Boolean ] →
                    S[ τ2 <:: τ1 ] →
                    T[ Γ ⊢ If e s1 s2 EndSeq :→→ Some τ1 ]
  | T_If_Cont     : ∀ e s1 s2 s τ,
                    T[ Γ ⊢ s1 :→→ None ] →
                    T[ Γ ⊢ s2 :→→ None ] →
                    T[ Γ ⊢ e :→ Boolean ] →
                    T[ Γ ⊢ s :→→ τ ] →
                    T[ Γ ⊢ If e s1 s2 s :→→ τ ]
  | T_While       : ∀ e s1 s τ,
                    T[ Γ ⊢ s1 :→→ None ] →
                    T[ Γ ⊢ e :→ Boolean ] →
                    T[ Γ ⊢ s :→→ τ ] →
                    T[ Γ ⊢ While e s1 s :→→ τ ]
  | T_Return      : ∀ e τ,
                    T[ Γ ⊢ e :→ τ ] →
                    T[ Γ ⊢ Return e :→→ Some τ ]
  where "'T[' Γ '⊢' s ':→→' τ ']'" := (seq_typing Γ s τ).
End typing.
