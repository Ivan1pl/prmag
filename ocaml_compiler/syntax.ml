type ident = { id : string }

let ident s = { id = s }

type jType =
  | Named of ident
  | Int
  | Boolean

type jVar =
  | Var of jType * ident

type jExpr =
  | This
  | Asgn of jExpr * jExpr
  | Val of ident
  | Call of jExpr * ident * jExpr list
  | Super of jExpr list
  | Field of jExpr * ident
  | New of ident * jExpr list
  | Num of int
  | Plus of jExpr * jExpr
  | Minus of jExpr * jExpr
  | Times of jExpr * jExpr
  | Div of jExpr * jExpr
  | Mod of jExpr * jExpr
  | UnaryMinus of jExpr
  | Lt of jExpr * jExpr
  | Leq of jExpr * jExpr
  | Gt of jExpr * jExpr
  | Geq of jExpr * jExpr
  | Eq of jExpr * jExpr
  | Neq of jExpr * jExpr
  | JTrue
  | JFalse
  | And of jExpr * jExpr
  | Or of jExpr * jExpr
  | Not of jExpr
  | Null
  | Cast of jType * jExpr

type jSeq =
  | EndSeq
  | Declaration of jVar * jSeq
  | Expression of jExpr * jSeq
  | If of jExpr * jSeq * jSeq * jSeq
  | While of jExpr * jSeq * jSeq
  | Return of jExpr

type jConstructor =
  | Constructor of ident * jVar list * jSeq

type jMethod =
  | Method of jType * ident * jVar list * jSeq

type jClassBody =
  | EmptyBody
  | ClassField of jVar * jClassBody
  | ClassConstructor of jConstructor * jClassBody
  | ClassMethod of jMethod * jClassBody

type jClass =
  | Class of ident * ident * jClassBody

let get_name (Class (n, _, _)) = n.id

let val_to_type v =
  match v with
  | Val n -> if n.id = "int" then Int else if n.id = "boolean" then Boolean else Named n
  | _     -> failwith "Type name expected in cast"

