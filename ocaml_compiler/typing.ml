open Syntax

type ident = Syntax.ident

type jType = Syntax.jType

type jVar = Syntax.jVar

type exprType = jType

type jExpr =
  | TThis of exprType
  | TAsgn of jExpr * jExpr * exprType
  | TVal of ident * exprType
  | TCall of jExpr * ident * jExpr list * exprType
  | TSuper of jExpr list
  | TField of jExpr * ident * exprType
  | TNew of ident * jExpr list * exprType
  | TNum of int * exprType
  | TPlus of jExpr * jExpr * exprType
  | TMinus of jExpr * jExpr * exprType
  | TTimes of jExpr * jExpr * exprType
  | TDiv of jExpr * jExpr * exprType
  | TMod of jExpr * jExpr * exprType
  | TUnaryMinus of jExpr * exprType
  | TLt of jExpr * jExpr * exprType
  | TLeq of jExpr * jExpr * exprType
  | TGt of jExpr * jExpr * exprType
  | TGeq of jExpr * jExpr * exprType
  | TEq of jExpr * jExpr * exprType
  | TNeq of jExpr * jExpr * exprType
  | TJTrue of exprType
  | TJFalse of exprType
  | TAnd of jExpr * jExpr * exprType
  | TOr of jExpr * jExpr * exprType
  | TNot of jExpr * exprType
  | TNull
  | TCast of jType * jExpr

type jSeq =
  | TEndSeq
  | TDeclaration of jVar * jSeq
  | TExpression of jExpr * jSeq
  | TIf of jExpr * jSeq * jSeq * jSeq
  | TWhile of jExpr * jSeq * jSeq
  | TReturn of jExpr

type jConstructor =
  | TConstructor of jVar list * jSeq

type jMethod =
  | TMethod of jType * ident * jVar list * jSeq

type jClassBody =
  | TEmptyBody
  | TClassField of jVar * jClassBody
  | TClassConstructor of jConstructor * jClassBody
  | TClassMethod of jMethod * jClassBody

type jClass =
  | TClass of ident * ident * jClassBody

let rec get_return_type_internal m body =
  match body with
  | EmptyBody                               -> None
  | ClassField (_, body)
  | ClassConstructor (_, body)              -> get_return_type_internal m body
  | ClassMethod (Method (t, i, _, _), body) -> if i.id = m.id then Some t else get_return_type_internal m body

let rec get_return_type m tid classdefs =
  match classdefs with
  | []                        -> failwith ("Cannot resolve type: " ^ tid.id)
  | Class (i1, _, body) :: xs -> if i1.id = tid.id then get_return_type_internal m body else get_return_type m tid xs

let rec get_supertype t classdefs =
  match classdefs with
  | []                         -> failwith ("Cannot resolve type: " ^ t.id)
  | Class (i1, i2, body) :: xs -> if i1.id = t.id then Named i2 else get_supertype t xs

(* get return type of method *)
let rec get_method_return_type m t classdefs =
  match t with
  | Named n -> (match get_return_type m n classdefs with
               | Some t -> t
               | None   -> get_method_return_type m (get_supertype n classdefs) classdefs)
  | _       -> failwith "Attempt to call method on primitive type value"

let rec get_field_type_internal f body =
  match body with
  | EmptyBody                     -> None
  | ClassField (Var (t, i), body) -> if i.id = f.id then Some t else get_field_type_internal f body
  | ClassConstructor (_, body)
  | ClassMethod (_, body)         -> get_field_type_internal f body

let rec get_f_type f tid classdefs =
  match classdefs with
  | []                        -> failwith ("Cannot resolve type: " ^ tid.id)
  | Class (i1, _, body) :: xs -> if i1.id = tid.id then get_field_type_internal f body else get_f_type f tid xs

let rec get_field_type f t classdefs =
  match t with
  | Named n -> (match get_f_type f n classdefs with
               | Some t -> t
               | None   -> get_field_type f (get_supertype n classdefs) classdefs)
  | _       -> failwith "Attempt to access field of primitive type value"

(* get type of variable *)
let rec get_type i vars =
  match vars with
  | []    -> failwith ("Undeclared variable: " ^ i.id)
  | x::xs -> match x with
             | Var (t, n) -> if n.id = i.id then t else get_type i xs


(* extract type from expression *)
let extract_type e =
  match e with
  | TThis t
  | TAsgn (_, _, t)
  | TVal (_, t)
  | TCall (_, _, _, t)
  | TField (_, _, t)
  | TNew (_, _, t)
  | TPlus (_, _, t)
  | TMinus (_, _, t)
  | TTimes (_, _, t)
  | TDiv (_, _, t)
  | TMod (_, _, t)
  | TUnaryMinus (_, t)
  | TLt (_, _, t)
  | TLeq (_, _, t)
  | TGt (_, _, t)
  | TGeq (_, _, t)
  | TEq (_, _, t)
  | TNeq (_, _, t)
  | TJTrue t
  | TJFalse t
  | TAnd (_, _, t)
  | TOr (_, _, t)
  | TNot (_, t)
  | TNum (_, t)
  | TCast (t, _)       -> t
  | TSuper _           -> failwith "Attempted operation on super"
  | TNull              -> failwith "Attempted operation on null"

(* assign type to expression *)
let rec assign_types_expr e vars classdefs currtype =
  match e with
  | This              -> TThis currtype
  | Asgn (e1, e2)     -> let (t1, t2) = (assign_types_expr e1 vars classdefs currtype, assign_types_expr e2 vars classdefs currtype) in TAsgn (t1, t2, extract_type t1)
  | Val i             -> TVal (i, get_type i vars)
  | Call (e, i, args) -> let t = assign_types_expr e vars classdefs currtype in TCall (t, i, List.map (fun e -> assign_types_expr e vars classdefs currtype) args, get_method_return_type i (extract_type t) classdefs)
  | Super args        -> TSuper (List.map (fun e -> assign_types_expr e vars classdefs currtype) args)
  | Field (e, i)      -> let t = assign_types_expr e vars classdefs currtype in TField (t, i, get_field_type i (extract_type t) classdefs)
  | New (i, args)     -> TNew (i, List.map (fun e -> assign_types_expr e vars classdefs currtype) args, Named i)
  | Plus (e1, e2)     -> let (t1, t2) = (assign_types_expr e1 vars classdefs currtype, assign_types_expr e2 vars classdefs currtype)
                         in (if extract_type t1 <> Int || extract_type t2 <> Int then failwith "+: both operands should be int" else TPlus (t1, t2, Int))
  | Minus (e1, e2)    -> let (t1, t2) = (assign_types_expr e1 vars classdefs currtype, assign_types_expr e2 vars classdefs currtype)
                         in (if extract_type t1 <> Int || extract_type t2 <> Int then failwith "-: both operands should be int" else TMinus (t1, t2, Int))
  | Times (e1, e2)    -> let (t1, t2) = (assign_types_expr e1 vars classdefs currtype, assign_types_expr e2 vars classdefs currtype)
                         in (if extract_type t1 <> Int || extract_type t2 <> Int then failwith "*: both operands should be int" else TTimes (t1, t2, Int))
  | Div (e1, e2)      -> let (t1, t2) = (assign_types_expr e1 vars classdefs currtype, assign_types_expr e2 vars classdefs currtype)
                         in (if extract_type t1 <> Int || extract_type t2 <> Int then failwith "/: both operands should be int" else TDiv (t1, t2, Int))
  | Mod (e1, e2)      -> let (t1, t2) = (assign_types_expr e1 vars classdefs currtype, assign_types_expr e2 vars classdefs currtype)
                         in (if extract_type t1 <> Int || extract_type t2 <> Int then failwith "%: both operands should be int" else TMod (t1, t2, Int))
  | UnaryMinus e      -> let t = assign_types_expr e vars classdefs currtype in (if extract_type t <> Int then failwith "-: expected value of type int" else TUnaryMinus (t, Int))
  | Lt (e1, e2)       -> let (t1, t2) = (assign_types_expr e1 vars classdefs currtype, assign_types_expr e2 vars classdefs currtype)
                         in (if extract_type t1 <> Int || extract_type t2 <> Int then failwith "<: both operands should be int" else TLt (t1, t2, Boolean))
  | Leq (e1, e2)      -> let (t1, t2) = (assign_types_expr e1 vars classdefs currtype, assign_types_expr e2 vars classdefs currtype)
                         in (if extract_type t1 <> Int || extract_type t2 <> Int then failwith "<=: both operands should be int" else TLeq (t1, t2, Boolean))
  | Gt (e1, e2)       -> let (t1, t2) = (assign_types_expr e1 vars classdefs currtype, assign_types_expr e2 vars classdefs currtype)
                         in (if extract_type t1 <> Int || extract_type t2 <> Int then failwith ">: both operands should be int" else TGt (t1, t2, Boolean))
  | Geq (e1, e2)      -> let (t1, t2) = (assign_types_expr e1 vars classdefs currtype, assign_types_expr e2 vars classdefs currtype)
                         in (if extract_type t1 <> Int || extract_type t2 <> Int then failwith ">=: both operands should be int" else TGeq (t1, t2, Boolean))
  | Eq (e1, e2)       -> TEq (assign_types_expr e1 vars classdefs currtype, assign_types_expr e2 vars classdefs currtype, Boolean)
  | Neq (e1, e2)      -> TNeq (assign_types_expr e1 vars classdefs currtype, assign_types_expr e2 vars classdefs currtype, Boolean)
  | JTrue             -> TJTrue Boolean
  | JFalse            -> TJFalse Boolean
  | And (e1, e2)      -> let (t1, t2) = (assign_types_expr e1 vars classdefs currtype, assign_types_expr e2 vars classdefs currtype)
                         in (if extract_type t1 <> Boolean || extract_type t2 <> Boolean then failwith "&&: both operands should be boolean" else TAnd (t1, t2, Boolean))
  | Or (e1, e2)       -> let (t1, t2) = (assign_types_expr e1 vars classdefs currtype, assign_types_expr e2 vars classdefs currtype)
                         in (if extract_type t1 <> Boolean || extract_type t2 <> Boolean then failwith "||: both operands should be int" else TOr (t1, t2, Boolean))
  | Not e             -> let t = assign_types_expr e vars classdefs currtype in (if extract_type t <> Boolean then failwith "!: expected boolean expression" else TNot (t, Boolean))
  | Num n             -> TNum (n, Int)
  | Null              -> TNull
  | Cast (t, e)       -> TCast (t, assign_types_expr e vars classdefs currtype)

let rec assign_types_seq seq vars classdefs currtype =
  match seq with
  | EndSeq               -> TEndSeq
  | Declaration (v, seq) -> TDeclaration (v, assign_types_seq seq (v::vars) classdefs currtype)
  | Expression (e, seq)  -> TExpression (assign_types_expr e vars classdefs currtype, assign_types_seq seq vars classdefs currtype)
  | If (e, s1, s2, s3)   -> TIf (assign_types_expr e vars classdefs currtype, assign_types_seq s1 vars classdefs currtype, assign_types_seq s2 vars classdefs currtype, assign_types_seq s3 vars classdefs currtype)
  | While (e, s1, s2)    -> TWhile (assign_types_expr e vars classdefs currtype, assign_types_seq s1 vars classdefs currtype, assign_types_seq s2 vars classdefs currtype)
  | Return e             -> TReturn (assign_types_expr e vars classdefs currtype)

let assign_types_constructor c classdefs currtype =
  match c with
  | Constructor (i, args, seq) -> if Named i = currtype then TConstructor (args, assign_types_seq seq args classdefs currtype) else failwith ("Return type not specified for method: " ^ i.Syntax.id)

let assign_types_method m classdefs currtype =
  match m with
  | Method (t, i, args, seq) -> TMethod (t, i, args, assign_types_seq seq args classdefs currtype)

let rec assign_types_body body classdefs currtype =
  match body with
  | EmptyBody                  -> TEmptyBody
  | ClassField (v, body)       -> TClassField (v, assign_types_body body classdefs currtype)
  | ClassConstructor (c, body) -> TClassConstructor (assign_types_constructor c classdefs currtype, assign_types_body body classdefs currtype)
  | ClassMethod (m, body)      -> TClassMethod (assign_types_method m classdefs currtype, assign_types_body body classdefs currtype)

let assign_types c classdefs =
  match c with
  | Class (i1, i2, body) -> TClass (i1, i2, assign_types_body body classdefs (Named i1))

