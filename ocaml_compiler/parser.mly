%{
open List
open Syntax
%}

%token <Syntax.ident> IDENTIFIER
%token <int> INT_LITERAL

/* Separators */

%token LP		/* ( */
%token RP		/* ) */
%token LC		/* { */
%token RC		/* } */
%token SM		/* ; */
%token CM		/* , */
%token DOT		/* . */

/* Operators */

%token EQ		/* = */
%token GT		/* > */
%token LT		/* < */
%token NOT		/* ! */
%token EQ_EQ		/* == */
%token LE		/* <= */
%token GE		/* >= */
%token NOT_EQ		/* != */
%token AND_AND		/* && */
%token OR_OR		/* || */
%token PLUS		/* + */
%token MINUS		/* - */
%token TIMES		/* * */
%token DIV		/* / */
%token MOD		/* % */

%token BOOLEAN CLASS ELSE EXTENDS FALSE IF INT NEW NULL RETURN
%token SUPER THIS TRUE WHILE

%token EOF

%start goal
%type <Syntax.jClass> goal

%%

goal:
	CLASS Identifier EXTENDS Identifier LC ClassBody RC EOF { Class ($2, $4, $6) }

Identifier:
	IDENTIFIER  { $1 }
;

ClassBody:
	/* empty */ { EmptyBody }
|	Var ClassBody { ClassField ($1, $2) }
|	Constructor ClassBody { ClassConstructor ($1, $2) }
|	Method ClassBody { ClassMethod ($1, $2) }
;

Var:
	VarDecl SM { $1 }
;

VarDecl:
	TypeIdentifier Identifier { Var ($1, $2) }
;

TypeIdentifier:
	Int { $1 }
|	Boolean { $1 }
|	Identifier { Named $1 }
;

Int:
	INT { Int }
;

Boolean:
	BOOLEAN { Boolean }
;

Constructor:
	Identifier LP Args RP LC ConstructorCommands RC { Constructor ($1, $3, $6) }
;

Method:
	TypeIdentifier Identifier LP Args RP LC Commands RC { Method ($1, $2, $4, $7) }

Args:
	/* empty */ { [] }
|	NonEmptyArgs { $1 }
;

NonEmptyArgs:
	VarDecl { [$1] }
|	VarDecl CM NonEmptyArgs { $1 :: $3 }
;

ConstructorCommands:
	SUPER LP ArgVals RP SM Commands { Expression (Super $3, $6) }
;

Commands:
	/* empty */ { EndSeq }
|	Var Commands { Declaration ($1, $2) }
|	Expr SM Commands { Expression ($1, $3) }
|	IF LP Expr RP LC Commands RC ELSE LC Commands RC Commands { If ($3, $6, $10, $12) }
|	WHILE LP Expr RP LC Commands RC Commands { While ($3, $6, $8) }
|	RETURN Expr SM { Return $2 }
;

Expr:
	AssignmentExpr { $1 }
;

Primary:
	THIS { This }
|	Identifier { Val $1 }
|	LP Expr RP { $2 }
|	NEW Identifier LP ArgVals RP { New ($2, $4) }
|	FieldAccess { $1 }
|	MethodInvocation { $1 }
|	IntLiteral { Num $1 }
|	TRUE { JTrue }
|	FALSE { JFalse }
|   NULL { Null }
;

FieldAccess:
	Primary DOT Identifier { Field ($1, $3) }
;

MethodInvocation:
	Primary DOT Identifier LP ArgVals RP { Call ($1, $3, $5) }
;

PostfixExpr:
	Primary { $1 }
;

UnaryExpr:
	MINUS PostfixExpr { UnaryMinus $2 }
|	UnaryExprNotPlusMinus { $1 }
;

UnaryExprNotPlusMinus:
	PostfixExpr { $1 }
|	NOT PostfixExpr { Not $2 }
|	CastExpr { $1 }
;

CastExpr:
	LP Expr RP UnaryExprNotPlusMinus { Cast (val_to_type $2, $4) }
;

MultiplicativeExpr:
	UnaryExpr { $1 }
|	MultiplicativeExpr TIMES UnaryExpr { Times ($1, $3) }
|	MultiplicativeExpr DIV UnaryExpr { Div ($1, $3) }
|	MultiplicativeExpr MOD UnaryExpr { Mod ($1, $3) }
;

AdditiveExpr:
	MultiplicativeExpr { $1 }
|	AdditiveExpr PLUS MultiplicativeExpr { Plus ($1, $3) }
|	AdditiveExpr MINUS MultiplicativeExpr { Minus ($1, $3) }
;

RelationalExpr:
	AdditiveExpr { $1 }
|	RelationalExpr LT AdditiveExpr { Lt ($1, $3) }
|	RelationalExpr GT AdditiveExpr { Gt ($1, $3) }
|	RelationalExpr LE AdditiveExpr { Leq ($1, $3) }
|	RelationalExpr GE AdditiveExpr { Geq ($1, $3) }
;

EqualityExpr:
	RelationalExpr { $1 }
|	EqualityExpr EQ_EQ RelationalExpr { Eq ($1, $3) }
|	EqualityExpr NOT_EQ RelationalExpr { Neq ($1, $3) }
;

ConditionalAndExpr:
	EqualityExpr { $1 }
|	ConditionalAndExpr AND_AND EqualityExpr { And ($1, $3) }
;

ConditionalOrExpr:
	ConditionalAndExpr { $1 }
|	ConditionalOrExpr OR_OR ConditionalAndExpr { Or ($1, $3) }
;

Assignment:
	LeftHandSide EQ AssignmentExpr { Asgn ($1, $3) }
;

LeftHandSide:
	Identifier  { Val $1 }
|	FieldAccess  { $1 }
;

AssignmentExpr:
	ConditionalOrExpr { $1 }
|	Assignment { $1 }
;

ArgVals:
	/* empty */ { [] }
|	NonEmptyArgVals { $1 }
;

NonEmptyArgVals:
	Expr { [$1] }
|	Expr CM NonEmptyArgVals { $1 :: $3 }
;

IntLiteral:
	INT_LITERAL { $1 }
;
