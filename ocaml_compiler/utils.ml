let object_constructor_def = Syntax.Constructor (Syntax.ident "Object", [], Syntax.EndSeq)

let rec get_constructor_def_body body =
  match body with
  | Syntax.EmptyBody               -> failwith "Constructor not found"
  | Syntax.ClassField (_, body)
  | Syntax.ClassMethod (_, body)   -> get_constructor_def_body body
  | Syntax.ClassConstructor (c, _) -> c

let rec get_constructor_def classdefs t =
  match classdefs with
  | []                              -> if t.Syntax.id = "Object" then object_constructor_def else failwith ("Class " ^ t.Syntax.id ^ " not found")
  | Syntax.Class (i, _, body) :: xs -> if i = t then get_constructor_def_body body else get_constructor_def xs t

let rec get_method_def_body body name =
  match body with
  | Syntax.EmptyBody                  -> None
  | Syntax.ClassField (_, body)
  | Syntax.ClassConstructor (_, body) -> get_method_def_body body name
  | Syntax.ClassMethod (m, body)      -> match m with
                                         | Syntax.Method (_, n, _, _) -> if n = name then Some m else get_method_def_body body name

let rec get_method_def classdefs t name =
  let rec method_def cdefs t =
    match cdefs with
    | []                               -> None
    | Syntax.Class (i, i2, body) :: xs -> if i = t
                                            then (match get_method_def_body body name with
                                                  | Some def -> Some def
                                                  | None     -> method_def classdefs i2)
                                            else method_def xs t
  in
    match method_def classdefs t with
    | Some def -> def
    | None     -> failwith ("Method " ^ name.Syntax.id ^ " not found in class " ^ t.Syntax.id)

let rec get_field_def_body body name =
  match body with
  | Syntax.EmptyBody                  -> None
  | Syntax.ClassMethod (_, body)
  | Syntax.ClassConstructor (_, body) -> get_field_def_body body name
  | Syntax.ClassField (f, body)       -> match f with
                                         | Syntax.Var (_, n) -> if n = name then Some f else get_field_def_body body name

let get_field_def classdefs t name =
  let rec field_def cdefs t =
    match cdefs with
    | []                               -> None
    | Syntax.Class (i, i2, body) :: xs -> if i = t
                                            then (match get_field_def_body body name with
                                                  | Some def -> Some def
                                                  | None     -> field_def classdefs i2)
                                            else field_def xs t
  in
    match field_def classdefs t with
    | Some def -> def
    | None     -> failwith ("Field " ^ name.Syntax.id ^ " not found in class " ^ t.Syntax.id)

let rec uniq l =
  match l with
  | []      -> []
  | x :: xs -> x :: List.filter (fun e -> e <> x) (uniq xs)

let index l elem =
  let rec index_aux l aux =
    match l with
    | x :: xs -> if x = elem then aux else index_aux xs (aux+1)
    | []      -> failwith "Not found"
  in index_aux l 0

let first_index p l =
  let rec first_index_aux l aux =
    match l with
    | x :: xs -> if p x then aux else first_index_aux xs (aux+1)
    | []      -> failwith "Not found"
  in first_index_aux l 0

let list_max l =
  let rec list_max_aux l currmax =
    match (l, currmax) with
    | ([], _)           -> currmax
    | (x :: xs, None)   -> list_max_aux xs (Some x)
    | (x :: xs, Some y) -> list_max_aux xs (if x > y then Some x else currmax)
  in match list_max_aux l None with
  | Some x -> x
  | None   -> failwith "Max of 0 elements"

