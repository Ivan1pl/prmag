open Utils

type cp_info =
  | Class of int                           (* CONSTANT_Class_info (CONSTANT_Class, name_index) *)
  | Fieldref of (int * int)                (* CONSTANT_Fieldref_info (CONSTANT_Fieldref, class_index, name_and_type_field_descriptor) *)
  | Methodref of (int * int)               (* CONSTANT_Methodref_info (CONSTANT_Methodref, class_index, name_and_type_method_descriptor) *)
  | Integer of int                         (* CONSTANT_Integer_info (CONSTANT_Integer, value) *)
  | NameAndType of (int * int)             (* CONSTANT_NameAndType_info (CONSTANT_NameAndType, name_index, descriptor_index) *)
  | Utf8 of (int * bytes)                  (* CONSTANT_Utf8_info (CONSTANT_Utf8, lenght, bytes) *)

type field_descriptor = string

type method_descriptor = string

type cp_info_step1 =
  | S1_Fieldref of (string * string * field_descriptor)
  | S1_Methodref of (string * string * method_descriptor)
  | S1_Integer of int
  | S1_Cast of string

type cp_info_step2 =
  | S2_Class of string
  | S2_Fieldref of (int * string * field_descriptor)
  | S2_Methodref of (int * string * method_descriptor)
  | S2_Integer of int

type cp_info_step3 =
  | S3_Class of string
  | S3_Fieldref of (int * int)
  | S3_Methodref of (int * int)
  | S3_Integer of int
  | S3_NameAndType of (string * string)

let make_field_descriptor t =
  match t with
  | Syntax.Named n -> "L" ^ n.Syntax.id ^ ";"
  | Syntax.Int     -> "I"
  | Syntax.Boolean -> "Z"

let make_constructor_descriptor args =
  "(" ^ (List.fold_right (fun (Syntax.Var (t, _)) -> fun s -> make_field_descriptor t ^ s) args "") ^ ")V"

let make_method_descriptor args rettype =
  "(" ^ (List.fold_right (fun (Syntax.Var (t, _)) -> fun s -> make_field_descriptor t ^ s) args "") ^ ")" ^ make_field_descriptor rettype

let build_cpinfo_step1_var v currtype =
  match v with
  | Syntax.Var (t, i) -> [S1_Fieldref (currtype, i.Syntax.id, make_field_descriptor t)]

let build_cpinfo_step1_constructor c currtype =
  match c with
  | Syntax.Constructor (_, args, _) -> [S1_Methodref (currtype, "<init>", make_constructor_descriptor args)]

let build_cpinfo_step1_method m currtype =
  match m with
  | Syntax.Method (rettype, name, args, _) -> [S1_Methodref (currtype, name.Syntax.id, make_method_descriptor args rettype)]

let build_cpinfo_step1_tconstructor c currtype =
  match c with
  | Typing.TConstructor (args, _) -> [S1_Methodref (currtype, "<init>", make_constructor_descriptor args)]

let build_cpinfo_step1_tmethod m currtype =
  match m with
  | Typing.TMethod (rettype, name, args, _) -> [S1_Methodref (currtype, name.Syntax.id, make_method_descriptor args rettype)]

let build_cpinfo_step1_expr_call owner name classdefs =
  match owner with
  | Syntax.Int
  | Syntax.Boolean -> failwith "Calling method on primitive type value"
  | Syntax.Named n -> let m = get_method_def classdefs n name
                      in build_cpinfo_step1_method m n.Syntax.id

let build_cpinfo_step1_expr_field owner name classdefs =
  match owner with
  | Syntax.Int
  | Syntax.Boolean -> failwith "Accessing field of primitive type value"
  | Syntax.Named n -> let f = get_field_def classdefs n name
                      in build_cpinfo_step1_var f n.Syntax.id

let build_cpinfo_step1_cast t =
  match t with
  | Syntax.Named n -> [S1_Cast n.Syntax.id]
  | _              -> failwith "Casts to primitive types are not supported"

let rec build_cpinfo_step1_expr e classdefs supertype =
  match e with
  | Typing.TThis _
  | Typing.TVal (_, _)
  | Typing.TJTrue _
  | Typing.TJFalse _
  | Typing.TNull                    -> []
  | Typing.TNum (n, _)              -> [S1_Integer n]
  | Typing.TCall (e, name, args, _) -> let t = Typing.extract_type e in build_cpinfo_step1_expr_call t name classdefs @ List.fold_right (fun arg -> fun xs -> build_cpinfo_step1_expr arg classdefs supertype @ xs) args []
  | Typing.TSuper args              -> let c = get_constructor_def classdefs supertype in build_cpinfo_step1_constructor c supertype.Syntax.id @ List.fold_right (fun arg -> fun xs -> build_cpinfo_step1_expr arg classdefs supertype @ xs) args []
  | Typing.TField (e, name, _)      -> let t = Typing.extract_type e in build_cpinfo_step1_expr_field t name classdefs
  | Typing.TNew (name, args, _)     -> let c = get_constructor_def classdefs name in build_cpinfo_step1_constructor c name.Syntax.id @ List.fold_right (fun arg -> fun xs -> build_cpinfo_step1_expr arg classdefs supertype @ xs) args []
  | Typing.TAsgn (e1, e2, _)
  | Typing.TPlus (e1, e2, _)
  | Typing.TMinus (e1, e2, _)
  | Typing.TTimes (e1, e2, _)
  | Typing.TDiv (e1, e2, _)
  | Typing.TMod (e1, e2, _)
  | Typing.TLt (e1, e2, _)
  | Typing.TLeq (e1, e2, _)
  | Typing.TGt (e1, e2, _)
  | Typing.TGeq (e1, e2, _)
  | Typing.TEq (e1, e2, _)
  | Typing.TNeq (e1, e2, _)
  | Typing.TAnd (e1, e2, _)
  | Typing.TOr (e1, e2, _)          -> build_cpinfo_step1_expr e1 classdefs supertype @ build_cpinfo_step1_expr e2 classdefs supertype
  | Typing.TUnaryMinus (e, _)
  | Typing.TNot (e, _)              -> build_cpinfo_step1_expr e classdefs supertype
  | Typing.TCast (t, e)             -> build_cpinfo_step1_expr e classdefs supertype @ build_cpinfo_step1_cast t

let rec build_cpinfo_step1_seq seq classdefs supertype =
  match seq with
  | Typing.TEndSeq               -> []
  | Typing.TDeclaration (_, seq) -> build_cpinfo_step1_seq seq classdefs supertype
  | Typing.TExpression (e, seq)  -> build_cpinfo_step1_expr e classdefs supertype @ build_cpinfo_step1_seq seq classdefs supertype
  | Typing.TIf (e, s1, s2, s3)   -> build_cpinfo_step1_expr e classdefs supertype @ build_cpinfo_step1_seq s1 classdefs supertype @ build_cpinfo_step1_seq s2 classdefs supertype @ build_cpinfo_step1_seq s3 classdefs supertype
  | Typing.TWhile (e, s1, s2)    -> build_cpinfo_step1_expr e classdefs supertype @ build_cpinfo_step1_seq s1 classdefs supertype @ build_cpinfo_step1_seq s2 classdefs supertype
  | Typing.TReturn e             -> build_cpinfo_step1_expr e classdefs supertype

let rec build_cpinfo_step1_body body classdefs supertype =
  match body with
  | Typing.TEmptyBody                                             -> []
  | Typing.TClassField (_, body)                                  -> build_cpinfo_step1_body body classdefs supertype
  | Typing.TClassConstructor (Typing.TConstructor (_, seq), body)
  | Typing.TClassMethod (Typing.TMethod (_, _, _, seq), body)     -> build_cpinfo_step1_seq seq classdefs supertype @ build_cpinfo_step1_body body classdefs supertype

let rec build_cpinfo_step1_body_defs body classdefs currtype =
  match body with
  | Typing.TEmptyBody                  -> []
  | Typing.TClassField (v, body)       -> build_cpinfo_step1_var v currtype.Syntax.id @ build_cpinfo_step1_body_defs body classdefs currtype
  | Typing.TClassConstructor (c, body) -> build_cpinfo_step1_tconstructor c currtype.Syntax.id @ build_cpinfo_step1_body_defs body classdefs currtype
  | Typing.TClassMethod (m, body)      -> build_cpinfo_step1_tmethod m currtype.Syntax.id @ build_cpinfo_step1_body_defs body classdefs currtype

(* Get methods and fields used in method and constructor definitions *)
let build_cpinfo_step1 cdef classdefs =
  match cdef with
  | Typing.TClass (t1, t2, body) -> build_cpinfo_step1_body body classdefs t2

let rec build_cpinfo_step2_classes info =
  match info with
  | []                           -> []
  | S1_Fieldref (n, _, _) :: xs
  | S1_Methodref (n, _, _) :: xs -> S2_Class n :: build_cpinfo_step2_classes xs
  | S1_Integer _ :: xs           -> build_cpinfo_step2_classes xs
  | S1_Cast n :: xs              -> S2_Class n :: build_cpinfo_step2_classes xs

let rec build_cpinfo_step2_noclasses info =
  match info with
  | []                           -> []
  | S1_Fieldref (t, n, d) :: xs  -> S2_Fieldref (0, n, d) :: build_cpinfo_step2_noclasses xs
  | S1_Methodref (t, n, d) :: xs -> S2_Methodref (0, n, d) :: build_cpinfo_step2_noclasses xs
  | S1_Integer _ :: xs           -> build_cpinfo_step2_noclasses xs
  | S1_Cast _ :: xs              -> build_cpinfo_step2_noclasses xs

let merge_tables_step2 info classes =
  let l = List.length (List.filter (fun x -> match x with S1_Cast _ -> false | _ -> true) info) + 1
  in let rec merge_tables_internal info =
    match info with
    | []                           -> classes
    | S1_Fieldref (t, n, d) :: xs  -> let i = index classes (S2_Class t) in S2_Fieldref (i+l, n, d) :: merge_tables_internal xs
    | S1_Methodref (t, n, d) :: xs -> let i = index classes (S2_Class t) in S2_Methodref (i+l, n, d) :: merge_tables_internal xs
    | S1_Integer n :: xs           -> S2_Integer n :: merge_tables_internal xs
    | S1_Cast _ :: xs              -> merge_tables_internal xs
  in merge_tables_internal info

let get_cpinfo_current_class (Typing.TClass (t, _, _)) = S2_Class t.Syntax.id

let get_cpinfo_superclass (Typing.TClass (_, t, _)) = S2_Class t.Syntax.id

(* Get classes used in cp table *)
let build_cpinfo_step2 cdef classdefs =
  let info_step1 = uniq (build_cpinfo_step1 cdef classdefs)
  in let classes = uniq (get_cpinfo_current_class cdef :: get_cpinfo_superclass cdef :: build_cpinfo_step2_classes info_step1)
  in merge_tables_step2 info_step1 classes

let rec build_cpinfo_step3_descriptors info =
  match info with
  | []                           -> []
  | S2_Class _ :: xs
  | S2_Integer _ :: xs           -> build_cpinfo_step3_descriptors xs
  | S2_Fieldref (_, n, d) :: xs
  | S2_Methodref (_, n, d) :: xs -> S3_NameAndType (n, d) :: build_cpinfo_step3_descriptors xs

let merge_tables_step3 info descriptors =
  let l = List.length info + 1
  in let rec merge_tables_internal info =
    match info with
    | []                           -> descriptors
    | S2_Class n :: xs             -> S3_Class n :: merge_tables_internal xs
    | S2_Fieldref (c, n, d) :: xs  -> let i = index descriptors (S3_NameAndType (n, d)) in S3_Fieldref (c, i+l) :: merge_tables_internal xs
    | S2_Methodref (c, n, d) :: xs -> let i = index descriptors (S3_NameAndType (n, d)) in S3_Methodref (c, i+l) :: merge_tables_internal xs
    | S2_Integer n :: xs           -> S3_Integer n :: merge_tables_internal xs
  in merge_tables_internal info

(* Get field and method descriptors *)
let build_cpinfo_step3 cdef classdefs =
  let info_step2 = build_cpinfo_step2 cdef classdefs
  in let descriptors = uniq (build_cpinfo_step3_descriptors info_step2)
  in merge_tables_step3 info_step2 descriptors

let rec build_cpinfo_step4_names info =
  match info with
  | []                            -> []
  | S3_Class n :: xs              -> Utf8 (String.length n, Bytes.of_string n) :: build_cpinfo_step4_names xs
  | S3_Fieldref (_, _) :: xs
  | S3_Methodref (_, _) :: xs
  | S3_Integer _ :: xs            -> build_cpinfo_step4_names xs
  | S3_NameAndType (n1, n2) :: xs -> Utf8 (String.length n1, Bytes.of_string n1) :: Utf8 (String.length n2, Bytes.of_string n2) :: build_cpinfo_step4_names xs

let merge_tables_step4 info names =
  let l = List.length info + 1
  in let rec merge_tables_internal info =
    match info with
    | []                            -> names
    | S3_Class n :: xs              -> let i = index names (Utf8 (String.length n, Bytes.of_string n)) in Class (i+l) :: merge_tables_internal xs
    | S3_Fieldref (i1, i2) :: xs    -> Fieldref (i1, i2) :: merge_tables_internal xs
    | S3_Methodref (i1, i2) :: xs   -> Methodref (i1, i2) :: merge_tables_internal xs
    | S3_Integer n :: xs            -> Integer n :: merge_tables_internal xs
    | S3_NameAndType (n1, n2) :: xs -> let (i1, i2) = (index names (Utf8 (String.length n1, Bytes.of_string n1)), index names (Utf8 (String.length n2, Bytes.of_string n2))) in NameAndType (i1+l, i2+l) :: merge_tables_internal xs
  in merge_tables_internal info

let get_local_method_and_field_descriptors (Typing.TClass (t1, _, body)) classdefs =
  List.filter (function Utf8 (_, _) -> true | _ -> false) (build_cpinfo_step4_names (build_cpinfo_step3_descriptors (build_cpinfo_step2_noclasses (build_cpinfo_step1_body_defs body classdefs t1))))

(* Build cp_info table *)
let build_cpinfo cdef classdefs =
  let info_step3 = uniq (build_cpinfo_step3 cdef classdefs)
  in let names = uniq (build_cpinfo_step4_names info_step3 @ get_local_method_and_field_descriptors cdef classdefs @ [Utf8 (4, Bytes.of_string "Code")])
  in merge_tables_step4 info_step3 names

(*
let find_constructor cpool name =
  let i_cname = Utils.index cpool (Utf8 (String.length name, Bytes.of_string name)) + 1 and i_init = Utils.index cpool (Utf8 (6, Bytes.of_string "<init>")) + 1
  in let i_class = Utils.index cpool (Class i_cname) + 1
  in let i_descr = Utils.first_index (function Methodref (i1, i2) ->
      if i1 = i_class then (if Utils.
*)

let rec explain c cp comment =
  match cp with
  | Class n            -> comment ^ explain c (List.nth c (n-1)) ""
  | Fieldref (a, b)
  | Methodref (a, b)   -> comment ^ explain c (List.nth c (a-1)) "" ^ "." ^ explain c (List.nth c (b-1)) ""
  | Integer _          -> ""
  | NameAndType (a, b) -> comment ^ explain c (List.nth c (a-1)) "" ^ ":" ^ explain c (List.nth c (b-1)) ""
  | Utf8 (_, s)        -> Bytes.to_string s

let format_cpinfo_str c =
  let l = int_of_float (ceil (log10 (float_of_int (List.length c)))) + 3
  in let format_cpinfo_entry cp =
    match cp with
    | Class n            -> "Class              " ^ Printf.sprintf "%-15s" ("#" ^ string_of_int n) ^ explain c cp "// "
    | Fieldref (a, b)    -> "Fieldref           " ^ Printf.sprintf "%-15s" ("#" ^ string_of_int a ^ ".#" ^ string_of_int b) ^ explain c cp "// "
    | Methodref (a, b)   -> "Methodref          " ^ Printf.sprintf "%-15s" ("#" ^ string_of_int a ^ ".#" ^ string_of_int b) ^ explain c cp "// "
    | Integer n          -> "Integer            " ^ Printf.sprintf "%-15s" (string_of_int n)
    | NameAndType (a, b) -> "NameAndType        " ^ Printf.sprintf "%-15s" ("#" ^ string_of_int a ^ ":#" ^ string_of_int b) ^ explain c cp "// "
    | Utf8 (_, b)        -> "Utf8               " ^ Bytes.to_string b
  in let rec format_cpinfo_internal c rownum =
    match c with
    | []      -> ""
    | x :: xs -> (Printf.sprintf ((Scanf.format_from_string ("%" ^ string_of_int l ^ "s") "%s") ^^ " = ") ("#" ^ string_of_int rownum)) ^ format_cpinfo_entry x ^ "\n" ^ format_cpinfo_internal xs (rownum+1)
  in "Constant pool:\n" ^ format_cpinfo_internal c 1

let rec get_text l id =
  match l with
  | []      -> failwith "Not found"
  | x :: xs -> if id = 1
                 then (match x with
                       | Utf8 (_, b) -> Bytes.to_string b
                       | _           -> failwith "Invalid index - element is not utf8 constant")
                 else get_text xs (id-1)

let get_text_r cptable id =
  let rec get_text_r_internal l id =
    match l with
    | [] -> failwith "Not found"
    | x :: xs -> if id = 1
                   then (match x with
                         | Utf8 (_, b)        -> Bytes.to_string b
                         | Class i
                         | Fieldref (_, i)
                         | Methodref (_, i)
                         | NameAndType (i, _) -> get_text_r_internal cptable i
                         | _                  -> failwith "Invalid index - element is not named constant")
                   else get_text_r_internal xs (id-1)
  in get_text_r_internal cptable id

