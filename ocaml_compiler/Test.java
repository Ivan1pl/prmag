class Test extends Object {

    int a;
    boolean b;

    Test(int a) {
        super();
        if (a >= 0) {
            this.a = a;
        } else {
            this.a = 0;
        }
    }

    Test copy() {
        return new Test(this.a);
    }

    Test obj(Object o) {
        Test t;
        t = (Test) o;
        return t;
    }
}

