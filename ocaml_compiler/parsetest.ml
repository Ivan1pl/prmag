open Lexing
open Syntax

let usage () =
  Printf.eprintf "Usage: %s file ...\n" Sys.argv.(0);
  exit 1

let error msg =
  Printf.eprintf "%s: %s\n" (Source.location ()) msg

exception ParseError of (exn * (int * int * string))

let parse () =
  try
    let _ = Parsing.set_trace false in
    Source.with_lexbuf
      (fun lexbuf ->
        try
          Parser.goal Lexer.token lexbuf
        with e ->
          begin
            let curr = lexbuf.Lexing.lex_curr_p in
            let line = curr.Lexing.pos_lnum in
            let cnum = curr.Lexing.pos_cnum - curr.Lexing.pos_bol in
            let tok = Lexing.lexeme lexbuf in
            raise (ParseError (e,(line,cnum,tok)))
          end)
  with ParseError (_, (line, cnum, tok)) ->
    begin
      print_endline "Parse error";
      print_endline ("In line: " ^ string_of_int line);
      print_endline ("On position: " ^ string_of_int cnum);
      print_endline ("On token: " ^ tok);
      failwith "Error";
    end
  (*with e -> failwith "Error"*)
  (* with e -> error (Printexc.to_string e) *)

let write_class name bytes =
  let out = open_out_bin (name ^ ".class")
  in output_bytes out bytes; close_out out

let process_class cdef classdefs =
  let cname = Syntax.get_name cdef
  and typed = Typing.assign_types cdef classdefs
  in let cptable = Cpinfo.build_cpinfo typed classdefs
  in let linked = Linking.link_class classdefs typed cptable
  in let translated = Translating.translate_class linked
  in let cclass = Byteoffsets.calculate_offsets translated
  in let () = print_endline (String.make 80 '#'); print_endline (Byteoffsets.format_class_str cclass)
  in let encoded = Classfile.encode cclass
  in write_class cname encoded

let process_classes classdefs =
  let rec process_classes_internal cdefs =
    match cdefs with
    | []         -> print_endline (String.make 80 '#')
    | cdef :: xs -> process_class cdef classdefs; process_classes_internal xs
  in process_classes_internal classdefs

let _ =
  let argc = Array.length Sys.argv in
  if argc <= 1 then
    usage ();
  let classdefs = ref [] in
  begin
    for i = 1 to argc-1 do
      Source.set_file_name Sys.argv.(i);
      classdefs := (parse ()) :: !classdefs;
    done;
    process_classes !classdefs;
  end
  
