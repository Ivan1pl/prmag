open Byteoffsets

let byte_mask = 0xFF

let rec char_list_of_bytes b =
  let l = Bytes.length b
  in if l = 0 then [] else Bytes.get b 0 :: char_list_of_bytes (Bytes.sub b 1 (l-1))

let bytes_of_char_list l =
  let binit = Bytes.create (List.length l)
  in let () = List.iteri (fun i -> fun ch -> Bytes.set binit i ch) l
  in binit

let encode_cp_info cpinfo =
  let encode_cp_info_internal info =
    match info with
    | Cpinfo.Class i              -> [Char.chr 7; Char.chr (i lsr 8); Char.chr (i land byte_mask)]
    | Cpinfo.Fieldref (i1, i2)    -> [Char.chr 9; Char.chr (i1 lsr 8); Char.chr (i1 land byte_mask); Char.chr (i2 lsr 8); Char.chr (i2 land byte_mask)]
    | Cpinfo.Methodref (i1, i2)   -> [Char.chr 10; Char.chr (i1 lsr 8); Char.chr (i1 land byte_mask); Char.chr (i2 lsr 8); Char.chr (i2 land byte_mask)]
    | Cpinfo.Integer i            -> [Char.chr 3; Char.chr (i lsr 24); Char.chr ((i lsr 16) land byte_mask); Char.chr ((i lsr 8) land byte_mask); Char.chr (i land byte_mask)]
    | Cpinfo.NameAndType (i1, i2) -> [Char.chr 12; Char.chr (i1 lsr 8); Char.chr (i1 land byte_mask); Char.chr (i2 lsr 8); Char.chr (i2 land byte_mask)]
    | Cpinfo.Utf8 (i, b)          -> [Char.chr 1; Char.chr (i lsr 8); Char.chr (i land byte_mask)] @ char_list_of_bytes b
  and l = List.length cpinfo + 1
  in let rec encode_table t =
    match t with
    | []      -> []
    | x :: xs -> encode_cp_info_internal x @ encode_table xs
  in [Char.chr (l lsr 8); Char.chr (l land byte_mask)] @ encode_table cpinfo

let encode_fields fields =
  let l = List.length fields
  in let rec encode_fields_internal f =
    match f with
    | []                   -> []
    | Field (i1, i2) :: xs -> [Char.chr 0; Char.chr 1; Char.chr (i1 lsr 8); Char.chr (i1 land byte_mask); Char.chr (i2 lsr 8); Char.chr (i2 land byte_mask); Char.chr 0; Char.chr 0] @ encode_fields_internal xs
  in [Char.chr (l lsr 8); Char.chr (l land byte_mask)] @ encode_fields_internal fields

let encode_instr instr =
  match instr with
  | Aconst_null     -> [Char.chr 0x01]
  | Aload i         -> [Char.chr 0x19; Char.chr i]
  | Aload_0         -> [Char.chr 0x2A]
  | Aload_1         -> [Char.chr 0x2B]
  | Aload_2         -> [Char.chr 0x2C]
  | Aload_3         -> [Char.chr 0x2D]
  | Areturn         -> [Char.chr 0xB0]
  | Astore i        -> [Char.chr 0x3A; Char.chr i]
  | Astore_0        -> [Char.chr 0x4B]
  | Astore_1        -> [Char.chr 0x4C]
  | Astore_2        -> [Char.chr 0x4D]
  | Astore_3        -> [Char.chr 0x4E]
  | Checkcast i     -> [Char.chr 0xC0; Char.chr ((i lsr 8) land byte_mask); Char.chr (i land byte_mask)]
  | Dup             -> [Char.chr 0x59]
  | Dup_x1          -> [Char.chr 0x5A]
  | Getfield i      -> [Char.chr 0xB4; Char.chr ((i lsr 8) land byte_mask); Char.chr (i land byte_mask)]
  | Goto i          -> [Char.chr 0xA7; Char.chr ((i lsr 8) land byte_mask); Char.chr (i land byte_mask)]
  | Iadd            -> [Char.chr 0x60]
  | Iand            -> [Char.chr 0x7E]
  | Iconst_0        -> [Char.chr 0x03]
  | Iconst_1        -> [Char.chr 0x04]
  | Idiv            -> [Char.chr 0x6C]
  | If_acmpeq i     -> [Char.chr 0xA5; Char.chr ((i lsr 8) land byte_mask); Char.chr (i land byte_mask)]
  | If_acmpne i     -> [Char.chr 0xA6; Char.chr ((i lsr 8) land byte_mask); Char.chr (i land byte_mask)]
  | If_icmpeq i     -> [Char.chr 0x9F; Char.chr ((i lsr 8) land byte_mask); Char.chr (i land byte_mask)]
  | If_icmpge i     -> [Char.chr 0xA2; Char.chr ((i lsr 8) land byte_mask); Char.chr (i land byte_mask)]
  | If_icmpgt i     -> [Char.chr 0xA3; Char.chr ((i lsr 8) land byte_mask); Char.chr (i land byte_mask)]
  | If_icmple i     -> [Char.chr 0xA4; Char.chr ((i lsr 8) land byte_mask); Char.chr (i land byte_mask)]
  | If_icmplt i     -> [Char.chr 0xA1; Char.chr ((i lsr 8) land byte_mask); Char.chr (i land byte_mask)]
  | If_icmpne i     -> [Char.chr 0xA0; Char.chr ((i lsr 8) land byte_mask); Char.chr (i land byte_mask)]
  | Ifeq i          -> [Char.chr 0x99; Char.chr ((i lsr 8) land byte_mask); Char.chr (i land byte_mask)]
  | Ifne i          -> [Char.chr 0x9A; Char.chr ((i lsr 8) land byte_mask); Char.chr (i land byte_mask)]
  | Ifnonnull i     -> [Char.chr 0xC7; Char.chr ((i lsr 8) land byte_mask); Char.chr (i land byte_mask)]
  | Ifnull i        -> [Char.chr 0xC6; Char.chr ((i lsr 8) land byte_mask); Char.chr (i land byte_mask)]
  | Iload i         -> [Char.chr 0x15; Char.chr i]
  | Iload_0         -> [Char.chr 0x1A]
  | Iload_1         -> [Char.chr 0x1B]
  | Iload_2         -> [Char.chr 0x1C]
  | Iload_3         -> [Char.chr 0x1D]
  | Imul            -> [Char.chr 0x68]
  | Ineg            -> [Char.chr 0x74]
  | Ior             -> [Char.chr 0x80]
  | Invokespecial i -> [Char.chr 0xB7; Char.chr ((i lsr 8) land byte_mask); Char.chr (i land byte_mask)]
  | Invokevirtual i -> [Char.chr 0xB6; Char.chr ((i lsr 8) land byte_mask); Char.chr (i land byte_mask)]
  | Irem            -> [Char.chr 0x70]
  | Ireturn         -> [Char.chr 0xAC]
  | Istore i        -> [Char.chr 0x36; Char.chr i]
  | Istore_0        -> [Char.chr 0x3B]
  | Istore_1        -> [Char.chr 0x3C]
  | Istore_2        -> [Char.chr 0x3D]
  | Istore_3        -> [Char.chr 0x3E]
  | Isub            -> [Char.chr 0x64]
  | Ldc i           -> [Char.chr 0x12; Char.chr i]
  | Ldc_w i         -> [Char.chr 0x13; Char.chr ((i lsr 8) land byte_mask); Char.chr (i land byte_mask)]
  | New i           -> [Char.chr 0xBB; Char.chr ((i lsr 8) land byte_mask); Char.chr (i land byte_mask)]
  | Putfield i      -> [Char.chr 0xB5; Char.chr ((i lsr 8) land byte_mask); Char.chr (i land byte_mask)]
  | Return          -> [Char.chr 0xB1]

let encode_code code locals stack cpinfo =
  let code_str_index = Utils.index cpinfo (Cpinfo.Utf8 (4, Bytes.of_string "Code")) + 1
  in let rec encode_code_internal c =
    match c with
    | []                   -> []
    | Command (_, e) :: xs -> encode_instr e @ encode_code_internal xs
  in let ncode = [Char.chr (stack lsr 8); Char.chr (stack land byte_mask); Char.chr (locals lsr 8); Char.chr (locals land byte_mask)]
  and lcode = encode_code_internal code
  in let clen = List.length lcode
  in let encoded_clen = [Char.chr (clen lsr 24); Char.chr ((clen lsr 16) land byte_mask); Char.chr ((clen lsr 8) land byte_mask); Char.chr (clen land byte_mask)]
  in let clist = ncode @ encoded_clen @ lcode @ [Char.chr 0; Char.chr 0; Char.chr 0; Char.chr 0]
  in let byte_length = List.length clist
  in let encoded_byte_length = [Char.chr (byte_length lsr 24); Char.chr ((byte_length lsr 16) land byte_mask); Char.chr ((byte_length lsr 8) land byte_mask); Char.chr (byte_length land byte_mask)]
  in [Char.chr (code_str_index lsr 8); Char.chr (code_str_index land byte_mask)] @ encoded_byte_length @ clist

let encode_methods methods cpinfo =
  let l = List.length methods
  in let rec encode_methods_internal m =
    match m with
    | []                                            -> []
    | Method (i1, i2, locals, _, stack, code) :: xs -> let encoded_code = encode_code code locals stack cpinfo
                                                       in [Char.chr 0; Char.chr 1; Char.chr (i1 lsr 8); Char.chr (i1 land byte_mask); Char.chr (i2 lsr 8); Char.chr (i2 land byte_mask); Char.chr 0; Char.chr 1] @ encoded_code @ encode_methods_internal xs
  in [Char.chr (l lsr 8); Char.chr (l land byte_mask)] @ encode_methods_internal methods

let encode_class (Class (i1, i2, cpinfo, fields, methods)) =
  let encoded_cpinfo = encode_cp_info cpinfo
  and encoded_fields = encode_fields fields
  and encoded_methods = encode_methods methods cpinfo
  in encoded_cpinfo @ [Char.chr 0; Char.chr 1; Char.chr (i1 lsr 8); Char.chr (i1 land byte_mask); Char.chr (i2 lsr 8); Char.chr (i2 land byte_mask); Char.chr 0; Char.chr 0] @ encoded_fields @ encoded_methods @ [Char.chr 0; Char.chr 0]

let encode c =
  bytes_of_char_list ([Char.chr 0xCA; Char.chr 0xFE; Char.chr 0xBA; Char.chr 0xBE; Char.chr 0; Char.chr 0; Char.chr 0; Char.chr 46] @ encode_class c)

