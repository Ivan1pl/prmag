open Cpinfo
open Utils

type exprType = Typing.exprType

type jExpr =
  | Asgn of jExpr * jExpr * exprType
  | Val of int * exprType
  | Call of jExpr * int * jExpr list * exprType
  | Super of jExpr * int * jExpr list
  | Field of jExpr * int * exprType
  | New of int * int * jExpr list * exprType    (* class_id, constructor_id, args, type *)
  | Num of int * exprType
  | Plus of jExpr * jExpr * exprType
  | Minus of jExpr * jExpr * exprType
  | Times of jExpr * jExpr * exprType
  | Div of jExpr * jExpr * exprType
  | Mod of jExpr * jExpr * exprType
  | UnaryMinus of jExpr * exprType
  | Lt of jExpr * jExpr * exprType
  | Leq of jExpr * jExpr * exprType
  | Gt of jExpr * jExpr * exprType
  | Geq of jExpr * jExpr * exprType
  | Eq of jExpr * jExpr * exprType
  | Neq of jExpr * jExpr * exprType
  | JTrue of exprType
  | JFalse of exprType
  | And of jExpr * jExpr * exprType
  | Or of jExpr * jExpr * exprType
  | Not of jExpr * exprType
  | Null
  | Cast of int * jExpr * exprType

type jSeq =
  | EndSeq
  | Expression of jExpr * jSeq
  | If of jExpr * jSeq * jSeq * jSeq
  | While of jExpr * jSeq * jSeq
  | Return of jExpr

type jMethod =
  | Method of int * int * int * int * jSeq (* name_index, descriptor_index, local_vars_count, args_size, seq *)

type jField =
  | Field of int * int (* name_index, descriptor_index *)

type jClassBody =
  | EmptyBody
  | ClassField of jField * jClassBody
  | ClassMethod of jMethod * jClassBody

type jClass =
  | Class of int * int * cp_info list * jClassBody

let extract_type e =
  match e with
  | Asgn (_, _, t)
  | Val (_, t)
  | Call (_, _, _, t)
  | Field (_, _, t)
  | New (_, _, _, t)
  | Num (_, t)
  | Plus (_, _, t)
  | Minus (_, _, t)
  | Times (_, _, t)
  | Div (_, _, t)
  | Mod (_, _, t)
  | UnaryMinus (_, t)
  | Lt (_, _, t)
  | Leq (_, _, t)
  | Gt (_, _, t)
  | Geq (_, _, t)
  | Eq (_, _, t)
  | Neq (_, _, t)
  | JTrue t
  | JFalse t
  | And (_, _, t)
  | Or (_, _, t)
  | Not (_, t)
  | Cast (_, _, t)    -> Some t
  | Super (_, _, _)   -> failwith "Invalid use of super"
  | Null              -> None

let find_utf8_index name cptable =
  let rec find_utf8_index_aux cptable idx =
    match cptable with
    | []                  -> failwith ("Linking: " ^ name ^ " not found in constant pool table")
    | Utf8 (l, str) :: xs -> if Bytes.of_string name = str && String.length name = l then idx else find_utf8_index_aux xs (idx+1)
    | _ :: xs             -> find_utf8_index_aux xs (idx+1)
  in find_utf8_index_aux cptable 1

let rec extract_names args =
  match args with
  | []                      -> []
  | Syntax.Var (_, i) :: xs -> i.Syntax.id :: extract_names xs

let add_local_var vars (Syntax.Var (_, i)) =
  if List.mem i.Syntax.id vars then failwith ("Variable " ^ i.Syntax.id ^ " already declared") else vars @ [i.Syntax.id]

let get_method_id_internal m owner cptable =
  match m with
  | Syntax.Method (rettype, name, args, _) -> let descriptor = make_method_descriptor args rettype
                                              in let name_id = find_utf8_index name.Syntax.id cptable
                                              and owner_id = find_utf8_index owner.Syntax.id cptable
                                              and descriptor_id = find_utf8_index descriptor cptable
                                              in let name_and_type_id = index cptable (NameAndType (name_id, descriptor_id)) + 1
                                              and class_id = index cptable (Class owner_id) + 1
                                              in index cptable (Methodref (class_id, name_and_type_id)) + 1

let get_method_id owner name classdefs cptable =
  match owner with
  | Syntax.Int
  | Syntax.Boolean -> failwith "Calling method on primitive type value"
  | Syntax.Named n -> let m = get_method_def classdefs n name
                      in get_method_id_internal m n cptable

let get_constructor_id owner classdefs cptable =
  let get_constructor_id_internal c =
    match c with
    | Syntax.Constructor (_, args, _) -> let descriptor = make_constructor_descriptor args
                                         in let name_id = find_utf8_index "<init>" cptable
                                         and owner_id = find_utf8_index owner.Syntax.id cptable
                                         and descriptor_id = find_utf8_index descriptor cptable
                                         in let name_and_type_id = index cptable (NameAndType (name_id, descriptor_id)) + 1
                                         and class_id = index cptable (Class owner_id) + 1
                                         in index cptable (Methodref (class_id, name_and_type_id)) + 1
  in get_constructor_id_internal (get_constructor_def classdefs owner)

let get_field_id_internal f owner cptable =
  match f with
  | Syntax.Var (t, i) -> let descriptor = make_field_descriptor t
                         in let name_id = find_utf8_index i.Syntax.id cptable
                         and owner_id = find_utf8_index owner.Syntax.id cptable
                         and descriptor_id = find_utf8_index descriptor cptable
                         in let name_and_type_id = index cptable (NameAndType (name_id, descriptor_id)) + 1
                         and class_id = index cptable (Class owner_id) + 1
                         in index cptable (Fieldref (class_id, name_and_type_id)) + 1

let get_field_id owner name classdefs cptable =
  match owner with
  | Syntax.Int
  | Syntax.Boolean -> failwith "Accessing field of primitive type value"
  | Syntax.Named n -> let f = get_field_def classdefs n name
                      in get_field_id_internal f n cptable

let get_reference_type_id t cptable =
  match t with
  | Syntax.Named n -> let name_index = find_utf8_index n.Syntax.id cptable
                      in Utils.index cptable (Cpinfo.Class name_index) + 1
  | _              -> failwith "Expected reference type"

let rec link_expr classdefs e local_vars cptable currtype supertype =
  match e with
  | Typing.TThis t               -> Val (0, t)
  | Typing.TAsgn (e1, e2, t)     -> Asgn (link_expr classdefs e1 local_vars cptable currtype supertype, link_expr classdefs e2 local_vars cptable currtype supertype, t)
  | Typing.TVal (n, t)           -> let name_index = index local_vars n.Syntax.id in Val (name_index + 1, t)
  | Typing.TCall (e, n, args, t) -> let method_index = get_method_id (Typing.extract_type e) n classdefs cptable
                                    in Call (link_expr classdefs e local_vars cptable currtype supertype, method_index, List.map (fun arg -> link_expr classdefs arg local_vars cptable currtype supertype) args, t)
  | Typing.TSuper args           -> let method_index = get_constructor_id supertype classdefs cptable
                                    in Super (Val (0, Syntax.Named currtype), method_index, List.map (fun arg -> link_expr classdefs arg local_vars cptable currtype supertype) args)
  | Typing.TField (e, n, t)      -> let field_index = get_field_id (Typing.extract_type e) n classdefs cptable
                                    in Field (link_expr classdefs e local_vars cptable currtype supertype, field_index, t)
  | Typing.TNew (n, args, t)     -> let class_name_index = find_utf8_index n.Syntax.id cptable
                                    and method_index = get_constructor_id n classdefs cptable
                                    in let class_index = index cptable (Class class_name_index) + 1
                                    in New (class_index, method_index, List.map (fun arg -> link_expr classdefs arg local_vars cptable currtype supertype) args, t)
  | Typing.TNum (n, t)           -> let num_index = index cptable (Integer n) + 1
                                    in Num (num_index, t)
  | Typing.TPlus (e1, e2, t)     -> Plus (link_expr classdefs e1 local_vars cptable currtype supertype, link_expr classdefs e2 local_vars cptable currtype supertype, t)
  | Typing.TMinus (e1, e2, t)    -> Minus (link_expr classdefs e1 local_vars cptable currtype supertype, link_expr classdefs e2 local_vars cptable currtype supertype, t)
  | Typing.TTimes (e1, e2, t)    -> Times (link_expr classdefs e1 local_vars cptable currtype supertype, link_expr classdefs e2 local_vars cptable currtype supertype, t)
  | Typing.TDiv (e1, e2, t)      -> Div (link_expr classdefs e1 local_vars cptable currtype supertype, link_expr classdefs e2 local_vars cptable currtype supertype, t)
  | Typing.TMod (e1, e2, t)      -> Mod (link_expr classdefs e1 local_vars cptable currtype supertype, link_expr classdefs e2 local_vars cptable currtype supertype, t)
  | Typing.TUnaryMinus (e, t)    -> UnaryMinus (link_expr classdefs e local_vars cptable currtype supertype, t)
  | Typing.TLt (e1, e2, t)       -> Lt (link_expr classdefs e1 local_vars cptable currtype supertype, link_expr classdefs e2 local_vars cptable currtype supertype, t)
  | Typing.TLeq (e1, e2, t)      -> Leq (link_expr classdefs e1 local_vars cptable currtype supertype, link_expr classdefs e2 local_vars cptable currtype supertype, t)
  | Typing.TGt (e1, e2, t)       -> Gt (link_expr classdefs e1 local_vars cptable currtype supertype, link_expr classdefs e2 local_vars cptable currtype supertype, t)
  | Typing.TGeq (e1, e2, t)      -> Geq (link_expr classdefs e1 local_vars cptable currtype supertype, link_expr classdefs e2 local_vars cptable currtype supertype, t)
  | Typing.TEq (e1, e2, t)       -> Eq (link_expr classdefs e1 local_vars cptable currtype supertype, link_expr classdefs e2 local_vars cptable currtype supertype, t)
  | Typing.TNeq (e1, e2, t)      -> Neq (link_expr classdefs e1 local_vars cptable currtype supertype, link_expr classdefs e2 local_vars cptable currtype supertype, t)
  | Typing.TJTrue t              -> JTrue t
  | Typing.TJFalse t             -> JFalse t
  | Typing.TAnd (e1, e2, t)      -> And (link_expr classdefs e1 local_vars cptable currtype supertype, link_expr classdefs e2 local_vars cptable currtype supertype, t)
  | Typing.TOr (e1, e2, t)       -> Or (link_expr classdefs e1 local_vars cptable currtype supertype, link_expr classdefs e2 local_vars cptable currtype supertype, t)
  | Typing.TNot (e, t)           -> Not (link_expr classdefs e local_vars cptable currtype supertype, t)
  | Typing.TNull                 -> Null
  | Typing.TCast (t, e)          -> Cast (get_reference_type_id t cptable, link_expr classdefs e local_vars cptable currtype supertype, t)

let link_seq seq classdefs local_vars cptable currtype supertype =
  let rec link_seq_aux seq local_vars curr_count =
    match seq with
    | Typing.TEndSeq               -> (EndSeq, curr_count)
    | Typing.TDeclaration (v, seq) -> link_seq_aux seq (add_local_var local_vars v) (curr_count+1)
    | Typing.TExpression (e, seq)  -> let le = link_expr classdefs e local_vars cptable currtype supertype and (newseq, count) = link_seq_aux seq local_vars curr_count in (Expression (le, newseq), count)
    | Typing.TIf (e, s1, s2, s3)   -> let le = link_expr classdefs e local_vars cptable currtype supertype
                                      and (news1, count1) = link_seq_aux s1 local_vars curr_count
                                      and (news2, count2) = link_seq_aux s2 local_vars curr_count
                                      and (news3, count3) = link_seq_aux s3 local_vars curr_count
                                      in (If (le, news1, news2, news3), list_max [count1; count2; count3])
    | Typing.TWhile (e, s1, s2)    -> let le = link_expr classdefs e local_vars cptable currtype supertype
                                      and (news1, count1) = link_seq_aux s1 local_vars curr_count
                                      and (news2, count2) = link_seq_aux s2 local_vars curr_count
                                      in (While (le, news1, news2), list_max [count1; count2])
    | Typing.TReturn e             -> (Return (link_expr classdefs e local_vars cptable currtype supertype), curr_count)
  in link_seq_aux seq local_vars (List.length local_vars + 1)

let link_method classdefs m cptable currtype supertype =
  match m with
  | Typing.TMethod (rettype, name, args, seq) ->
          let name_index = find_utf8_index name.Syntax.id cptable
          and descriptor_index = find_utf8_index (make_method_descriptor args rettype) cptable
          and (newseq, local_vars_count) = link_seq seq classdefs (extract_names args) cptable currtype supertype
          in Method (name_index, descriptor_index, local_vars_count, List.length args + 1, newseq)

let link_constructor classdefs c cptable currtype supertype =
  match c with
  | Typing.TConstructor (args, seq) ->
          let name_index = find_utf8_index "<init>" cptable
          and descriptor_index = find_utf8_index (make_constructor_descriptor args) cptable
          and (newseq, local_vars_count) = link_seq seq classdefs (extract_names args) cptable currtype supertype
          in Method (name_index, descriptor_index, local_vars_count, List.length args + 1, newseq)

let link_field classdefs f cptable =
  match f with
  | Syntax.Var (t, n) ->
          let name_index = find_utf8_index n.Syntax.id cptable
          and descriptor_index = find_utf8_index (make_field_descriptor t) cptable
          in Field (name_index, descriptor_index)

let rec link_body classdefs body cptable currtype supertype =
  match body with
  | Typing.TEmptyBody                  -> EmptyBody
  | Typing.TClassField (f, body)       -> ClassField (link_field classdefs f cptable, link_body classdefs body cptable currtype supertype)
  | Typing.TClassConstructor (c, body) -> ClassMethod (link_constructor classdefs c cptable currtype supertype, link_body classdefs body cptable currtype supertype)
  | Typing.TClassMethod (m, body)      -> ClassMethod (link_method classdefs m cptable currtype supertype, link_body classdefs body cptable currtype supertype)

let link_class classdefs c cptable =
  match c with
  | Typing.TClass (n1, n2, body) -> let n1_index = find_utf8_index n1.Syntax.id cptable
                                    and n2_index = find_utf8_index n2.Syntax.id cptable
                                    and newbody = link_body classdefs body cptable n1 n2
                                    in let n1_cindex = Utils.index cptable (Cpinfo.Class n1_index) + 1
                                    and n2_cindex = Utils.index cptable (Cpinfo.Class n2_index) + 1
                                    in Class (n1_cindex, n2_cindex, cptable, newbody)

