open Translating

type expr =
  | Aconst_null
  | Aload of int
  | Aload_0
  | Aload_1
  | Aload_2
  | Aload_3
  | Areturn
  | Astore of int
  | Astore_0
  | Astore_1
  | Astore_2
  | Astore_3
  | Checkcast of int
  | Dup
  | Dup_x1
  | Getfield of int
  | Goto of int
  | Iadd
  | Iand
  | Iconst_0
  | Iconst_1
  | Idiv
  | If_acmpeq of int
  | If_acmpne of int
  | If_icmpeq of int
  | If_icmpge of int
  | If_icmpgt of int
  | If_icmple of int
  | If_icmplt of int
  | If_icmpne of int
  | Ifeq of int
  | Ifne of int
  | Ifnonnull of int
  | Ifnull of int
  | Iload of int
  | Iload_0
  | Iload_1
  | Iload_2
  | Iload_3
  | Imul
  | Ineg
  | Ior
  | Invokespecial of int
  | Invokevirtual of int
  | Irem
  | Ireturn
  | Istore of int
  | Istore_0
  | Istore_1
  | Istore_2
  | Istore_3
  | Isub
  | Ldc of int
  | Ldc_w of int
  | New of int
  | Putfield of int
  | Return

type command =
  | Command of int * expr

type tfield =
  | Field of int * int

type tmethod =
  | Method of int * int * int * int * int * command list

type tclass =
  | Class of int * int * Cpinfo.cp_info list * tfield list * tmethod list

let get_command_length command =
  match command with
  | Aconst_null     -> 1
  | Aload _         -> 2
  | Aload_0         -> 1
  | Aload_1         -> 1
  | Aload_2         -> 1
  | Aload_3         -> 1
  | Areturn         -> 1
  | Astore _        -> 2
  | Astore_0        -> 1
  | Astore_1        -> 1
  | Astore_2        -> 1
  | Astore_3        -> 1
  | Checkcast _     -> 3
  | Dup             -> 1
  | Dup_x1          -> 1
  | Getfield _      -> 3
  | Goto _          -> 3
  | Iadd            -> 1
  | Iand            -> 1
  | Iconst_0        -> 1
  | Iconst_1        -> 1
  | Idiv            -> 1
  | If_acmpeq _     -> 3
  | If_acmpne _     -> 3
  | If_icmpeq _     -> 3
  | If_icmpge _     -> 3
  | If_icmpgt _     -> 3
  | If_icmple _     -> 3
  | If_icmplt _     -> 3
  | If_icmpne _     -> 3
  | Ifeq _          -> 3
  | Ifne _          -> 3
  | Ifnonnull _     -> 3
  | Ifnull _        -> 3
  | Iload _         -> 2
  | Iload_0         -> 1
  | Iload_1         -> 1
  | Iload_2         -> 1
  | Iload_3         -> 1
  | Imul            -> 1
  | Ineg            -> 1
  | Ior             -> 1
  | Invokespecial _ -> 3
  | Invokevirtual _ -> 3
  | Irem            -> 1
  | Ireturn         -> 1
  | Istore _        -> 2
  | Istore_0        -> 1
  | Istore_1        -> 1
  | Istore_2        -> 1
  | Istore_3        -> 1
  | Isub            -> 1
  | Ldc _           -> 2
  | Ldc_w _         -> 3
  | New _           -> 3
  | Putfield _      -> 3
  | Return          -> 1

let rec calculate_offset c =
  match c with
  | C_aconst_null     -> (1, Aconst_null)
  | C_aload i         -> if i = 0 then (1, Aload_0) else if i = 1 then (1, Aload_1) else if i = 2 then (1, Aload_2) else if i = 3 then (1, Aload_3) else (2, Aload i)
  | C_areturn         -> (1, Areturn)
  | C_astore i        -> if i = 0 then (1, Astore_0) else if i = 1 then (1, Astore_1) else if i = 2 then (1, Astore_2) else if i = 3 then (1, Astore_3) else (2, Astore i)
  | C_checkcast i     -> (3, Checkcast i)
  | C_dup             -> (1, Dup)
  | C_dup_x1          -> (1, Dup_x1)
  | C_getfield i      -> (3, Getfield i)
  | C_goto i          -> (3, Goto i)
  | C_iadd            -> (1, Iadd)
  | C_iand            -> (1, Iand)
  | C_iconst_0        -> (1, Iconst_0)
  | C_iconst_1        -> (1, Iconst_1)
  | C_idiv            -> (1, Idiv)
  | C_if_acmpeq i     -> (3, If_acmpeq i)
  | C_if_acmpne i     -> (3, If_acmpne i)
  | C_if_icmpeq i     -> (3, If_icmpeq i)
  | C_if_icmpge i     -> (3, If_icmpge i)
  | C_if_icmpgt i     -> (3, If_icmpgt i)
  | C_if_icmple i     -> (3, If_icmple i)
  | C_if_icmplt i     -> (3, If_icmplt i)
  | C_if_icmpne i     -> (3, If_icmpne i)
  | C_ifeq i          -> (3, Ifeq i)
  | C_ifne i          -> (3, Ifne i)
  | C_ifnonnull i     -> (3, Ifnonnull i)
  | C_ifnull i        -> (3, Ifnull i)
  | C_iload i         -> if i = 0 then (1, Iload_0) else if i = 1 then (1, Iload_1) else if i = 2 then (1, Iload_2) else if i = 3 then (1, Iload_3) else (2, Iload i)
  | C_imul            -> (1, Imul)
  | C_ineg            -> (1, Ineg)
  | C_ior             -> (1, Ior)
  | C_invokespecial i -> (3, Invokespecial i)
  | C_invokevirtual i -> (3, Invokevirtual i)
  | C_irem            -> (1, Irem)
  | C_ireturn         -> (1, Ireturn)
  | C_istore i        -> if i = 0 then (1, Istore_0) else if i = 1 then (1, Istore_1) else if i = 2 then (1, Istore_2) else if i = 3 then (1, Istore_3) else (2, Istore i)
  | C_isub            -> (1, Isub)
  | C_ldc i           -> (2, Ldc i)
  | C_ldc_w i         -> (3, Ldc_w i)
  | C_new i           -> (3, New i)
  | C_putfield i      -> (3, Putfield i)
  | C_return          -> (1, Return)

let calculate_offsets_s l =
  let jump_ref_to_global c currid l curroffset =
    match c with
    | Goto i      -> (match List.nth l (currid + i + 1) with Command (offset, _) -> Goto (offset - curroffset))
    | If_acmpeq i -> (match List.nth l (currid + i + 1) with Command (offset, _) -> If_acmpeq (offset - curroffset))
    | If_acmpne i -> (match List.nth l (currid + i + 1) with Command (offset, _) -> If_acmpne (offset - curroffset))
    | If_icmpeq i -> (match List.nth l (currid + i + 1) with Command (offset, _) -> If_icmpeq (offset - curroffset))
    | If_icmpge i -> (match List.nth l (currid + i + 1) with Command (offset, _) -> If_icmpge (offset - curroffset))
    | If_icmpgt i -> (match List.nth l (currid + i + 1) with Command (offset, _) -> If_icmpgt (offset - curroffset))
    | If_icmple i -> (match List.nth l (currid + i + 1) with Command (offset, _) -> If_icmple (offset - curroffset))
    | If_icmplt i -> (match List.nth l (currid + i + 1) with Command (offset, _) -> If_icmplt (offset - curroffset))
    | If_icmpne i -> (match List.nth l (currid + i + 1) with Command (offset, _) -> If_icmpne (offset - curroffset))
    | Ifeq i      -> (match List.nth l (currid + i + 1) with Command (offset, _) -> Ifeq (offset - curroffset))
    | Ifne i      -> (match List.nth l (currid + i + 1) with Command (offset, _) -> Ifne (offset - curroffset))
    | Ifnonnull i -> (match List.nth l (currid + i + 1) with Command (offset, _) -> Ifnonnull (offset - curroffset))
    | Ifnull i    -> (match List.nth l (currid + i + 1) with Command (offset, _) -> Ifnull (offset - curroffset))
    | _           -> c
  in let rec calculate_offsets_internal l offset =
    match l with
    | []      -> []
    | x :: xs -> let (o, c) = calculate_offset x in Command (offset, c) :: calculate_offsets_internal xs (offset + o)
  and jump_refs_to_global commands currid l =
    match commands with
    | []                        -> []
    | Command (offset, c) :: xs -> Command (offset, jump_ref_to_global c currid l offset) :: jump_refs_to_global xs (currid+1) l
  in let commands = calculate_offsets_internal l 0
  in jump_refs_to_global commands 0 commands

let calculate_offsets_f f =
  match f with
  | C_Field (i1, i2) -> Field (i1, i2)

let calculate_offsets_m m =
  match m with
  | C_Method (i1, i2, i3, i4, i5, seq) -> Method (i1, i2, i3, i4, i5, calculate_offsets_s seq)

let rec calculate_offsets_b body =
  match body with
  | C_EmptyBody             -> ([], [])
  | C_ClassField (f, body)  -> let (fields, methods) = calculate_offsets_b body in (calculate_offsets_f f :: fields, methods)
  | C_ClassMethod (m, body) -> let (fields, methods) = calculate_offsets_b body in (fields, calculate_offsets_m m :: methods)

let calculate_offsets c =
  match c with
  | C_Class (i1, i2, cptable, body) -> let (fields, methods) = calculate_offsets_b body in Class (i1, i2, cptable, fields, methods)

let get_code_length c =
  let rec get_code_length_aux c aux =
    match c with
    | []                              -> aux
    | Command (offset, command) :: xs -> get_code_length_aux xs (offset + get_command_length command)
  in get_code_length_aux c 0

let get_type_name s =
  let str = if String.contains s ')' then let i = String.index s ')' in String.sub s (i+1) (String.length s - i - 1) else s
  in if str = "I" then "int"
  else if str = "Z" then "boolean"
  else if str = "V" then "void"
  else String.sub str 1 (String.length str - 2)

let rec get_arg_types s =
  let str = if String.contains s ')' then String.sub s 1 (String.index s ')' - 1) else s
  in if str = ""
    then ""
    else let c = String.get str 0
         in if c = 'I' then let str2 = get_arg_types (String.sub str 1 (String.length str - 1)) in (if str2 = "" then "int" else "int, " ^ str2)
            else if c = 'Z' then let str2 = get_arg_types (String.sub str 1 (String.length str - 1)) in (if str2 = "" then "boolean" else "boolean, " ^ str2)
            else let i = String.index str ';' in let t = get_type_name (String.sub str 0 (i + 1)) and str2 = get_arg_types (String.sub str (i+1) (String.length str - i - 1)) in (if str2 = "" then t else t ^ ", " ^ str2)

let rec format_field (Field (i1, i2)) cptable =
  let name = Cpinfo.get_text cptable i1 and descriptor = Cpinfo.get_text cptable i2
  in let typename = get_type_name descriptor
  in "  " ^ typename ^ " " ^ name ^ ";\n" ^
     "    descriptor: " ^ descriptor ^ "\n\n"

let format_instruction i cptable curroffset =
  match i with
  | Aconst_null     -> Printf.sprintf "%-34s"  "aconst_null"
  | Aload i         -> Printf.sprintf "%-34s" ("aload         " ^ string_of_int i)
  | Aload_0         -> Printf.sprintf "%-34s"  "aload_0"
  | Aload_1         -> Printf.sprintf "%-34s"  "aload_1"
  | Aload_2         -> Printf.sprintf "%-34s"  "aload_2"
  | Aload_3         -> Printf.sprintf "%-34s"  "aload_3"
  | Areturn         -> Printf.sprintf "%-34s"  "areturn"
  | Astore i        -> Printf.sprintf "%-34s" ("astore        " ^ string_of_int i)
  | Astore_0        -> Printf.sprintf "%-34s"  "astore_0"
  | Astore_1        -> Printf.sprintf "%-34s"  "astore_1"
  | Astore_2        -> Printf.sprintf "%-34s"  "astore_2"
  | Astore_3        -> Printf.sprintf "%-34s"  "astore_3"
  | Checkcast i     -> Printf.sprintf "%-34s" ("checkcast     #" ^ string_of_int i) ^ Cpinfo.explain cptable (List.nth cptable (i-1)) "// Class "
  | Dup             -> Printf.sprintf "%-34s"  "dup"
  | Dup_x1          -> Printf.sprintf "%-34s"  "dup_x1"
  | Getfield i      -> Printf.sprintf "%-34s" ("getfield      #" ^ string_of_int i) ^ Cpinfo.explain cptable (List.nth cptable (i-1)) "// Field "
  | Goto i          -> Printf.sprintf "%-34s" ("goto          " ^ string_of_int (curroffset + i))
  | Iadd            -> Printf.sprintf "%-34s"  "iadd"
  | Iand            -> Printf.sprintf "%-34s"  "iand"
  | Iconst_0        -> Printf.sprintf "%-34s"  "iconst_0"
  | Iconst_1        -> Printf.sprintf "%-34s"  "iconst_1"
  | Idiv            -> Printf.sprintf "%-34s"  "idiv"
  | If_acmpeq i     -> Printf.sprintf "%-34s" ("if_acmpeq     " ^ string_of_int (curroffset + i))
  | If_acmpne i     -> Printf.sprintf "%-34s" ("if_acmpne     " ^ string_of_int (curroffset + i))
  | If_icmpeq i     -> Printf.sprintf "%-34s" ("if_icmpeq     " ^ string_of_int (curroffset + i))
  | If_icmpge i     -> Printf.sprintf "%-34s" ("if_icmpge     " ^ string_of_int (curroffset + i))
  | If_icmpgt i     -> Printf.sprintf "%-34s" ("if_icmpgt     " ^ string_of_int (curroffset + i))
  | If_icmple i     -> Printf.sprintf "%-34s" ("if_icmple     " ^ string_of_int (curroffset + i))
  | If_icmplt i     -> Printf.sprintf "%-34s" ("if_icmplt     " ^ string_of_int (curroffset + i))
  | If_icmpne i     -> Printf.sprintf "%-34s" ("if_icmpne     " ^ string_of_int (curroffset + i))
  | Ifeq i          -> Printf.sprintf "%-34s" ("ifeq          " ^ string_of_int (curroffset + i))
  | Ifne i          -> Printf.sprintf "%-34s" ("ifne          " ^ string_of_int (curroffset + i))
  | Ifnonnull i     -> Printf.sprintf "%-34s" ("ifnonnull     " ^ string_of_int (curroffset + i))
  | Ifnull i        -> Printf.sprintf "%-34s" ("ifnull        " ^ string_of_int (curroffset + i))
  | Iload i         -> Printf.sprintf "%-34s" ("iload         " ^ string_of_int (curroffset + i))
  | Iload_0         -> Printf.sprintf "%-34s"  "iload_0"
  | Iload_1         -> Printf.sprintf "%-34s"  "iload_1"
  | Iload_2         -> Printf.sprintf "%-34s"  "iload_2"
  | Iload_3         -> Printf.sprintf "%-34s"  "iload_3"
  | Imul            -> Printf.sprintf "%-34s"  "imul"
  | Ineg            -> Printf.sprintf "%-34s"  "ineg"
  | Ior             -> Printf.sprintf "%-34s"  "ior"
  | Invokespecial i -> Printf.sprintf "%-34s" ("invokespecial #" ^ string_of_int i) ^ Cpinfo.explain cptable (List.nth cptable (i-1)) "// Method "
  | Invokevirtual i -> Printf.sprintf "%-34s" ("invokevirtual #" ^ string_of_int i) ^ Cpinfo.explain cptable (List.nth cptable (i-1)) "// Method "
  | Irem            -> Printf.sprintf "%-34s"  "irem"
  | Ireturn         -> Printf.sprintf "%-34s"  "ireturn"
  | Istore i        -> Printf.sprintf "%-34s" ("istore        " ^ string_of_int i)
  | Istore_0        -> Printf.sprintf "%-34s"  "istore_0"
  | Istore_1        -> Printf.sprintf "%-34s"  "istore_1"
  | Istore_2        -> Printf.sprintf "%-34s"  "istore_2"
  | Istore_3        -> Printf.sprintf "%-34s"  "istore_3"
  | Isub            -> Printf.sprintf "%-34s"  "isub"
  | Ldc i           -> Printf.sprintf "%-34s" ("ldc           #" ^ string_of_int i) ^ (match List.nth cptable (i-1) with Cpinfo.Integer x -> "// int " ^ string_of_int x | _ -> "")
  | Ldc_w i         -> Printf.sprintf "%-34s" ("ldc_w         #" ^ string_of_int i) ^ (match List.nth cptable (i-1) with Cpinfo.Integer x -> "// int " ^ string_of_int x | _ -> "")
  | New i           -> Printf.sprintf "%-34s" ("new           #" ^ string_of_int i) ^ Cpinfo.explain cptable (List.nth cptable (i-1)) "// Class "
  | Putfield i      -> Printf.sprintf "%-34s" ("putfield      #" ^ string_of_int i) ^ Cpinfo.explain cptable (List.nth cptable (i-1)) "// Field "
  | Return          -> Printf.sprintf "%-34s"  "return"

let rec format_code code cptable =
  let l = int_of_float (ceil (log10 (float_of_int (get_code_length code)))) + 8
  in match code with
     | []                   -> ""
     | Command (o, x) :: xs -> (Printf.sprintf ((Scanf.format_from_string ("%" ^ string_of_int l ^ "s") "%s") ^^ ": ") (string_of_int o)) ^ format_instruction x cptable o ^ "\n" ^ format_code xs cptable

let rec format_method (Method (i1, i2, locals, args_size, stack_size, code)) cptable cname =
  let name = Cpinfo.get_text cptable i1 and descriptor = Cpinfo.get_text cptable i2
  in let real_name = if name = "<init>" then cname else name
  in let rettype = get_type_name descriptor and argtypes = get_arg_types descriptor
  in "  " ^ (if name = real_name then rettype ^ " " else "") ^ real_name ^ "(" ^ argtypes ^ ");\n" ^
     "    descriptor: " ^ descriptor ^ "\n" ^
     "    Code:\n" ^
     "      locals=" ^ string_of_int locals ^ ", args_size=" ^ string_of_int args_size ^ ", max_stack=" ^ string_of_int stack_size ^ "\n" ^
     format_code code cptable ^ "\n"

let rec format_fields fields cptable =
  match fields with
  | []      -> ""
  | x :: xs -> format_field x cptable ^ format_fields xs cptable

let rec format_methods methods cptable cname =
  match methods with
  | []      -> ""
  | x :: xs -> format_method x cptable cname ^ format_methods xs cptable cname

let format_class_str c =
  match c with
  | Class (i1, i2, l, fields, methods) -> "class " ^ Cpinfo.get_text_r l i1 ^ "\n" ^ Cpinfo.format_cpinfo_str l ^
                                          "{\n" ^
                                          format_fields fields l ^
                                          format_methods methods l (Cpinfo.get_text_r l i1) ^
                                          "}\n"

