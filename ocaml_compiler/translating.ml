open Linking

type cexpr =
  | C_aconst_null
  | C_aload of int
  | C_areturn
  | C_astore of int
  | C_checkcast of int
  | C_dup
  | C_dup_x1
  | C_getfield of int
  | C_goto of int          (* local offset - #instructions to skip *)
  | C_iadd
  | C_iand
  | C_iconst_0
  | C_iconst_1
  | C_idiv
  | C_if_acmpeq of int     (* local offset - #instructions to skip *)
  | C_if_acmpne of int     (* local offset - #instructions to skip *)
  | C_if_icmpeq of int     (* local offset - #instructions to skip *)
  | C_if_icmpge of int     (* local offset - #instructions to skip *)
  | C_if_icmpgt of int     (* local offset - #instructions to skip *)
  | C_if_icmple of int     (* local offset - #instructions to skip *)
  | C_if_icmplt of int     (* local offset - #instructions to skip *)
  | C_if_icmpne of int     (* local offset - #instructions to skip *)
  | C_ifeq of int          (* local offset - #instructions to skip *)
  | C_ifne of int          (* local offset - #instructions to skip *)
  | C_ifnonnull of int     (* local offset - #instructions to skip *)
  | C_ifnull of int        (* local offset - #instructions to skip *)
  | C_iload of int
  | C_imul
  | C_ineg
  | C_ior
  | C_invokespecial of int
  | C_invokevirtual of int
  | C_irem
  | C_ireturn
  | C_istore of int
  | C_isub
  | C_ldc of int
  | C_ldc_w of int
  | C_new of int
  | C_putfield of int
  | C_return

type cfield =
  | C_Field of int * int

type cmethod =
  | C_Method of int * int * int * int * int * cexpr list

type cclassbody =
  | C_EmptyBody
  | C_ClassField of cfield * cclassbody
  | C_ClassMethod of cmethod * cclassbody

type cclass =
  | C_Class of int * int * Cpinfo.cp_info list * cclassbody

let rec translate_expr e value_needed =
  match e with
  | Asgn (e1, e2, _)     -> let l = translate_expr e2 true
                            in (match e1 with
                                | Field (e1_1, i, _) -> translate_expr e1_1 true @ l @ if value_needed then [C_dup_x1; C_putfield i] else [C_putfield i]
                                | Val (i, t)         -> (match t with
                                                         | Syntax.Int
                                                         | Syntax.Boolean -> l @ if value_needed then [C_dup_x1; C_istore i] else [C_istore i]
                                                         | Syntax.Named _ -> l @ if value_needed then [C_dup_x1; C_astore i] else [C_astore i])
                                | _                  -> failwith "Assignment possible only to field or variable")
  | Val (i, t)           -> (match t with
                             | Syntax.Int
                             | Syntax.Boolean -> [C_iload i]
                             | Syntax.Named _ -> [C_aload i])
  | Call (e, m, args, _) -> let l = translate_expr e true
                            in l @ List.fold_right (fun arg -> fun xs -> translate_expr arg true @ xs) args [C_invokevirtual m]
  | Super (e, m, args)   -> let l = translate_expr e true
                            in l @ List.fold_right (fun arg -> fun xs -> translate_expr arg true @ xs) args [C_invokespecial m]
  | Field (e, i, _)      -> translate_expr e true @ [C_getfield i]
  | New (c, m, args, _)  -> C_new c :: C_dup :: List.fold_right (fun arg -> fun xs -> translate_expr arg true @ xs) args [C_invokespecial m]
  | Num (i, _)           -> if i <= 255 then [C_ldc i] else [C_ldc_w i]
  | Plus (e1, e2, _)     -> translate_expr e1 true @ translate_expr e2 true @ [C_iadd]
  | Minus (e1, e2, _)    -> translate_expr e1 true @ translate_expr e2 true @ [C_isub]
  | Times (e1, e2, _)    -> translate_expr e1 true @ translate_expr e2 true @ [C_imul]
  | Div (e1, e2, _)      -> translate_expr e1 true @ translate_expr e2 true @ [C_idiv]
  | Mod (e1, e2, _)      -> translate_expr e1 true @ translate_expr e2 true @ [C_irem]
  | UnaryMinus (e, _)    -> translate_expr e true @ [C_ineg]
  | Lt (e1, e2, _)       -> translate_expr e1 true @ translate_expr e2 true @ [C_if_icmplt 2; C_iconst_0; C_goto 1; C_iconst_1]
  | Leq (e1, e2, _)      -> translate_expr e1 true @ translate_expr e2 true @ [C_if_icmple 2; C_iconst_0; C_goto 1; C_iconst_1]
  | Gt (e1, e2, _)       -> translate_expr e1 true @ translate_expr e2 true @ [C_if_icmpgt 2; C_iconst_0; C_goto 1; C_iconst_1]
  | Geq (e1, e2, _)      -> translate_expr e1 true @ translate_expr e2 true @ [C_if_icmpge 2; C_iconst_0; C_goto 1; C_iconst_1]
  | Eq (e1, e2, _)       -> let l1 = translate_expr e1 true and l2 = translate_expr e2 true
                            in (match (extract_type e1, extract_type e2) with
                                | (Some Syntax.Int, Some Syntax.Int)
                                | (Some Syntax.Boolean, Some Syntax.Boolean)     -> l1 @ l2 @ [C_if_icmpeq 2; C_iconst_0; C_goto 1; C_iconst_1]
                                | (Some (Syntax.Named _), None)                  -> l1 @ [C_ifnull 2; C_iconst_0; C_goto 1; C_iconst_1]
                                | (None, Some (Syntax.Named _))                  -> l2 @ [C_ifnull 2; C_iconst_0; C_goto 1; C_iconst_1]
                                | (Some (Syntax.Named _), Some (Syntax.Named _)) -> l1 @ l2 @ [C_if_acmpeq 2; C_iconst_0; C_goto 1; C_iconst_1]
                                | (None, None)                                   -> [C_iconst_1]
                                | _                                              -> failwith "Incompatible types for '=='")
  | Neq (e1, e2, _)      -> let l1 = translate_expr e1 true and l2 = translate_expr e2 true
                            in (match (extract_type e1, extract_type e2) with
                                | (Some Syntax.Int, Some Syntax.Int)
                                | (Some Syntax.Boolean, Some Syntax.Boolean)     -> l1 @ l2 @ [C_if_icmpne 2; C_iconst_0; C_goto 1; C_iconst_1]
                                | (Some (Syntax.Named _), None)                  -> l1 @ [C_ifnonnull 2; C_iconst_0; C_goto 1; C_iconst_1]
                                | (None, Some (Syntax.Named _))                  -> l2 @ [C_ifnonnull 2; C_iconst_0; C_goto 1; C_iconst_1]
                                | (Some (Syntax.Named _), Some (Syntax.Named _)) -> l1 @ l2 @ [C_if_acmpne 2; C_iconst_0; C_goto 1; C_iconst_1]
                                | (None, None)                                   -> [C_iconst_0]
                                | _                                              -> failwith "Incompatible types for '!='")
  | JTrue _              -> [C_iconst_1]
  | JFalse _             -> [C_iconst_0]
  | And (e1, e2, _)      -> translate_expr e1 true @ translate_expr e2 true @ [C_iand]
  | Or (e1, e2, _)       -> translate_expr e1 true @ translate_expr e2 true @ [C_ior]
  | Not (e, _)           -> translate_expr e true @ [C_ifne 2; C_iconst_1; C_goto 1; C_iconst_0]
  | Null                 -> [C_aconst_null]
  | Cast (i, e, _)       -> translate_expr e true @ [C_checkcast i]

let stack_size s =
  let stack_size_c c currs maxs =
    match c with
    | C_aconst_null
    | C_aload _
    | C_dup
    | C_dup_x1
    | C_iconst_0
    | C_iconst_1
    | C_iload _
    | C_ldc _
    | C_ldc_w _
    | C_new _       -> (currs + 1, max maxs (currs + 1))
    | C_areturn
    | C_ireturn
    | C_return      -> (0, maxs)
    | C_astore _
    | C_iadd
    | C_iand
    | C_idiv
    | C_ifeq _
    | C_ifne _
    | C_ifnonnull _
    | C_ifnull _
    | C_imul
    | C_ior
    | C_irem
    | C_istore _
    | C_isub        -> (currs - 1, maxs)
    | C_if_acmpeq _
    | C_if_acmpne _
    | C_if_icmpeq _
    | C_if_icmpge _
    | C_if_icmpgt _
    | C_if_icmple _
    | C_if_icmplt _
    | C_if_icmpne _
    | C_putfield _  -> (currs - 2, maxs)
    | _             -> (currs, maxs)
  in let rec stack_size_aux s currs maxs =
    match s with
    | []      -> maxs
    | x :: xs -> let (newc, newm) = stack_size_c x currs maxs in stack_size_aux xs newc newm
  in stack_size_aux s 0 0

let rec translate_seq seq =
  match seq with
  | EndSeq              -> ([], 0)
  | Expression (e, seq) -> let (tseq, maxstack) = translate_seq seq
                           and expr_code = translate_expr e false
                           in let ssize = stack_size expr_code
                           in (expr_code @ tseq, max maxstack ssize)
  | If (e, s1, s2, s3)  -> let (l1, m1) = translate_seq s1 and (l2, m2) = translate_seq s2 and (l3, m3) = translate_seq s3 and l = translate_expr e true
                           in let offset1 = List.length l1 + 1 and offset2 = List.length l2
                           and ssize = stack_size l
                           in (l @ [C_ifeq offset1] @ l1 @ [C_goto offset2] @ l2 @ l3, Utils.list_max [ssize; m1; m2; m3])
  | While (e, s1, s2)   -> let (l1, m1) = translate_seq s1 and (l2, m2) = translate_seq s2 and l = translate_expr e true
                           in let loop_offset = List.length l1 + 1
                           and ssize = stack_size l
                           in let loop = l @ [C_ifeq loop_offset] @ l1
                           in let offset = - (List.length loop + 1)
                           in (loop @ [C_goto offset] @ l2, Utils.list_max [ssize; m1; m2])
  | Return e            -> let l = translate_expr e true in
                           match extract_type e with
                           | Some Syntax.Int
                           | Some Syntax.Boolean -> (l @ [C_ireturn], stack_size l)
                           | _                   -> (l @ [C_areturn], stack_size l)

let translate_field f =
  match f with
  | Field (i1, i2) -> C_Field (i1, i2)

let translate_method m =
  match m with
  | Method (i1, i2, i3, i4, seq) -> let (newseq, ssize) = translate_seq seq
                                    in let app =
                                      if newseq = []
                                        then [C_return]
                                        else let last = List.nth newseq (List.length newseq - 1)
                                             in if last = C_return || last = C_ireturn || last = C_areturn then [] else [C_return]
                                    in C_Method (i1, i2, i3, i4, ssize, newseq @ app)

let rec translate_body body =
  match body with
  | EmptyBody             -> C_EmptyBody
  | ClassField (f, body)  -> C_ClassField (translate_field f, translate_body body)
  | ClassMethod (m, body) -> C_ClassMethod (translate_method m, translate_body body)

let translate_class c =
  match c with
  | Class (i1, i2, l, body) -> C_Class (i1, i2, l, translate_body body)

